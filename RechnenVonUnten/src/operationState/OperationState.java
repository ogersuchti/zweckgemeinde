package operationState;

import java.math.BigInteger;

import model.Operator;

public interface OperationState {
	
	/**
	 * @return the value from the given Operator from cache or by computing it.
	 */
	BigInteger getValue(Operator operator);
	
	/**
	 * Throws all caches away because the value has changed.
	 */
	void valueChanged(Operator operator);

}
