package operationState;

import java.math.BigInteger;

import model.Operator;

public class NoValue implements OperationState {

	private static NoValue theNoValue;

	/**
	 * Singleton constructor.
	 */
	public static NoValue create() {
		if (theNoValue == null) {
			theNoValue = new NoValue();
		}
		return theNoValue;
	}

	private NoValue() {
	}

	@Override
	public BigInteger getValue(Operator operator) {
		final BigInteger computedValue = operator.computeValue();
		operator.setState(Value.create(computedValue));
		return computedValue;
	}

	@Override
	public void valueChanged(Operator operator) {
	}

}
