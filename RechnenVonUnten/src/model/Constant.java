package model;

import java.math.BigInteger;

public class Constant extends Observee implements Expression {

	private final BigInteger value;

	public static Constant create(BigInteger value) {
		return new Constant(value);
	}

	private Constant(BigInteger value) {
		this.value = value;
	}

	@Override
	public BigInteger getValue() {
		return this.value;
	}

	@Override
	public BigInteger computeValue() {
		return this.getValue();
	}

	@Override
	public String getName() {
		return this.value.toString();
	}

	@Override
	public String toString() {
		return this.getName();

	}

	@Override
	public boolean contains(Expression expression) {
		return this.equals(expression);
	}

}
