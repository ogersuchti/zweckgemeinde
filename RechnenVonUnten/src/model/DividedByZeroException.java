package model;

@SuppressWarnings("serial")
public class DividedByZeroException extends RuntimeException {
	
	public DividedByZeroException(String message) {
		super(message);
	}

}
