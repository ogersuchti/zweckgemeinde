package model;

import java.math.BigInteger;

public class Variable extends ExpressionCommon {

	private static final BigInteger InitialVariableValue = BigInteger.ZERO;
	private static final String ValueOpenBracket = "(";
	private static final String ValueCloseBracket = ")";
	private static final BigInteger IncrementValue = BigInteger.ONE;

	public static Variable createVariable(final String name) {
		return new Variable(name, InitialVariableValue);
	}

	private final String name;
	private BigInteger value;
	
	private Variable(final String name, final BigInteger initialValue) {
		this.name = name;
		this.setValue(initialValue);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public boolean equals(Object argument) {
		return super.equals(argument);
	}

	@Override
	public String toString() {
		return this.getName() + ValueOpenBracket + this.getValue() + ValueCloseBracket;
	}

	public void up() {
		this.setValue(this.getValue().add(IncrementValue));
		this.notifyObservers();
	}

	public void down() {
		this.setValue(this.getValue().subtract(IncrementValue));
		this.notifyObservers();
	}

	@Override
	public BigInteger getValue() {
		return this.value;
	}

	public void setValue(final BigInteger newValue) {
		this.value = newValue;
	}

	@Override
	public BigInteger computeValue() {
		return this.value;
	}

	@Override
	public boolean contains(Expression expression) {
		return this.equals(expression);
	}
}
