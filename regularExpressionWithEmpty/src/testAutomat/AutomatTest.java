package testAutomat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import util.automaton.Automat;
import util.automaton.Configuration;
import util.automaton.State;
import util.automaton.Transition;

public class AutomatTest {

	// Automat Nr.1
	private State state;
	private Automat a1;

	// Automat Nr.2

	private Automat a2;
	private State z1;
	private State z2;

	@Before
	public void setUp() throws Exception {
		// Automat Nr.1

		a1 = Automat.create();
		state = a1.createState();
		final State begin = a1.getAnfangsZustand();
		final State end = a1.getEndZustand();
		begin.addTransition('a', state);
		state.addTransition('b', state);
		state.addTransition('a', end);

		// Automat Nr.2

		a2 = Automat.create();
		z1 = a2.createState();
		z2 = a2.createState();
		a2.getAnfangsZustand().addTransition('a', z1);
		z1.addTransition('b', a2.getAnfangsZustand());
		z1.addTransition('b', z1);
		z1.addTransition('a', z2);
		z2.addTransition('b', z1);
		z2.addTransition('c', a2.getEndZustand());
		z2.addTransition('b', z2);
	}

	@Test
	public void testAutomatId() {
		final Automat automatId = Automat.createId();
		assertTrue(automatId.recognizes(""));
		automatId.sequence(Automat.createId());
		automatId.sequence(Automat.createId());
		automatId.sequence(Automat.createId());
		automatId.sequence(Automat.createId());
		assertTrue(automatId.recognizes(""));
		assertTrue(a1.recognizes("aa"));
		assertFalse(a1.recognizes("a"));
		assertFalse(a1.recognizes("abbbb"));
		assertFalse(a1.recognizes("ababbb"));
		assertTrue(a1.recognizes("abbbba"));
		// Neutrales Element bei sequence
		a1.sequence(automatId);
		assertTrue(a1.recognizes("aa"));
		assertFalse(a1.recognizes("a"));
		assertFalse(a1.recognizes("abbbb"));
		assertFalse(a1.recognizes("ababbb"));
		assertTrue(a1.recognizes("abbbba"));
	}

	@Test
	public void testNullAutomat() {
		final Automat nullAutomat = Automat.create();
		assertFalse(nullAutomat.recognizes(""));
		assertFalse(nullAutomat.recognizes("x"));
		assertFalse(nullAutomat.recognizes("y"));
		assertFalse(nullAutomat.recognizes("asdasdjnajskdaklsdmasdjaskdasdjasd"));
		// Neutrales Element bei choice
		nullAutomat.choice(Automat.create());
		assertFalse(nullAutomat.recognizes(""));
		assertFalse(nullAutomat.recognizes("x"));
		assertFalse(nullAutomat.recognizes("y"));
		assertFalse(nullAutomat.recognizes("asdasdjnajskdaklsdmasdjaskdasdjasd"));

		assertTrue(a1.recognizes("aa"));
		assertFalse(a1.recognizes("a"));
		assertFalse(a1.recognizes("abbbb"));
		assertFalse(a1.recognizes("ababbb"));
		assertTrue(a1.recognizes("abbbba"));
		// Neutrales Element bei choice
		a1.choice(nullAutomat);
		assertTrue(a1.recognizes("aa"));
		assertFalse(a1.recognizes("a"));
		assertFalse(a1.recognizes("abbbb"));
		assertFalse(a1.recognizes("ababbb"));
		assertTrue(a1.recognizes("abbbba"));
	}

	@Test
	public void testRecognizeAutomat1() {
		assertTrue(a1.recognizes("aa"));
		assertTrue(a1.recognizes("abbbba"));
		assertTrue(a1.recognizes("abbbbbbbbbbbbba"));
		assertTrue(a1.recognizes("aba"));

		assertFalse(a1.recognizes("a"));
		assertFalse(a1.recognizes("XYZ%&//!�!)"));
		assertFalse(a1.recognizes("aabb"));
		assertFalse(a1.recognizes("abbbb"));
		assertFalse(a1.recognizes("ababbb"));

		assertFalse(a1.recognizes(""));
		a1.setRecognizesEmptyString(true);
		assertTrue(a1.recognizes(""));
	}

	@Test
	public void testRecognizeAutomat2() {

		assertTrue(a2.recognizes("aac"));
		assertTrue(a2.recognizes("abaac"));
		assertTrue(a2.recognizes("abbbbbaac"));
		assertTrue(a2.recognizes("aabbbbbac"));
		assertTrue(a2.recognizes("aabbc"));
		assertTrue(a2.recognizes("aabac"));
		assertTrue(a2.recognizes("aabbaac"));
		assertTrue(a2.recognizes("aabbbbbac"));
		assertTrue(a2.recognizes("abbbbbaac"));

		assertFalse(a2.recognizes("abbbbbc"));
		assertFalse(a2.recognizes("baac"));
		assertFalse(a2.recognizes("aacb"));
		assertFalse(a2.recognizes("abbbcc"));

		assertFalse(a2.recognizes(""));
		a2.setRecognizesEmptyString(true);
		assertTrue(a2.recognizes(""));
	}

	@Test
	public void testContainsEndState() {
		final HashSet<State> states = new HashSet<State>();
		assertFalse(a1.containsEndState(states));
		assertFalse(a2.containsEndState(states));
		states.add(a1.getEndZustand());
		states.add(a1.getAnfangsZustand());
		states.add(state);
		assertTrue(a1.containsEndState(states));
		assertFalse(a2.containsEndState(states));
		states.remove(a1.getEndZustand());
		assertFalse(a1.containsEndState(states));
		assertFalse(a2.containsEndState(states));
	}

	@Test
	public void testStep() {
		final Collection<State> currentStates2 = new HashSet<State>();
		currentStates2.add(a1.getAnfangsZustand());
		final Configuration startConfig = Configuration.create("a", a1, currentStates2);
		assertEquals(Configuration.create("", a1), Configuration.create("", a1).step());
		final Configuration nextConfig = startConfig.step();
		final Collection<State> currentStates1 = new HashSet<State>();
		currentStates1.add(state);
		assertEquals(Configuration.create("", a1, currentStates1), nextConfig);

	}

	@Test
	public void testNextStatesWithParameter() {
		final HashSet<State> oneEntryA1 = new HashSet<State>();
		final HashSet<State> oneEntryEnd = new HashSet<State>();
		oneEntryA1.add(state);
		oneEntryEnd.add(a1.getEndZustand());
		assertEquals(oneEntryA1, a1.nextStates('a', a1.getAnfangsZustand()));
		assertEquals(oneEntryEnd, a1.nextStates('a', state));
		assertEquals(oneEntryA1, a1.nextStates('b', state));
	}

	@Test
	public void testNextStatesWithoutParameter() {
		final Collection<State> z0z1z2 = a2.nextStates(z1);
		assertTrue(z0z1z2.contains(a2.getAnfangsZustand()));
		assertTrue(z0z1z2.contains(z2));
		assertTrue(z0z1z2.contains(z1));
		assertFalse(z0z1z2.contains(a2.getEndZustand()));
	}

	@Test
	public void testIsInput() {

		final Transition trans1 = Transition.create('X', z1, z1);
		final Transition trans2 = Transition.create('Y', z1, z1);
		final Transition trans3 = Transition.create('Z', z1, z1);

		assertTrue(trans1.isInput('X'));
		assertTrue(trans2.isInput('Y'));
		assertTrue(trans3.isInput('Z'));

		assertFalse(trans1.isInput('Z'));
		assertFalse(trans2.isInput('1'));
		assertFalse(trans3.isInput('8'));
	}

	@Test
	/**
	 * Detallierte Zeichung des mautomat entnehmen Sie dem Pdf-Dokument
	 * Testautomat_GetUsedStates2.
	 */
	public void testGetUsedStates2() {
		// setUp mautomat
		final Automat mautomat = Automat.create();
		final State z0 = mautomat.getAnfangsZustand();
		final State zE = mautomat.getEndZustand();
		final State z1 = mautomat.createState();
		final State z2 = mautomat.createState();
		final State z3 = mautomat.createState();
		final State z4 = mautomat.createState();
		final State z5 = mautomat.createState();
		final State z6 = mautomat.createState();
		final State z7 = mautomat.createState();
		z0.addTransition('a', z1);

		z0.addTransition('a', z2);
		z2.addTransition('a', z4);
		z4.addTransition('a', z2);
		z4.addTransition('a', zE);

		z0.addTransition('a', z6);
		z6.addTransition('a', z7);
		z7.addTransition('a', z6);

		z3.addTransition('a', zE);
		z3.addTransition('a', z3);
		z3.addTransition('a', z5);
		z5.addTransition('a', zE);
		// Test getUsedStates
		final HashSet<State> usedStates = mautomat.getUsedStates();
		assertTrue(usedStates.contains(zE));
		assertTrue(usedStates.contains(z4));
		assertTrue(usedStates.contains(z0));
		assertTrue(usedStates.contains(z2));

		assertFalse(usedStates.contains(z1));
		assertFalse(usedStates.contains(z6));
		assertFalse(usedStates.contains(z7));
		assertFalse(usedStates.contains(z3));
		assertFalse(usedStates.contains(z5));
		
		// Test normalise !
		assertEquals(9,mautomat.countStates());
		mautomat.normalise();
		assertEquals(4,mautomat.countStates());
	}

	@Test
	/**
	 * Testet getUsedStates indem dem Automaten a1 und a2 zwei unn�tze Zust�nde
	 * hinzugef�gt werde.
	 */
	public void testGetUsedStates1() {
		final State testState1 = a1.createState();
		a1.getAnfangsZustand().addTransition('x', testState1);
		final HashSet<State> usedStatesA1 = a1.getUsedStates();
		assertTrue(usedStatesA1.contains(a1.getAnfangsZustand()));
		assertTrue(usedStatesA1.contains(state));
		assertTrue(usedStatesA1.contains(a1.getEndZustand()));
		assertFalse(usedStatesA1.contains(testState1));

		final State testState2 = a2.createState();
		a2.getEndZustand().addTransition('x', testState2);
		final HashSet<State> usedStatesA2 = a2.getUsedStates();
		assertTrue(usedStatesA2.contains(z1));
		assertTrue(usedStatesA2.contains(z2));
		assertTrue(usedStatesA2.contains(a2.getAnfangsZustand()));
		assertTrue(usedStatesA2.contains(a2.getEndZustand()));
		assertFalse(usedStatesA2.contains(testState2));
	}
	@Test
	public void testGetUsedStates3() {
		// setUp mautomat
		final Automat mautomat = Automat.create();
		final State z0 = mautomat.getAnfangsZustand();
		final State zE = mautomat.getEndZustand();
		final State z1 = mautomat.createState();
		final State z2 = mautomat.createState();
		final State z3 = mautomat.createState();
		final State z4 = mautomat.createState();
		final State z5 = mautomat.createState();
		final State z6 = mautomat.createState();
		final State z7 = mautomat.createState();
		z0.addTransition('a', z1);

		z0.addTransition('a', z2);
		z2.addTransition('a', z4);
		z4.addTransition('a', z2);
		z4.addTransition('a', zE);

		z0.addTransition('a', z6);
		z6.addTransition('a', z7);
		z7.addTransition('a', z6);

		z3.addTransition('a', zE);
		z3.addTransition('a', z3);
		z3.addTransition('a', z5);
		z5.addTransition('a', zE);
		
		zE.addTransition('a', z3);
		// Test getUsedStates
		final HashSet<State> usedStates = mautomat.getUsedStates();
		assertTrue(usedStates.contains(zE));
		assertTrue(usedStates.contains(z4));
		assertTrue(usedStates.contains(z0));
		assertTrue(usedStates.contains(z2));
		assertTrue(usedStates.contains(z3));
		assertTrue(usedStates.contains(z5));

		assertFalse(usedStates.contains(z1));
		assertFalse(usedStates.contains(z6));
		assertFalse(usedStates.contains(z7));
		
		// Test normalise !
		assertEquals(9,mautomat.countStates());
		mautomat.normalise();
		assertEquals(6,mautomat.countStates());
	}

}
