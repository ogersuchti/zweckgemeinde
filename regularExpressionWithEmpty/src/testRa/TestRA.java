package testRa;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import expressions.Choice;
import expressions.BaseExpression;
import expressions.Sequence;
import expressions.RegularExpression;

public class TestRA {
	// Basisausdrücke zum Testen
	private RegularExpression basisausdruckA;
	private RegularExpression basisausdruckB;
	private RegularExpression basisausdruckC;
	// Konkatenationen von Basisausdrücken zum Testen
	private RegularExpression konkatenationAB;
	private RegularExpression konkatenationAC;
	// Konkatenation von konkatenationAB und basisausdruckC
	private RegularExpression konkatenationABC;
	// Auswahl von Basisausdrücken zum Testen
	private RegularExpression auswahlAB;
	private RegularExpression auswahlAC;
	// Auswahl von Auswahl und Basisausdruck zum Testen
	private RegularExpression auswahlABC;

	// Gesamt Test siehe Pdf
	private RegularExpression a1;
	private RegularExpression a2;
	private RegularExpression bX;
	private RegularExpression bY;
	private RegularExpression bZ;
	private RegularExpression bT;
	private RegularExpression bH;
	private RegularExpression k;

	@Before
	/**
	 * Initialisiert alle Regulären Ausdrücke korrekt.
	 */
	public void setUp() throws Exception {
		// Basisausdrücke
		basisausdruckA = BaseExpression.create('a');
		basisausdruckB = BaseExpression.create('b');
		basisausdruckC = BaseExpression.create('c');
		// Konkatenationen aus Basisausdrücken
		konkatenationAB = Sequence.create();
		konkatenationAC = Sequence.create();
		konkatenationAB.addExpression(basisausdruckA);
		konkatenationAB.addExpression(basisausdruckB);
		konkatenationAC.addExpression(basisausdruckA);
		konkatenationAC.addExpression(basisausdruckC);
		// Konkatenation aus Konkatenation und Basisausdruck
		konkatenationABC = Sequence.create();
		konkatenationABC.addExpression(konkatenationAB);
		konkatenationABC.addExpression(basisausdruckC);
		// Auswahl aus Basisausdrücken
		auswahlAB = Choice.create();
		auswahlAB.addExpression(basisausdruckA);
		auswahlAB.addExpression(basisausdruckB);
		auswahlAC = Choice.create();
		auswahlAC.addExpression(basisausdruckA);
		auswahlAC.addExpression(basisausdruckC);
		// Auswahl aus Auswahl und Basisausdruck
		auswahlABC = Choice.create();
		auswahlABC.addExpression(auswahlAB);
		auswahlABC.addExpression(basisausdruckC);

		// Initialisierung gesamt Test siehe Pdf
		bT = BaseExpression.create('T');
		bH = BaseExpression.create('H');
		bX = BaseExpression.create('X');
		bY = BaseExpression.create('Y');
		bY.setIterated(true);
		bZ = BaseExpression.create('Z');
		a2 = Choice.create();
		a2.addExpression(bX);
		a2.addExpression(bY);
		a2.addExpression(bZ);
		k = Sequence.create();
		k.addExpression(bT);
		k.addExpression(bH);
		a1 = Choice.create();
		a1.setIterated(true);
		a1.addExpression(k);
		a1.addExpression(a2);
	}

	@Test
	public void testContains() {
		// Reflexive Contains Basisausdrücke. True
		assertTrue(basisausdruckA.contains(basisausdruckA));
		assertTrue(basisausdruckB.contains(basisausdruckB));

		// Reflexive Contains Basisausdrücke. False
		assertFalse(basisausdruckA.contains(basisausdruckB));
		assertFalse(basisausdruckB.contains(basisausdruckA));

		// Direct Contains True
		assertTrue(konkatenationAB.contains(basisausdruckA));
		assertTrue(konkatenationAB.contains(basisausdruckB));
		assertTrue(konkatenationAC.contains(basisausdruckA));
		assertTrue(konkatenationAC.contains(basisausdruckC));
		assertTrue(konkatenationABC.contains(basisausdruckC));

		// Indirect Contains True
		assertTrue(konkatenationABC.contains(basisausdruckA));
		assertTrue(konkatenationABC.contains(basisausdruckB));

		// Contains False
		assertFalse(konkatenationAB.contains(basisausdruckC));
		assertFalse(konkatenationABC.contains(konkatenationAC));
		assertFalse(konkatenationAC.contains(basisausdruckB));

	}

	@Test
	public void testAddExpression() {
		// Exception bei Basisausdrücken
		try {
			basisausdruckA.addExpression(basisausdruckB);
			fail();
		} catch (final Exception e) {
		}
		try {
			basisausdruckB.addExpression(konkatenationAB);
			fail();
		} catch (final Exception e1) {
		}

		// Exception bei Konkatenation
		try {
			konkatenationAB.addExpression(konkatenationABC);
			fail();
		} catch (final Exception e) {
		}
		// Exception bei Auswahl
		try {
			auswahlAB.addExpression(auswahlABC);
			fail();
		} catch (final Exception e) {
		}
		// Test ob sie wirklich dann Teil der Collection sind.
		final Sequence testKonkatenation = Sequence.create();
		try {
			testKonkatenation.addExpression(auswahlAB);
			testKonkatenation.addExpression(auswahlAC);
			testKonkatenation.addExpression(konkatenationAB);
			testKonkatenation.addExpression(konkatenationAC);
		} catch (final Exception e) {
			fail();
		}
		assertTrue(testKonkatenation.contains(auswahlAB));
		assertTrue(testKonkatenation.contains(auswahlAC));
		assertTrue(testKonkatenation.contains(konkatenationAB));
		assertTrue(testKonkatenation.contains(konkatenationAC));

	}

	@Test
	/**
	 * Testing toAutomaton and toAutomaton with iterated flag true.
	 */
	public void testToAutomatBasisAusdruck() {
		// True
		assertTrue(basisausdruckA.toAutomaton().recognizes("a"));
		assertTrue(basisausdruckB.toAutomaton().recognizes("b"));
		assertTrue(basisausdruckC.toAutomaton().recognizes("c"));

		// False
		assertFalse(basisausdruckA.toAutomaton().recognizes("b"));
		assertFalse(basisausdruckB.toAutomaton().recognizes("c"));
		assertFalse(basisausdruckC.toAutomaton().recognizes("a"));

		// setUp A --> A*
		basisausdruckA.setIterated(true);
		basisausdruckB.setIterated(true);
		basisausdruckC.setIterated(true);

		// True
		assertTrue(basisausdruckA.toAutomaton().recognizes("aaaaaaaa"));
		assertTrue(basisausdruckB.toAutomaton().recognizes("bbbb"));
		assertTrue(basisausdruckC.toAutomaton().recognizes("ccc"));

		// False
		assertFalse(basisausdruckA.toAutomaton().recognizes("aaaaabaaaa"));
		assertFalse(basisausdruckB.toAutomaton().recognizes("bbbbbXbbb"));
		assertFalse(basisausdruckC.toAutomaton().recognizes("ccccccbccc"));

		// Recognizes Empty String

		assertFalse(basisausdruckA.toAutomaton().recognizes(""));
		assertFalse(basisausdruckB.toAutomaton().recognizes(""));
		assertFalse(basisausdruckC.toAutomaton().recognizes(""));

		basisausdruckA.setRecognizesEmptyString(true);
		basisausdruckB.setRecognizesEmptyString(true);
		basisausdruckC.setRecognizesEmptyString(true);

		assertTrue(basisausdruckA.toAutomaton().recognizes(""));
		assertTrue(basisausdruckB.toAutomaton().recognizes(""));
		assertTrue(basisausdruckC.toAutomaton().recognizes(""));

	}

	@Test
	/**
	 * Testet to Automaton bei zwei Konkatenationen. konkatenationAB ist die
	 * Konkatenation der zwei Basisausdrücke a und b. konkatenationAC ist die
	 * Konkatenation der zwei Basisausdrücke a und c. Dann werden beide
	 * Konkatenationen auf Iterated true gesetzt, sodass dann von
	 * konkatenationAB zum Beispiel "abab" zusätzlich erkannt wird.
	 */
	public void testToAutomatonSimpleKonkatenation() {

		// True
		assertTrue(konkatenationAB.toAutomaton().recognizes("ab"));
		assertTrue(konkatenationAC.toAutomaton().recognizes("ac"));
		assertTrue(konkatenationABC.toAutomaton().recognizes("abc"));

		// False
		assertFalse(konkatenationAB.toAutomaton().recognizes("aa"));
		assertFalse(konkatenationAB.toAutomaton().recognizes("ba"));
		assertFalse(konkatenationAB.toAutomaton().recognizes("bb"));

		assertFalse(konkatenationAC.toAutomaton().recognizes("aa"));
		assertFalse(konkatenationAC.toAutomaton().recognizes("ca"));
		assertFalse(konkatenationAC.toAutomaton().recognizes("cc"));

		assertFalse(konkatenationABC.toAutomaton().recognizes("ab"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("bc"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("abcZ"));

		// zB. AB --> AB*
		konkatenationAB.setIterated(true);
		konkatenationAC.setIterated(true);
		konkatenationABC.setIterated(true);

		// True konkatenationAB
		assertTrue(konkatenationAB.toAutomaton().recognizes("ab"));
		assertTrue(konkatenationAB.toAutomaton().recognizes("abab"));
		assertTrue(konkatenationAB.toAutomaton().recognizes("ababab"));
		// True konkatenationAC
		assertTrue(konkatenationAC.toAutomaton().recognizes("ac"));
		assertTrue(konkatenationAC.toAutomaton().recognizes("acac"));
		assertTrue(konkatenationAC.toAutomaton().recognizes("acacac"));
		// true konkatenationABC
		assertTrue(konkatenationABC.toAutomaton().recognizes("abc"));
		assertTrue(konkatenationABC.toAutomaton().recognizes("abcabc"));
		assertTrue(konkatenationABC.toAutomaton().recognizes("abcabcabc"));
		// False konkatenationAB
		assertFalse(konkatenationAB.toAutomaton().recognizes(""));
		assertFalse(konkatenationAB.toAutomaton().recognizes("ababaXb"));
		assertFalse(konkatenationAB.toAutomaton().recognizes("123"));
		assertFalse(konkatenationAB.toAutomaton().recognizes("??§&"));
		// False konkatenationAC
		assertFalse(konkatenationAC.toAutomaton().recognizes(""));
		assertFalse(konkatenationAC.toAutomaton().recognizes("acZacac"));
		assertFalse(konkatenationAC.toAutomaton().recognizes("<>^^2"));
		assertFalse(konkatenationAC.toAutomaton().recognizes("askdasdkasdjkasjldjkaskljd"));
		// False konkatenationABC
		assertFalse(konkatenationABC.toAutomaton().recognizes(""));
		assertFalse(konkatenationABC.toAutomaton().recognizes("acZacac"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("abcabccccB"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("aabbcc"));

		// Recognizes Empty String

		assertFalse(konkatenationAB.toAutomaton().recognizes(""));
		assertFalse(konkatenationAC.toAutomaton().recognizes(""));
		assertFalse(konkatenationABC.toAutomaton().recognizes(""));

		konkatenationAB.setRecognizesEmptyString(true);
		konkatenationAC.setRecognizesEmptyString(true);
		konkatenationABC.setRecognizesEmptyString(true);

		assertTrue(konkatenationAB.toAutomaton().recognizes(""));
		assertTrue(konkatenationAC.toAutomaton().recognizes(""));
		assertTrue(konkatenationABC.toAutomaton().recognizes(""));

		konkatenationAB.setRecognizesEmptyString(false);
		konkatenationAC.setRecognizesEmptyString(false);
		konkatenationABC.setRecognizesEmptyString(false);
		basisausdruckC.setRecognizesEmptyString(true);
		
		assertTrue(konkatenationABC.toAutomaton().recognizes("abcabcab"));
		assertTrue(konkatenationABC.toAutomaton().recognizes("abababc"));
		assertTrue(konkatenationABC.toAutomaton().recognizes("ababab"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("ababccccccab"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("ababbab"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("ababcbc"));
		assertFalse(konkatenationABC.toAutomaton().recognizes("bbcbc"));

		basisausdruckA.setRecognizesEmptyString(true);

		assertTrue(konkatenationABC.toAutomaton().recognizes("bcbcb"));
		assertTrue(konkatenationABC.toAutomaton().recognizes("bbbc"));
		assertTrue(konkatenationABC.toAutomaton().recognizes("bbcb"));
	}

	@Test
	/**
	 * 
	 */
	public void testToAutomatonAuswahl() {

		// True
		assertTrue(auswahlAB.toAutomaton().recognizes("a"));
		assertTrue(auswahlAB.toAutomaton().recognizes("b"));

		assertTrue(auswahlAC.toAutomaton().recognizes("a"));
		assertTrue(auswahlAC.toAutomaton().recognizes("c"));

		assertTrue(auswahlABC.toAutomaton().recognizes("a"));
		assertTrue(auswahlABC.toAutomaton().recognizes("b"));
		assertTrue(auswahlABC.toAutomaton().recognizes("c"));

		// False
		assertFalse(auswahlAB.toAutomaton().recognizes("c"));
		assertFalse(auswahlAB.toAutomaton().recognizes("ab"));
		assertFalse(auswahlAB.toAutomaton().recognizes("dd"));

		assertFalse(auswahlAC.toAutomaton().recognizes("ac"));
		assertFalse(auswahlAC.toAutomaton().recognizes("ca"));
		assertFalse(auswahlAC.toAutomaton().recognizes("???"));

		assertFalse(auswahlABC.toAutomaton().recognizes("abc"));
		assertFalse(auswahlABC.toAutomaton().recognizes("ACD"));
		assertFalse(auswahlABC.toAutomaton().recognizes("Thorben"));

		// zB. AB --> AB*
		auswahlAB.setIterated(true);
		auswahlAC.setIterated(true);
		auswahlABC.setIterated(true);

		// True auswahlAB
		assertTrue(auswahlAB.toAutomaton().recognizes("a"));
		assertTrue(auswahlAB.toAutomaton().recognizes("aaaaa"));
		assertTrue(auswahlAB.toAutomaton().recognizes("b"));
		assertTrue(auswahlAB.toAutomaton().recognizes("bbbb"));
		assertTrue(auswahlAB.toAutomaton().recognizes("ab"));
		assertTrue(auswahlAB.toAutomaton().recognizes("abababab"));
		// True auswahlAC
		assertTrue(auswahlAC.toAutomaton().recognizes("a"));
		assertTrue(auswahlAC.toAutomaton().recognizes("aaaa"));
		assertTrue(auswahlAC.toAutomaton().recognizes("c"));
		assertTrue(auswahlAC.toAutomaton().recognizes("ccc"));
		assertTrue(auswahlAC.toAutomaton().recognizes("ac"));
		assertTrue(auswahlAC.toAutomaton().recognizes("acac"));
		// true konkatenationABC
		assertTrue(auswahlABC.toAutomaton().recognizes("a"));
		assertTrue(auswahlABC.toAutomaton().recognizes("aaaa"));
		assertTrue(auswahlABC.toAutomaton().recognizes("b"));
		assertTrue(auswahlABC.toAutomaton().recognizes("bbbb"));
		assertTrue(auswahlABC.toAutomaton().recognizes("c"));
		assertTrue(auswahlABC.toAutomaton().recognizes("ccccc"));
		assertTrue(auswahlABC.toAutomaton().recognizes("abc"));
		assertTrue(auswahlABC.toAutomaton().recognizes("abcabcabcabc"));
		// False auswahlAB
		assertFalse(auswahlAB.toAutomaton().recognizes(""));
		assertFalse(auswahlAB.toAutomaton().recognizes("ababaXb"));
		assertFalse(auswahlAB.toAutomaton().recognizes("123"));
		assertFalse(auswahlAB.toAutomaton().recognizes("aabbc"));
		// False auswahlAC
		assertFalse(auswahlAC.toAutomaton().recognizes(""));
		assertFalse(auswahlAC.toAutomaton().recognizes("acZacac"));
		assertFalse(auswahlAC.toAutomaton().recognizes("<>^^2"));
		assertFalse(auswahlAC.toAutomaton().recognizes("aaccd"));
		// False auswahlABC
		assertFalse(auswahlABC.toAutomaton().recognizes(""));
		assertFalse(auswahlABC.toAutomaton().recognizes("acZacac"));
		assertFalse(auswahlABC.toAutomaton().recognizes("abcabccccB"));
		assertFalse(auswahlABC.toAutomaton().recognizes("aabbccd"));

		assertFalse(auswahlAB.toAutomaton().recognizes(""));
		assertFalse(auswahlAC.toAutomaton().recognizes(""));
		assertFalse(auswahlABC.toAutomaton().recognizes(""));

		auswahlAB.setRecognizesEmptyString(true);
		auswahlAC.setRecognizesEmptyString(true);
		auswahlABC.setRecognizesEmptyString(true);

		assertTrue(auswahlAB.toAutomaton().recognizes(""));
		assertTrue(auswahlAC.toAutomaton().recognizes(""));
		assertTrue(auswahlABC.toAutomaton().recognizes(""));

	}

	@Test
	public void RecognizesEmptyString() {
		basisausdruckA.setRecognizesEmptyString(true);
		basisausdruckB.setRecognizesEmptyString(true);
		assertTrue(konkatenationAB.toAutomaton().recognizes(""));
		assertTrue(auswahlAB.toAutomaton().recognizes(""));

		basisausdruckA.setRecognizesEmptyString(false);

		assertFalse(konkatenationAB.toAutomaton().recognizes(""));
		assertTrue(auswahlAB.toAutomaton().recognizes(""));

		basisausdruckB.setRecognizesEmptyString(false);

		assertFalse(konkatenationAB.toAutomaton().recognizes(""));
		assertFalse(auswahlAB.toAutomaton().recognizes(""));

		basisausdruckA.setRecognizesEmptyString(false);
		basisausdruckB.setRecognizesEmptyString(true);
		assertTrue(konkatenationAB.toAutomaton().recognizes("a"));

		basisausdruckA.setRecognizesEmptyString(true);
		basisausdruckB.setRecognizesEmptyString(false);
		assertTrue(konkatenationAB.toAutomaton().recognizes("b"));

	}

	@Test
	/**
	 * Testet den erstellten RegEx aus dem Pdf
	 */
	public void testBigRegEx() {
		// True
		assertTrue(a1.toAutomaton().recognizes("TH"));
		assertTrue(a1.toAutomaton().recognizes("THXX"));
		assertTrue(a1.toAutomaton().recognizes("THZZ"));
		assertTrue(a1.toAutomaton().recognizes("THTHTH"));
		assertTrue(a1.toAutomaton().recognizes("XXX"));
		// False
		assertFalse(a1.toAutomaton().recognizes("XYXZXTX"));
		assertFalse(a1.toAutomaton().recognizes("XXXTZ"));
		assertFalse(a1.toAutomaton().recognizes("XTHQXX"));
	}
}
