package util.automaton;

public class Transition {
	
	private final char input;
	private State before;
	private State after;
	
	public static Transition create(char input, State before, State after) {
		return new Transition(input, before, after);
	}
	
	private Transition(char input, State before, State after) {
		this.input = input;
		this.before = before;
		this.after = after;
	}
	
	public boolean isInput(char input) {
		return this.input == input ? true : false;
	}

	public State getAfter() {
		return this.after;
	}

	public State getBefore() {
		return this.before;
	}
	
	public char getInput() {
		return this.input;
	}

	public void setAfter(State state) {
		this.after = state;
	}
	
	public void setBefore(State state) {
		this.before = state;
	}

	@Override
	public String toString() {
		return this.before.toString() + " " + this.input+ " " + this.after.toString();
	}

}
