package util.automaton;

import java.util.HashSet;
import java.util.Iterator;

public class State {

	private Automat automat;

	public static State create(Automat automat) {
		return new State(automat);
	}

	private State(Automat automat) {
		this.automat = automat;
	}

	public void addTransition(char c, State after) {
		this.automat.addTransition(Transition.create(c, this, after));
		this.automat.addState(this);
		this.automat.addState(after);
	}

	@Override
	public boolean equals(Object arg) {
		if (arg instanceof State) {
			final State argAsState = (State) arg;
			return this == argAsState;
		}
		return false;
	}

	public void setAutomat(Automat automat) {
		this.automat = automat;

	}

	/**
	 * Schnittstelle f�rs reachsEndState die eine leere Menge erzeugt und als
	 * Paramter and die Methode �bergibt, die die eigentliche Arbeit erledigt.
	 */
	public HashSet<State> reachsEndState() {
		return this.reachsEndState(new HashSet<State>());
	}
	/**
	 * @return eine leere Menge, falls der receiver den Endzustand nicht �ber Transitionen erreicht. 
	 * Gibt die Menge aller Zust�nde zur�ck die auf dem Weg des receivers zum Endzustand sind einschlie�lich dem Endzustand zur�ck.
	 */
	private HashSet<State> reachsEndState(HashSet<State> visitedStates) {
		final HashSet<State> returnedStates = new HashSet<State>();
		if (this.equals(this.automat.getEndZustand())) {
			returnedStates.add(this);
			// Schleifen um den Endzustand erkennen.
			final HashSet<State> nextStatesFromEnd = this.nextStatesWithOut(visitedStates);
			final Iterator<State> iteratorEnd = nextStatesFromEnd.iterator();
			while(iteratorEnd.hasNext()) {
				final State current = iteratorEnd.next();
				returnedStates.addAll(current.reachsEndState(visitedStates));
			}
			return returnedStates; // Wenn ich ein Endzustand bin gebe ich mich
									// als Singleton zur�ck.
		}
		visitedStates.add(this); // Ich trage mich in die Liste der besuchten
									// Zust�nde ein.
		final HashSet<State> nextStates = this.nextStatesWithOut(visitedStates);
		final Iterator<State> iterator = nextStates.iterator();
		while (iterator.hasNext()) {
			final State current = iterator.next();
			returnedStates.addAll(current.reachsEndState(visitedStates));
		}
		if (!returnedStates.isEmpty()) {
			returnedStates.add(this);
		}
		return returnedStates;
	}

	/**
	 * Gibt die Menge aller Zust�nde zur�ck ohne die Zust�nde <withoutThese> ,
	 * in die man vom receiver mit einer Transition kommt. Delegiert and die
	 * Methode nextStatesWithOut von Automat, indem es die Paramter richtig
	 * �bergibt.
	 */
	private HashSet<State> nextStatesWithOut(HashSet<State> withoutThese) {
		return this.automat.nextStatesWithOut(this, withoutThese);
	}

}
