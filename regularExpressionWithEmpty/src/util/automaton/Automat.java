package util.automaton;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class Automat {

	private State anfangsZustand;
	private State endZustand;
	private final Collection<State> states;
	private final Collection<Transition> delta;
	private boolean recognizesEmptyString;

	public void setRecognizesEmptyString(boolean recognizesEmptyString) {
		this.recognizesEmptyString = recognizesEmptyString;

	}

	public boolean getRecognizesEmptyString() {
		return this.recognizesEmptyString;
	}

	public State getEndZustand() {
		return endZustand;
	}

	/**
	 * Wirft ein Error falls der �bergebene endZustand gleich dem aktuellen
	 * Anfangszustand ist. Normalerweise wird das Feld <endZustand> auf den
	 * �bergebenen endZustand gesetzt.
	 */
	private void setEndZustand(State endZustand) {
		if (this.anfangsZustand.equals(endZustand))
			throw new Error();
		this.endZustand = endZustand;
	}

	public State getAnfangsZustand() {
		return anfangsZustand;
	}

	public Collection<Transition> getDelta() {
		return this.delta;
	}

	public State createState() {
		final State state = State.create(this);
		this.addState(state);
		return state;
	}

	/**
	 * Adds the given state to the Field <states> which is a Collection<State>.
	 */
	public void addState(State state) {
		this.states.add(state);
	}

	/**
	 * Adds the given transition to the Field <delta> which is a Collection
	 * <transition>.
	 */
	public void addTransition(Transition transition) {
		this.delta.add(transition);
	}

	/**
	 * Gibt den Nullautomaten zur�ck der keine W�rter erkennt.
	 */
	public static Automat create() {
		return new Automat();
	}

	/**
	 * Gibt den Identit�sautomaten zur�ck.
	 */
	public static Automat createId() {
		final Automat createdAutomat = new Automat();
		createdAutomat.endZustand = createdAutomat.anfangsZustand;
		return createdAutomat;
	}

	/**
	 * @return
	 */
	public static Automat createBasic(char c, boolean recognizesEmptyString) {
		final Automat createdAutomat = Automat.create();
		createdAutomat.getAnfangsZustand().addTransition(c, createdAutomat.getEndZustand());
		createdAutomat.recognizesEmptyString = recognizesEmptyString;
		return createdAutomat;
	}

	private Automat() {
		this.anfangsZustand = State.create(this);
		this.endZustand = State.create(this);
		this.states = new HashSet<State>();
		this.delta = new HashSet<Transition>();
		this.recognizesEmptyString = false;
	}

	public boolean recognizes(String input) {
		if (this.recognizesEmptyString && input.isEmpty())
			return true;
		final HashSet<State> startSet = new HashSet<State>();
		startSet.add(anfangsZustand);
		final Configuration start = Configuration.create(input, this, startSet);
		final Configuration finalConfig = start.run();
		return this.containsEndState(finalConfig.getCurrentStates());
	}

	public HashSet<State> nextStates(char input, State state) {
		final HashSet<State> nextStates = new HashSet<State>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getBefore().equals(state)) {
				if (current.isInput(input)) {
					nextStates.add(current.getAfter());
				}
			}

		}
		return nextStates;
	}

	public HashSet<State> nextStates(State state) {
		final HashSet<State> nextStates = new HashSet<State>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getBefore().equals(state)) {
				nextStates.add(current.getAfter());

			}

		}
		return nextStates;
	}

	public boolean containsEndState(Collection<State> states) {
		final Iterator<State> iterator = states.iterator();
		while (iterator.hasNext()) {
			final State current = iterator.next();
			if (current.equals(this.endZustand))
				return true;
		}
		return false;
	}

	/**
	 * Ver�ndert den Empf�nger so, dass er nach der Ausf�hrung der Operation
	 * seine eigene Iteration darstellt.
	 */
	public void iterate() {
		final HashSet<Transition> TransitionsInEndState = this.TransitionsIn(this.endZustand);
		final Iterator<Transition> iterator = TransitionsInEndState.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			current.getBefore().addTransition(current.getInput(), this.anfangsZustand);
		}
	}

	/**
	 * Ver�ndert den Empf�nger e so, dass er nach der Ausf�hrung der Operation
	 * die Parallelschaltung von e und a darstellt.
	 */
	public void choice(Automat a) {
//		if (a.equalsNullAutomaton()) {
//			return;
//		}
		final Iterator<Transition> iteratorEndTransitions = a.delta.iterator();
		while (iteratorEndTransitions.hasNext()) {
			final Transition current = iteratorEndTransitions.next();
			// Alle Transitionen von a in den Endzustand gehen jetzt in den
			// Endzustand von this.
			if(current.getAfter().equals(a.getEndZustand())) {
			current.setAfter(this.endZustand);
			}
			// Alle Transitionen von a's Anfangszustand kommen jetzt vom
			// Anfangszustand von this.
			if(current.getBefore().equals(a.getAnfangsZustand())) {
				current.setBefore(this.anfangsZustand);
			}
		}

		this.addStates(a.states);
		this.delta.addAll(a.delta);
		this.states.remove(a.endZustand); // Dunno
		this.states.remove(a.anfangsZustand); // Dunno
		this.recognizesEmptyString = this.recognizesEmptyString || a.recognizesEmptyString;
	}

	/**
	 * Ver�ndert den Empf�nger e so, dass er nach der Ausf�hrung der Operation
	 * die Hintereinanderschaltung von e und danach a darstellt.
	 */
	public void sequence(Automat a) {
		if (this.equalsId()) {
			this.changeTo(a);
			return;
		}
		final HashSet<Transition> TransitionsInEndState = this.TransitionsIn(this.getEndZustand());
		final Iterator<Transition> iterator = TransitionsInEndState.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			current.setAfter(a.getAnfangsZustand());
			if (a.recognizesEmptyString) {
				current.getBefore().addTransition(current.getInput(), a.getEndZustand());
				// this.addTransition(Transition.create(current.getInput(),
				// current.getBefore(), a.getEndZustand()));
			}
		}
		if (this.recognizesEmptyString) {
			final Iterator<Transition> iteratorATrans = a.delta.iterator();
			while (iteratorATrans.hasNext()) {
				final Transition current = iteratorATrans.next();
				if (current.getBefore().equals(a.anfangsZustand)) {
					this.getAnfangsZustand().addTransition(current.getInput(), current.getAfter());
				}
			}
		}
		this.addStates(a.states);
		this.delta.addAll(a.delta);
		this.setEndZustand(a.getEndZustand());
		this.recognizesEmptyString = this.recognizesEmptyString && a.recognizesEmptyString;
	}

	public HashSet<Transition> TransitionsIn(State state) {
		final HashSet<Transition> beforeTransitions = new HashSet<Transition>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getAfter().equals(state)) {
				beforeTransitions.add(current);
			}

		}
		return beforeTransitions;
	}

	private void addStates(Collection<State> states) {
		final Iterator<State> iterator = states.iterator();
		while (iterator.hasNext()) {
			final State current = iterator.next();
			current.setAutomat(this);
			this.states.add(current);
		}

	}

	private void changeTo(Automat a) {
		this.anfangsZustand = a.getAnfangsZustand();
		this.endZustand = a.getEndZustand();
		this.addStates(a.states);
		this.delta.addAll(a.delta);
		this.recognizesEmptyString = a.recognizesEmptyString;
	}

	private boolean equalsId() {
		return this.anfangsZustand.equals(endZustand);
	}

	/**
	 * Wirft alle Zust�nde aus dem Feld <states> die nicht auf dem Weg vom
	 * Anfangszustand zum Endzustand liegen.
	 */
	public void normalise() {
		this.states.retainAll(this.getUsedStates());
	}

	/**
	 * Gibt die Menge aller Zust�nde zur�ck die auf dem Weg vom Anfangszustand
	 * in den Endzustand �ber Transitionen betreten werden.
	 */
	public HashSet<State> getUsedStates() {
		final HashSet<State> states = this.anfangsZustand.reachsEndState();
		return states;
	}
	
	/**
	 * Return the number of states considering the receiver automaton.
	 */
	public int countStates() {
		return this.states.size();
	}

	/**
	 * Gibt die Menge aller Zust�nde zur�ck ohne den Zust�nden <withoutThese>,
	 * die durch eine Transition mit <currentState> verbunden sind.
	 */
	public HashSet<State> nextStatesWithOut(State currentState, HashSet<State> withoutThese) {
		final HashSet<State> states = this.nextStates(currentState);
		states.removeAll(withoutThese);
		return states;
	}

}
