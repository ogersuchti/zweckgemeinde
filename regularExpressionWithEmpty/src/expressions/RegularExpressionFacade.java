package expressions;

import exceptions.AddExpressionException;

public class RegularExpressionFacade {

	public static RegularExpressionFacade create() {
		return new RegularExpressionFacade();
	}

	/**
	 * @return RegularBaseExpression, the language of which is {c} 
	 */
	public RegularExpression createBaseExpression(final char c) {
		System.out.println("Base:" + c);
		return BaseExpression.create(c);
	}

	/**
	 * @return RegularExpression, the language of which is the empty set
	 */
	public RegularExpression createChoiceExpression() {
		System.out.println("Choice");
		return Choice.create();
	}
	/**
	 * @return RegularExpression, the language of which is {""}
	 */
	public RegularExpression createSequenceExpression() {
		System.out.println("Sequence");
		return Sequence.create();
	}

	/**Adds the containee as a direct subexpression to the container. 
	 * Order of addition is significant!
	 * @param container The whole
	 * @param containee The part
	 */
	public void add(final RegularExpression container, final RegularExpression containee) {
		System.out.println("Add");
		try {
			container.addExpression(containee);
		} catch (final AddExpressionException e) {
			System.out.println("YOU MADE A MISTAKE");
		}
	}

	public void setIterated(final RegularExpression expression) {
		System.out.println("iterated");
		expression.setIterated(true);
	}

	public void setOptional(final RegularExpression result) {
		System.out.println("optional");
		result.setRecognizesEmptyString(true);
	}


}
