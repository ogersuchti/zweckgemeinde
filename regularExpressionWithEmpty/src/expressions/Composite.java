package expressions;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import exceptions.CycleException;
import util.automaton.Automat;

public abstract class Composite extends RegularExpression {

	protected Collection<RegularExpression> regEx;

	protected Composite() {
		super();
		this.regEx = new LinkedList<RegularExpression>();
	}

	@Override
	public boolean contains(final RegularExpression regEx) {
		if (this.equals(regEx))
			return true;
		final Iterator<RegularExpression> iterator = this.regEx.iterator();
		while (iterator.hasNext()) {
			final RegularExpression current = iterator.next();
			if (current.contains(regEx)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void addExpression(final RegularExpression regEx) throws CycleException {
		if (regEx.contains(this))
			throw new CycleException();
		this.regEx.add(regEx);
	}

	@Override
	public Automat toAutomaton() {
		final Automat createdAutomat = this.neutralAutomat(); // Neutrales
																// Element
		final Iterator<RegularExpression> iterator = this.regEx.iterator();
		while (iterator.hasNext()) {
			final RegularExpression current = iterator.next();
			this.combine(createdAutomat, current.toAutomaton());
		}
		this.automatonRecognizesEmptyWhenNeeded(createdAutomat);
		this.iterateAutomatonWhenNeeded(createdAutomat);
		return createdAutomat;
	}

	/**
	 * Gibt den neutralen Automaten zur�ck, der mit dem receiver.toAutomaton dann �ber
	 * combine() nichts ver�ndert.
	 * Wenn der receiver Auswahl ist dann der Nullautomat.
	 * Wenn der receiver Konkatenation ist dann der Identit�tsautomat.
	 */
	protected abstract Automat neutralAutomat();
	
	/**
	 * Kombiniert <first> mit <second> abh�ngig vom receiver Datentyp.
	 * Wenn das reiceiver ein Objektexemplar vom Typ Auswahl ist wird die Automatenmethode choice() benutzt.
	 * Wenn das reiceiver ein Objektexemplar vom Typ Konkatenation ist wird die Automatenmethode sequence() benutzt.
	 */
	protected abstract void combine(Automat first, Automat second);

}
