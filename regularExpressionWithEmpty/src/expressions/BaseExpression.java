package expressions;

import exceptions.NoSubstructureException;
import util.automaton.Automat;

public class BaseExpression extends RegularExpression{
	
	private final char c;
	
	public static BaseExpression create(final char c) {
		return new BaseExpression(c);
	}
	
	private BaseExpression(final char c) {
		this.c = c;
	}

	@Override
	public boolean contains(final RegularExpression regEx) {
		return this.equals(regEx);
	}

	@Override
	public void addExpression(final RegularExpression regEx) throws NoSubstructureException {
		throw new NoSubstructureException();
	}

	@Override
	public Automat toAutomaton() {
		final Automat createdAutomat = Automat.createBasic(this.c, this.getRecognizesEmptyString());
		this.iterateAutomatonWhenNeeded(createdAutomat);
		this.automatonRecognizesEmptyWhenNeeded(createdAutomat);
		return createdAutomat;
	}

}
