package expressions;

import exceptions.AddExpressionException;
import util.automaton.Automat;

public abstract class RegularExpression {

	private boolean iterated;
	private boolean recognizesEmptyString;

	/**
	 * @param text
	 * @return true if and only if the text belongs to the language of the
	 *         receiver.
	 */
	public boolean check(final String text) {
		return this.toAutomaton().recognizes(text);
	}

	protected RegularExpression() {
		this.iterated = false; // Make it clear
		this.recognizesEmptyString = false;
	}

	public boolean getIterated() {
		return this.iterated;
	}

	public void setIterated(final boolean iterated) {
		this.iterated = iterated;
	}

	public boolean getRecognizesEmptyString() {
		return this.recognizesEmptyString;
	}

	public void setRecognizesEmptyString(boolean recognizesEmptyString) {
		this.recognizesEmptyString = recognizesEmptyString;
	}

	/**
	 * Gibt den Automaten zur�ck, der die gleichen W�rter wie der regul�re
	 * Ausdr�ck erkennt, auf den diese Methode aufgerufen wird.
	 */
	public abstract Automat toAutomaton();

	/**
	 * Gibt true zur�ck wenn der receiver direkt oder indirekt regEx enth�lt.
	 * Sonst wird false zur�ckgegeben.
	 */
	public abstract boolean contains(final RegularExpression regEx);

	/**
	 * Adds the given regEx as subPart to the receiver RegEx when possible.
	 * Throws CycleException if the Method would lead into a TODO cyclic TODO
	 * structure. Throws NoSubstructure Expression if the receiver has no
	 * substructure.
	 */
	public abstract void addExpression(RegularExpression regEx) throws AddExpressionException;

	/**
	 * Uses the Method iterate() on the given Automat aut when the receiver
	 * RegEx has the field iterated set as true.
	 */
	protected void iterateAutomatonWhenNeeded(Automat aut) {
		if (this.getIterated()) {
			aut.iterate();
		}
	}

	/**
	 * Uses the Method setRecognizesEmptyString() on the given Automat aut when
	 * the receiver RegEx has the field RecognizesEmptyString set as true.
	 */
	protected void automatonRecognizesEmptyWhenNeeded(Automat aut) {
		if (this.getRecognizesEmptyString()) {
			aut.setRecognizesEmptyString(true);
		}
	}

}
