package expressions;

import util.automaton.Automat;

public class Choice extends Composite{
	
	public static Choice create() {
		return new Choice();
	}
	
	private Choice() {
		super();
	}

	@Override
	protected Automat neutralAutomat() {
		return Automat.create();
	}
	
	@Override
	protected void combine(Automat first, Automat second) {
		first.choice(second);
	}

}
