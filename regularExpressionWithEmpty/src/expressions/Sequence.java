package expressions;

import util.automaton.Automat;

public class Sequence extends Composite{
	
	public static Sequence create() {
		return new Sequence();
	}
	
	private Sequence() {
		super();
	}
	
	@Override
	protected Automat neutralAutomat() {
		return Automat.createId();
	}

	@Override
	protected void combine(Automat first, Automat second) {
		first.sequence(second);
	}

}
