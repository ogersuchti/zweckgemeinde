package state;

import model.MaterialList;
import model.Product;

public class MLCached extends State {

	protected final MaterialList matList;

	public MLCached(final MaterialList matList) {
		this.matList = matList;
	}

	@Override
	public void priceChanged(final Product product) {
	}

	@Override
	public void structureChanged(final Product product) {
		product.setState(NothingCached.create());
	}

	@Override
	public int getPrice(final Product product) {
		product.setState( PMLCached.create(this.matList, product.computeOverallPrice()));
		return product.getOverallPrice();
	}

	@Override
	public MaterialList getMaterialList(final Product product) {
		return this.matList;
	}

}
