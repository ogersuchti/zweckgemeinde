package state;

import model.MaterialList;
import model.Product;

public class PMLCached extends MLCached implements IPCached {

	private final PCached myPCached;

	public static PMLCached create(final MaterialList matList, final int price) {
		return new PMLCached(matList, price);
	}

	private PMLCached(final MaterialList matList, final int price) {
		super(matList);
		final PCached Pcached = new PCached(price);
		this.myPCached = Pcached;
//		this.myPCached.setThis(this); TODO
	}

	@Override
	public void priceChanged(final Product product) {
		product.setState(new MLCached(this.matList));
	}

	@Override
	public int getPrice(final Product product) {
		return this.myPCached.getPrice(product);
	}

	@Override
	public int getCachedPrice() {
		return this.myPCached.getCachedPrice();
	}

	@Override
	public void setCachedPrice(final int newprice) {
		this.myPCached.setCachedPrice(newprice);
	}

}
