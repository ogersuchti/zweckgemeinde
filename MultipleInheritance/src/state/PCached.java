package state;

import model.MaterialList;
import model.Product;

public class PCached extends State implements IPCached {

	private int price;
//	private PMLCached This; TODO

	public PCached(final int price) {
		this.price = price;
	}

	@Override
	public void priceChanged(final Product product) {
		product.setState(NothingCached.create());
	}

	@Override
	public void structureChanged(final Product product) {
		product.setState(NothingCached.create());
	}

	@Override
	public int getCachedPrice() {
		return this.price;
	}

	@Override
	public void setCachedPrice(final int newprice) {
		this.price = newprice;
	}

	@Override
	public MaterialList getMaterialList(final Product product) {
		product.setState(PMLCached.create(product.computeMaterialList(), this.price));
		return this.getMaterialList(product);
	}

	@Override
	public int getPrice(final Product product) {
		return this.getCachedPrice();
	}
	
//	public PMLCached getThis() { TODO
//		return this.This;
//	}
//
//
//	public void setThis(final PMLCached pmlCached) {
//		this.This = pmlCached;
//	}

}
