package state;

import model.MaterialList;
import model.Product;

public interface IPCached{

	public int getCachedPrice();
	
	public void setCachedPrice(int newprice);

	public void priceChanged(final Product product);

	public void structureChanged(final Product product);

	public MaterialList getMaterialList(final Product product);

}
