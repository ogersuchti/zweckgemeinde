package util;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class Observee {

	protected Collection<Observer> currentObservers;
	
	protected Observee() {
		this.currentObservers = new LinkedList<Observer>();
	}
    /**
     * 
     * @param observer
     */
	public void register(Observer observer) {
		this.currentObservers.add(observer); // TODO check what if already contains etc
	}

	public void deregister(Observer observer) {
		this.currentObservers.remove(observer); // TODO check what if contains twice etc
	}

	public void notifyObservers(Event e) {

		final Iterator<Observer> iterator = this.currentObservers.iterator();
		while (iterator.hasNext()) {
			final Observer current = iterator.next();
			current.update(e);
		}
	}

}
