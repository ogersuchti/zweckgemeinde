package util;

public class PriceChangedEvent implements Event {
	
	private static PriceChangedEvent thePriceChangedEvent;
	
	/**
	 * returns the singleton PriceChangedEvent
	 */
	public static PriceChangedEvent create() {
		if(thePriceChangedEvent == null) {
			thePriceChangedEvent = new PriceChangedEvent();
		}
		return thePriceChangedEvent;
	}
	
	private PriceChangedEvent() {
	}

	@Override
	public void accept(EventVisitor visitor) {
		visitor.handle(this);

	}

}
