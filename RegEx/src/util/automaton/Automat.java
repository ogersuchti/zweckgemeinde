package util.automaton;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class Automat {

	private State anfangsZustand;
	private State endZustand;
	private final Collection<State> states;
	private final Collection<Transition> delta;

	public State getEndZustand() {
		return endZustand;
	}

	/**
	 * Wirft ein Error falls der �bergebene endZustand gleich dem aktuellen
	 * Anfangszustand ist. Normalerweise wird das Feld <endZustand> auf den
	 * �bergebenen endZustand gesetzt.
	 */
	private void setEndZustand(State endZustand) {
		if (this.anfangsZustand.equals(endZustand))
			throw new Error();
		this.endZustand = endZustand;
	}

	public State getAnfangsZustand() {
		return anfangsZustand;
	}

	public Collection<Transition> getDelta() {
		return this.delta;
	}

	public State createState() {
		final State state = State.create(this);
		this.addState(state);
		return state;
	}

	/**
	 * Adds the given state to the Field <states> which is a Collection<State>.
	 */
	public void addState(State state) {
		this.states.add(state);
	}

	/**
	 * Adds the given transition to the Field <delta> which is a Collection
	 * <transition>.
	 */
	public void addTransition(Transition transition) {
		this.delta.add(transition);
	}

	/**
	 * Gibt den Nullautomaten zur�ck der keine W�rter erkennt.
	 */
	public static Automat create() {
		return new Automat();
	}

	/**
	 * Gibt den Identit�sautomaten zur�ck.
	 */
	public static Automat createId() {
		final Automat createdAutomat = new Automat();
		createdAutomat.endZustand = createdAutomat.anfangsZustand;
		return createdAutomat;
	}
	
	/**
	 * @return
	 */
	public static Automat createBasic(char c) {
		final Automat createdAutomat = Automat.create();
		createdAutomat.getAnfangsZustand().addTransition(c, createdAutomat.getEndZustand());
		return createdAutomat;
	}

	private Automat() {
		this.anfangsZustand = State.create(this);
		this.endZustand = State.create(this);
		this.states = new HashSet<State>();
		this.delta = new HashSet<Transition>();
	}

	public boolean recognizes(String input) {
		final HashSet<State> startSet = new HashSet<State>();
		startSet.add(anfangsZustand);
		final Configuration start = Configuration.create(input, this, startSet);
		final Configuration finalConfig = start.run();
		return this.containsEndState(finalConfig.getCurrentStates());
	}

	public HashSet<State> nextStates(char input, State state) {
		final HashSet<State> nextStates = new HashSet<State>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getBefore().equals(state)) {
				if (current.isInput(input)) {
					nextStates.add(current.getAfter());
				}
			}

		}
		return nextStates;
	}

	public HashSet<State> nextStates(State state) {
		final HashSet<State> nextStates = new HashSet<State>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getBefore().equals(state)) {
				nextStates.add(current.getAfter());

			}

		}
		return nextStates;
	}

	public boolean containsEndState(Collection<State> states) {
		final Iterator<State> iterator = states.iterator();
		while (iterator.hasNext()) {
			final State current = iterator.next();
			if (current.equals(this.endZustand))
				return true;
		}
		return false;
	}

	/**
	 * Ver�ndert den Empf�nger so, dass er nach der Ausf�hrung der Operation
	 * seine eigene Iteration darstellt.
	 */
	public void iterate() {
		final HashSet<Transition> TransitionsInEndState = this.TransitionsIn(this.endZustand);
		final Iterator<Transition> iterator = TransitionsInEndState.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			current.getBefore().addTransition(current.getInput(), this.anfangsZustand);
		}
	}

	/**
	 * Ver�ndert den Empf�nger e so, dass er nach der Ausf�hrung der Operation
	 * die Parallelschaltung von e und a darstellt.
	 */
	public void choice(Automat a) {
		if (a.equalsNullAutomaton()) {
			return;
		}
		// Alle Transitionen von a in den Endzustand gehen jetzt in den
		// Endzustand von this.
		final HashSet<Transition> transitionsInEndState = a.TransitionsIn(a.endZustand);
		final Iterator<Transition> iteratorEndTransitions = transitionsInEndState.iterator();
		while (iteratorEndTransitions.hasNext()) {
			final Transition current = iteratorEndTransitions.next();
			current.setAfter(this.endZustand);
		}
		// Alle Transitionen vom Anfangszustand von a fangen jetzt beim
		// Anfangszustand von this an.
		final HashSet<Transition> transitionsFromBeginnState = a.TransitionsFrom(a.anfangsZustand);
		final Iterator<Transition> iteratorBeginngTransitions = transitionsFromBeginnState.iterator();
		while (iteratorBeginngTransitions.hasNext()) {
			final Transition current = iteratorBeginngTransitions.next();
			current.setBefore(this.anfangsZustand);
		}

		this.addStates(a.states);
		this.delta.addAll(a.delta);
		this.states.remove(a.endZustand); // Dunno
		this.states.remove(a.anfangsZustand); // Dunno
	}

	/**
	 * Ver�ndert den Empf�nger e so, dass er nach der Ausf�hrung der Operation
	 * die Hintereinanderschaltung von e und danach a darstellt.
	 */
	public void sequence(Automat a) {
		if (this.equalsId()) {
			this.changeTo(a);
			return;
		}
		final HashSet<Transition> TransitionsInEndState = this.TransitionsIn(this.getEndZustand());
		final Iterator<Transition> iterator = TransitionsInEndState.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			current.setAfter(a.getAnfangsZustand());
		}
		this.addStates(a.states);
		this.delta.addAll(a.delta);
		this.setEndZustand(a.getEndZustand());
	}

	public HashSet<Transition> TransitionsIn(State state) {
		final HashSet<Transition> beforeTransitions = new HashSet<Transition>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getAfter().equals(state)) {
				beforeTransitions.add(current);
			}

		}
		return beforeTransitions;
	}

	private HashSet<Transition> TransitionsFrom(State state) {
		final HashSet<Transition> afterTransitions = new HashSet<Transition>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getBefore().equals(state)) {
				afterTransitions.add(current);
			}

		}
		return afterTransitions;
	}

	private void addStates(Collection<State> states) {
		final Iterator<State> iterator = states.iterator();
		while (iterator.hasNext()) {
			final State current = iterator.next();
			current.setAutomat(this);
			this.states.add(current);
		}

	}

	private void changeTo(Automat a) {
		this.anfangsZustand = a.getAnfangsZustand();
		this.endZustand = a.getEndZustand();
		this.addStates(a.states);
		this.delta.addAll(a.delta);
	}

	private boolean equalsNullAutomaton() {
		return this.delta.isEmpty() && this.states.isEmpty() && !this.anfangsZustand.equals(endZustand);
	}

	private boolean equalsId() {
		return this.anfangsZustand.equals(endZustand);
	}

	/**
	 * Wirft alle Zust�nde aus dem Feld <states> die nicht auf dem Weg vom
	 * Anfangszustand zum Endzustand liegen.
	 */
	public void normalise() {
		this.states.retainAll(this.getUsedStates());
	}

	/**
	 * Gibt die Menge aller Zust�nde zur�ck die auf dem Weg vom Anfangszustand
	 * in den Endzustand �ber Transitionen betreten werden.
	 */
	public HashSet<State> getUsedStates() {
		final HashSet<State> states = this.anfangsZustand.reachsEndState();
		return states;
	}
	
	
	/**
	 * Gibt die Menge aller Zust�nde zur�ck ohne den Zust�nden <withoutThese>, die durch eine Transition mit <currentState> verbunden sind.
	 */
	public HashSet<State> nextStatesWithOut(State currentState, HashSet<State> withoutThese) {
		final HashSet<State> states = this.nextStates(currentState);
		states.removeAll(withoutThese);
		return states;
	}

}
