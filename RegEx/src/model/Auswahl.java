package model;

import java.util.Iterator;

import util.automaton.Automat;

public class Auswahl extends Composite{
	
	public static Auswahl create() {
		return new Auswahl();
	}
	
	private Auswahl() {
		super();
	}

	@Override
	public Automat toAutomaton() {
		final Automat createdAutomat = Automat.create(); // Nullautomat erkennt nichts ! --> Neutrales Element
		final Iterator<RA> iterator = this.regEx.iterator();
		while(iterator.hasNext()) {
			final RA current = iterator.next();
			createdAutomat.choice(current.toAutomaton());
		}
		this.iterateWhenNeeded(createdAutomat);
		return createdAutomat;
	}

}
