package model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class Composite extends RA {

	protected Collection<RA> regEx;

	protected Composite() {
		super();
		this.regEx = new LinkedList<RA>();
	}

	@Override
	public boolean contains(final RA regEx) {
		if (this.equals(regEx))
			return true;
		final Iterator<RA> iterator = this.regEx.iterator();
		while (iterator.hasNext()) {
			final RA current = iterator.next();
			if (current.contains(regEx)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void addExpression(final RA regEx) throws Exception {
		if (regEx.contains(this))
			throw new CycleExpeption();
		this.regEx.add(regEx);
	}

}
