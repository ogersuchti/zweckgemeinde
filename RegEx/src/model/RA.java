package model;

import util.automaton.Automat;

public abstract class RA {

	private boolean iterated;

	protected RA() {
		this.iterated = false;
	}

	public abstract Automat toAutomaton();

	public abstract boolean contains(final RA regEx);

	public abstract void addExpression(RA regEx) throws Exception;

	public boolean getIterated() {
		return this.iterated;
	}

	public void setIterated(final boolean iterated) {
		this.iterated = iterated;
	}

	protected void iterateWhenNeeded(Automat aut) {
		if (this.getIterated()) {
			aut.iterate();
		}
	}

}
