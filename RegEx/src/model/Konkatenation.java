package model;

import java.util.Iterator;

import util.automaton.Automat;

public class Konkatenation extends Composite{
	
	public static Konkatenation create() {
		return new Konkatenation();
	}
	
	private Konkatenation() {
		super();
	}

	@Override
	public Automat toAutomaton() {
		final Automat createdAutomat = Automat.createId();
		final Iterator<RA> iterator = this.regEx.iterator();
		while(iterator.hasNext()) {
			final RA current = iterator.next();
			createdAutomat.sequence(current.toAutomaton());
		}
		this.iterateWhenNeeded(createdAutomat);
		return createdAutomat;
	}

}
