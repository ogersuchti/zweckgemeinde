package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import automaton.Automat;
import automaton.NotRecognizedWordException;
import automaton.State;
import automaton.Transition;

/**
 * Testet die Implementierung endlicher Automaten mit Ausgabe anhand des
 * Automaten aus testResources/TestAutomat1.pdf
 */
public class TestAutomaton1 {

	private Automat testAutomat1;

	@Before
	/**
	 * Baut den Automaten für das Testen auf. Siehe
	 * testResources/TestAutomat1.pdf für das Transitionsdiagramm des Automaten.
	 */
	public void setUp() {
		this.testAutomat1 = Automat.create();
		State z1 = this.testAutomat1.getStartState();
		State z2 = this.testAutomat1.createState();
		State z3 = this.testAutomat1.createState();
		State z4 = this.testAutomat1.getEndState();
		this.testAutomat1.addTransition(Transition.create('a', 'e', z1, z2));
		this.testAutomat1.addTransition(Transition.create('a', 'f', z1, z3));
		this.testAutomat1.addTransition(Transition.create('a', 'h', z3, z3));
		this.testAutomat1.addTransition(Transition.create('a', 'x', z2, z4));
		this.testAutomat1.addTransition(Transition.create('a', 'g', z3, z4));
	}

	@Test
	/**
	 * Testet ob der Automat die Wörter aa,aaa,aaaa, ... und aaaaaaaa erkennt.
	 * Natürlich wird immer eine entsprechende Ausgabe erwartet. Mögliche
	 * Ausgaben werden dann in die Menge possibleOutputs hinzugefügt. Die
	 * Methode testPossibleOutputs überprüft ob die Ausgaben passen.
	 */
	public void testRecognizesTrue() throws NotRecognizedWordException {
		Collection<String> possibleOutputs = new LinkedList<>();
		possibleOutputs.add("ex");
		possibleOutputs.add("fg");
		this.testPossibleOutputs(possibleOutputs, "aa");
		possibleOutputs = new LinkedList<>();
		possibleOutputs.add("fhg");
		this.testPossibleOutputs(possibleOutputs, "aaa");
		possibleOutputs = new LinkedList<>();
		possibleOutputs.add("fhhg");
		this.testPossibleOutputs(possibleOutputs, "aaaa");
		possibleOutputs = new LinkedList<>();
		possibleOutputs.add("fhhhg");
		this.testPossibleOutputs(possibleOutputs, "aaaaa");
		possibleOutputs = new LinkedList<>();
		possibleOutputs.add("fhhhhg");
		this.testPossibleOutputs(possibleOutputs, "aaaaaa");
		possibleOutputs = new LinkedList<>();
		possibleOutputs.add("fhhhhhg");
		this.testPossibleOutputs(possibleOutputs, "aaaaaaa");
		possibleOutputs = new LinkedList<>();
		possibleOutputs.add("fhhhhhhg");
		this.testPossibleOutputs(possibleOutputs, "aaaaaaaa");

	}

	/**
	 * Prüft ob @param possibleOutputs die tatsächliche Ausgabe des Automaten
	 * bei Eingabe von @param input enthält.
	 * 
	 * @throws NotRecognizedWordException
	 *             falls das eingegebene Wort nicht erkannt wird. Soll bei
	 *             diesen Testfällen nicht passieren.
	 */
	private void testPossibleOutputs(Collection<String> possibleOutputs, String input)
			throws NotRecognizedWordException {
		boolean wordMatch = false;
		String realOutput = this.testAutomat1.recognizes(input);
		Iterator<String> it = possibleOutputs.iterator();
		while (it.hasNext()) {
			String oneOutput = it.next();
			if (oneOutput.equals(realOutput)) {
				wordMatch = true;
				break;
			}
		}
		assertEquals(true, wordMatch);
	}

	@Test
	/**
	 * Der gegebene Automat aus testResources/Testautomat1.pdf soll die hier
	 * angegebenen Wörter nicht erkennen. Um dies zu testen wird eine
	 * entsprechende Methode aufgerufen.
	 */
	public void testRecognizesFalse() {
		this.testNotRecognizedInput("");
		this.testNotRecognizedInput("a");
		this.testNotRecognizedInput("aaaaaaaaaaaaaaac");
		this.testNotRecognizedInput("aabaaaaaaaaaaaaa");
		this.testNotRecognizedInput("b");
		this.testNotRecognizedInput("casasd");
		this.testNotRecognizedInput("aab");
	}

	/**
	 * * Der gegebene Automat aus testResources/Testautomat1.pdf soll die hier
	 * angegebenen Wörter nicht erkennen. D.h. eine
	 * {@link NotRecognizedWordException} wird erwartet. Tritt diese Ausnahme
	 * nicht auf, ist der Testfall fehlgeschlagen.
	 */
	private void testNotRecognizedInput(String input) {
		try {
			this.testAutomat1.recognizes(input);
			fail();
		} catch (NotRecognizedWordException e) {
			// can be empty --> we want an Exception to occure.
		}
	}
}
