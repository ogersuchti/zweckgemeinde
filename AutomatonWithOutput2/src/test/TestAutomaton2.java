package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import automaton.Automat;
import automaton.NotRecognizedWordException;
import automaton.State;

/**
 * Testet die Implementierung endlicher Automaten mit Ausgabe anhand der
 * Automaten aus testResources/TestAutomaten2.pdf
 */
public class TestAutomaton2 {

	private Automat automaton1;
	private Automat automaton2;

	@Before
	/**
	 * Baut die endlichen Automaten aus testResources/Testautomaten2.pdf auf.
	 */
	public void setUp() {
		this.automaton1 = Automat.create();
		State z01 = this.automaton1.getStartState();
		State zE1 = this.automaton1.getEndState();
		State z11 = this.automaton1.createState();
		State z21 = this.automaton1.createState();
		z01.addTransition('a', 'a', z11);
		z11.addTransition('b', 'b', z01);
		z11.addTransition('b', 'b', z11);
		z11.addTransition('a', 'a', z21);
		z21.addTransition('b', 'b', z11);
		z21.addTransition('b', 'b', z21);
		z21.addTransition('c', 'c', zE1);
		this.automaton2 = Automat.create();
		State z02 = this.automaton2.getStartState();
		State z12 = this.automaton2.createState();
		State zE2 = this.automaton2.getEndState();
		z02.addTransition('a', 'a', z12);
		z12.addTransition('b', 'b', z12);
		z12.addTransition('a', 'a', zE2);
	}

	@Test
	/**
	 * Testet den Automaten a1 aus dem pdf Dokument von
	 * testResources/Testautomaten2.pdf. Der Automat soll die angegebenen Wörter
	 * erkennen. Nach gegebenem Aufbau des Automaten sind dann Eingabe und
	 * Ausgabe des Automaten gleich.
	 */
	public void testAutomaton1True() throws NotRecognizedWordException {
		assertEquals("aac", this.automaton1.recognizes("aac"));
		assertEquals("abaac", this.automaton1.recognizes("abaac"));
		assertEquals("abbbbbbaac", this.automaton1.recognizes("abbbbbbaac"));
		assertEquals("abbbbac", this.automaton1.recognizes("abbbbac"));
		assertEquals("aabbc", this.automaton1.recognizes("aabbc"));
		assertEquals("aabac", this.automaton1.recognizes("aabac"));
		assertEquals("aabbaac", this.automaton1.recognizes("aabbaac"));
		assertEquals("abbbbbaac", this.automaton1.recognizes("abbbbbaac"));
	}

	@Test
	/**
	 * Der Automat soll die angegebenen Wörter nicht erkennen.
	 */
	public void testAutomaton1False() {
		this.testNotRecognizedInputAutomaton1("", this.automaton1);
		this.testNotRecognizedInputAutomaton1("aa", this.automaton1);
		this.testNotRecognizedInputAutomaton1("ab", this.automaton1);
		this.testNotRecognizedInputAutomaton1("cc", this.automaton1);
		this.testNotRecognizedInputAutomaton1("aab", this.automaton1);
		this.testNotRecognizedInputAutomaton1("aabbbbaa", this.automaton1);
		this.testNotRecognizedInputAutomaton1("abc", this.automaton1);
		this.testNotRecognizedInputAutomaton1("xyz", this.automaton1);
		this.testNotRecognizedInputAutomaton1("!!!!@§$%&/()=?", this.automaton1);
	}

	@Test
	/**
	 * Die erkannte Sprache des Automaten ist L(automaton2) = a(b)*a. Der
	 * Automat soll also alle Wörter gebildet nach diesem regulären Ausdruck
	 * erkennen. Nach Konstruktion des Automaten ist dann jeweils Eingabe ==
	 * Ausgabe .
	 */
	public void testAutomaton2True() throws NotRecognizedWordException {
		assertEquals("aa", this.automaton2.recognizes("aa"));
		assertEquals("aba", this.automaton2.recognizes("aba"));
		assertEquals("abba", this.automaton2.recognizes("abba"));
		assertEquals("abbba", this.automaton2.recognizes("abbba"));
		assertEquals("abbbba", this.automaton2.recognizes("abbbba"));
		assertEquals("abbbbba", this.automaton2.recognizes("abbbbba"));
		assertEquals("abbbbbba", this.automaton2.recognizes("abbbbbba"));
		assertEquals("abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba",
				this.automaton2.recognizes("abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba"));
	}

	@Test
	/**
	 * Die folgenden Wörter werden nicht erkannt, da sie nicht der Sprache
	 * beschrieben durch folgenden regulären Ausdruck a(b)*a entsprechen.
	 */
	public void testAutomaton2False() {
		this.testNotRecognizedInputAutomaton1("a", this.automaton2);
		this.testNotRecognizedInputAutomaton1("ab", this.automaton2);
		this.testNotRecognizedInputAutomaton1("abb", this.automaton2);
		this.testNotRecognizedInputAutomaton1("aab", this.automaton2);
		this.testNotRecognizedInputAutomaton1("abac", this.automaton2);
		this.testNotRecognizedInputAutomaton1("a/)", this.automaton2);
		this.testNotRecognizedInputAutomaton1("aba?=)=)", this.automaton2);
		this.testNotRecognizedInputAutomaton1("aXASDWQELKDSA", this.automaton2);
	}

	/**
	 * Der übergebene automat soll das übergebene Eingabewort nicht erkennen.
	 * D.h. es wird eine {@link NotRecognizedWordException} erwartet. Sollte
	 * diese nicht "geworfen" werden, ist dieser Testfall fehlgeschlagen.
	 */
	private void testNotRecognizedInputAutomaton1(String input, Automat automat) {
		try {
			automat.recognizes(input);
			fail();
		} catch (NotRecognizedWordException e) {
			// can be empty --> we want an Exception to occure.
		}
	}

}
