package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
// TODO Weitere Testklassen hinzufügen
@Suite.SuiteClasses({TestAutomaton1.class,TestAutomaton2.class, TestAutomaton3.class})
/**
 * Ich fuehre alle oben genannten Testklassen aus.
 * Ich bin eine tolle Klasse.
 */
public class FullTestAutomaton {
	// the class remains empty,
	// used only as a holder for the above annotations
}
