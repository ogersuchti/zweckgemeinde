package automaton;

import java.util.Iterator;

/**
 * Repräsentation von Configurationen endlicher Automaten mit Ausgabe <br>
 * Die wichtigsten Komponenten einer Konfiguration sind<br>
 * <br>
 * Eingabewort <input> <br>
 * Ausgabewort <output> <br>
 * Zugehöriger Automat <automat> <br>
 * Aktueller Zustand <currentState> <br>
 * Dient zur Verarbeitung von Wörtern.
 */
public class Configuration {

	private static final String CONFIGURATION_RUNNING = "Configuration running";
	private String input;
	private String output;
	private final Automat automat;
	private State currentState;
	private final Thread thread;
	private final ConfigManager manager;
	private boolean interrupted;

	public static Configuration create(String input, String output, Automat automat, State currentstate,
			ConfigManager manager) {
		return new Configuration(input, output, automat, currentstate, manager);
	}

	private Configuration(String input, String output, Automat automat, State state, ConfigManager manager) {
		this.input = input;
		this.output = output;
		this.automat = automat;
		this.currentState = state;
		this.manager = manager;
		this.interrupted = false;
		this.thread = new Thread(new Runnable() {

			@Override
			public void run() {
				Configuration.this.start();
			}
		}, CONFIGURATION_RUNNING);
	}

	/**
	 * Führt einen Schritt aus. Sollte es für currentState und input mehrere
	 * Transitionen geben, werden entsprechende Konfigurationen in neuen Threads
	 * gestartet. Die aktuelle Konfigurations verfolgt immer die letzte
	 * Transition weiter.
	 */
	private void start() {
		if (this.input.isEmpty()) {
			if (this.currentState.equals(this.automat.getEndState())) {
				this.manager.success(this.output);
			} else {
				this.manager.noSuccess(this);
			}
		} else {
			boolean noTransitions = true;
			char firstDigit = this.input.charAt(0);
			this.input = this.input.substring(1);
			Iterator<Transition> iterator = this.automat.getValidTransitions(this.currentState, firstDigit).iterator();
			while (iterator.hasNext() && !this.interrupted) {
				noTransitions = false;
				Transition current = iterator.next();
				if (!iterator.hasNext()) {
					// Ich bin der letzte
					this.output = this.output + current.getOutput();
					this.currentState = current.getAfter();
					this.continueWith();
				} else {

					this.manager.runNewConfig(new Configuration(this.input, this.output + current.getOutput(),
							this.automat, current.getAfter(), this.manager));
				}
			}
			if (noTransitions || this.interrupted) {
				this.manager.noSuccess(this);
			}
		}
	}

	/**
	 * Der Thread des Automaten fährt mit dem Einlesen der Eingabe fort.
	 */
	private void continueWith() {
		this.start();
	}

	/**
	 * Diese Methode lässt die Konfiguration laufen indem der Thread gestartet
	 * wird.
	 */
	public void run() {
		this.thread.start();
	}

	/**
	 * Bringt den Thread der Konfiguration zum stoppen indem eine boolsche
	 * Variable gesetzt wird und falls es wartende threads gibt werden diese
	 * aufgeweckt.
	 */
	public void stoppThread() {
		this.interrupted = true;
		this.thread.interrupt();
	}

}
