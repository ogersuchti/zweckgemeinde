package automaton;

@SuppressWarnings("serial")
/**
 *	Diese Ausnahme repräsentiert das ein gegebenes Wort nicht durch die Sprache eines gegebenen Automaten erkannt wird.
 *	D.h. diese Ausnahme stellt keinen Programmfehler dar, vielmehr die Ausnahme das der gegebene Automat das gegebene Wort nicht erkennen konnte.
 */
public class NotRecognizedWordException extends Exception {
	
	public NotRecognizedWordException() {
		super();
	}

}
