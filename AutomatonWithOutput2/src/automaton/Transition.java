package automaton;

/**
 * Repräsentation der Elemente der Überführungs- und Ausgabefunktion eines
 * endlichen Automaten mit Ausgabe. Enthält die für einen Übergang in einem
 * Automaten nötigen Informationen und ordnet gleichzeitig die Ausgabe zu.
 * 
 */
public class Transition {

	/**
	 * Eingabe der Transition
	 */
	private final char input;

	/**
	 * Ausgabe der Transition
	 */
	private final char output;

	/**
	 * Zustand vom dem die Transition kommt
	 */
	private State before;

	/**
	 * Zustand in den die Transition führt
	 */
	private State after;

	public static Transition create(char input, char output, State before, State after) {
		return new Transition(input, output, before, after);
	}

	private Transition(char input, char output, State before, State after) {
		this.input = input;
		this.output = output;
		this.before = before;
		this.after = after;
	}

	public State getAfter() {
		return this.after;
	}

	public State getBefore() {
		return this.before;
	}

	public char getInput() {
		return this.input;
	}

	public char getOutput() {
		return this.output;
	}

}
