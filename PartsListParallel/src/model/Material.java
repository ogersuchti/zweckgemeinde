package model;

public class Material extends ComponentCommon {

	private static final String MaterialDoesNotPossessAnySubstructureMessage = "Material does not possess any substructure";

	private Material(String name, int price) {
		super(name, price);
	}

	public static ComponentCommon create(String name, int price) {
		return new Material(name, price);
	}

	@Override
	public void addPart(ComponentCommon part, int quantitiy) throws Exception {
		throw new Exception(MaterialDoesNotPossessAnySubstructureMessage);
	}

	@Override
	public int getOverallPrice() {
		return this.price;
	}
	@Override
	public MaterialList getMaterialList() {
		return MaterialList.create(this, 1);
	}

	@Override
	protected boolean ContainsDetails(Component part) {
		return false;
	}

	@Override
	public Component copy() {
		return create(this.name,this.price);
	}

	@Override
	public MaterialList computeMaterialList() {
		return MaterialList.create(this, 1);
	}


}
