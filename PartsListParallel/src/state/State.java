package state;

import model.MaterialList;
import model.Product;

public abstract class State {

	/**
	 * Modifies the state. Price cache gets thrown away.
	 */
	public abstract void priceChanged(Product product);

	/**
	 * Modifies the state. Price and MaterialList cache gets thrown away.
	 */
	public abstract void structureChanged(Product product);

	/**
	 * 
	 * @return the overallPrice. If the price is not cached it will be computed
	 *         and returned.
	 */
	public abstract int getPrice(Product product);

	/**
	 * 
	 * @return the MaterialList. If the MaterialList is not cached it will be
	 *         computed and returned.
	 */
	public abstract MaterialList getMaterialList(Product product);

}
