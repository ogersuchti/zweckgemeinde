package state;

import model.MaterialList;
import model.Product;

public class PCached extends State {

	private final int price;

	public PCached( int price) {
		this.price = price;
	}

	@Override
	public void priceChanged(Product product) {
		product.setState(NothingCached.create());
	}

	@Override
	public void structureChanged(Product product) {
		product.setState(NothingCached.create());
	}

	@Override
	public int getPrice(Product product) {
		return this.price;
	}

	@Override
	public MaterialList getMaterialList(Product product) {
		product.setState(new PMLCached(product.computeMaterialList(), this.price));
		return this.getMaterialList(product);
	}

}
