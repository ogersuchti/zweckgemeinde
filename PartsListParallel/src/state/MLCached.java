package state;

import model.MaterialList;
import model.Product;

public class MLCached extends State {

	private final MaterialList materialList;

	public MLCached(MaterialList matList) {
		this.materialList = matList;
	}

	@Override
	public void priceChanged(Product product) {
	}

	@Override
	public void structureChanged(Product product) {
		product.setState(NothingCached.create());
	}

	@Override
	public int getPrice(Product product) {
		product.setState(new PMLCached(this.materialList, product.computeOverallPrice()));
		return product.getOverallPrice();
	}

	@Override
	public MaterialList getMaterialList(Product product) {
		return this.materialList;
	}

}
