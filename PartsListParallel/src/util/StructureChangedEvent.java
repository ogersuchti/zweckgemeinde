package util;

public class StructureChangedEvent implements Event {
	
	private static StructureChangedEvent theStructureChangedEvent;
	
	/**
	 * returns the singleton StructureChangedEvent
	 */
	public static StructureChangedEvent create() {
		if(theStructureChangedEvent == null) {
			theStructureChangedEvent = new StructureChangedEvent();
		}
		return theStructureChangedEvent;
	}

	@Override
	public void accept(EventVisitor visitor) {
		visitor.handle(this);

	}

}
