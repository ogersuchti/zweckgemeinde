package util;

public interface Event {
	/**
	 * accept/visitor pattern, informs the visitor about the Event
	 */
	void accept(EventVisitor visitor);

}
