package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import model.ComponentCommon;
import model.Material;
import model.Product;
import model.QuantifiedComponent;

public class PartsListNebenläufig {

	public static QuantifiedComponent qm1;
	public static QuantifiedComponent qm2;
	public static QuantifiedComponent qm3;
	public static QuantifiedComponent qm4;

	public static Product p1;
	public static Product p2;
	public static Product p3;

	public static ComponentCommon m1;
	public static ComponentCommon m2;
	public static ComponentCommon m3;
	public static ComponentCommon m4;
	
	@Test
	public void derUltimativeMegaKrasseTest() {
		for (int i = 0; i < 100; i++) {
			this.setUp();
			this.testGetOverallPrice();
		}
	}
	
	

	public void setUp() {
		m1 = Material.create("M1", 10);
		m2 = Material.create("M2", 20);
		m3 = Material.create("M3", 30);
		m4 = Material.create("M4", 40);

		p1 = Product.create("P1", 5);
		p2 = Product.create("P2", 1);
		p3 = Product.create("P3", 10);

		qm1 = QuantifiedComponent.create(4, m1);
		qm2 = QuantifiedComponent.create(2, m2);
		qm3 = QuantifiedComponent.create(6, m3);
		qm4 = QuantifiedComponent.create(2, m4);

	}

	public void testGetOverallPrice() {
		try {
			assertEquals(10, m1.getOverallPrice());
			assertEquals(20, m2.getOverallPrice());
			assertEquals(30, m3.getOverallPrice());
			assertEquals(40, m4.getOverallPrice());
			p1.addPart(p2, 2);
			p1.addPart(p3, 3);
			p1.addPart(m4, 2);
			p2.addPart(m1, 2);
			p2.addPart(m2, 1);
			p3.addPart(m3, 2);
			assertEquals(41, p2.getOverallPrice());
			assertEquals(70, p3.getOverallPrice());
			assertEquals(377, p1.getOverallPrice());
		} catch (final Exception e) {
			fail();
		}
	}

}