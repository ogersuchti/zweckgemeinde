package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import model.ComponentCommon;
import model.Material;
import model.MaterialList;
import model.Product;
import model.QuantifiedComponent;

public class PartsListTest {

	public static QuantifiedComponent qm1;
	public static QuantifiedComponent qm2;
	public static QuantifiedComponent qm3;
	public static QuantifiedComponent qm4;

	public static Product p1;
	public static Product p2;
	public static Product p3;

	public static ComponentCommon m1;
	public static ComponentCommon m2;
	public static ComponentCommon m3;
	public static ComponentCommon m4;

	@Before
	public void setUp() throws Exception {
		m1 = Material.create("M1", 10);
		m2 = Material.create("M2", 20);
		m3 = Material.create("M3", 30);
		m4 = Material.create("M4", 40);

		p1 = Product.create("P1", 5);
		p2 = Product.create("P2", 1);
		p3 = Product.create("P3", 10);

		qm1 = QuantifiedComponent.create(4, m1);
		qm2 = QuantifiedComponent.create(2, m2);
		qm3 = QuantifiedComponent.create(6, m3);
		qm4 = QuantifiedComponent.create(2, m4);

	}

	@Test
	public void addPartsTest() {

		try {
			m1.addPart(m1, 2);
			fail(); // Material can not have parts.
		} catch (final Exception e) {
		}

		try {
			m1.addPart(p1, 2);
			fail(); // Material can not have parts.
		} catch (final Exception e) {
		}

		try {
			p1.addPart(p1, 2);
			fail(); // Reflexive Cycle
		} catch (final Exception e) {
		}

		try {
			p1.addPart(p2, 1);
			p2.addPart(p1, 2);
			fail(); // Direct Cycle
		} catch (final Exception e) {
		}

		try {
			p1.addPart(p2, 1);
			p2.addPart(m1, 2);
			p2.addPart(p3, 2);
			p3.addPart(p1, 2); // Indirect Cycle: p1 already contains p2 and p2
								// already contains p3
			fail();
		} catch (final Exception e) {
		}
		try {
			p1.addPart(p2, 0);// p1 already contains p2 two times
			p1.addPart(p3, 3);
			p1.addPart(m4, 2);
			p2.addPart(m1, 0);// p2 already contains m1 two times
			p2.addPart(m2, 1);
			p3.addPart(m3, 2);

		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void containsTest() {
		assertTrue(p1.contains(p1)); // Reflexive contains
		assertFalse(p1.contains(p2));
		assertFalse(p1.contains(p3));
		assertFalse(p1.contains(m1));
		assertFalse(p1.contains(m2));
		assertFalse(p1.contains(m3));
		assertFalse(p1.contains(m4));
		assertFalse(p2.contains(m1));
		assertFalse(p2.contains(m2));
		assertFalse(p3.contains(m3));

		try {
			p1.addPart(p2, 2);
			p1.addPart(p3, 3);
			p1.addPart(m4, 2);
			p2.addPart(m1, 2);
			p2.addPart(m2, 1);
			p3.addPart(m3, 2);
		} catch (final Exception e) {
			fail();
		}
		assertTrue(p1.contains(p2)); // Direct contains
		assertTrue(p1.contains(p3)); // Direct contains
		assertTrue(p1.contains(m1)); // Indirect contains
		assertTrue(p1.contains(m2)); // Indirect contains
		assertTrue(p1.contains(m3)); // Indirect contains
		assertTrue(p1.contains(m4)); // Direct contains
		assertTrue(p2.contains(m1)); // Direct contains
		assertTrue(p2.contains(m2)); // Direct contains
		assertTrue(p3.contains(m3)); // Direct contains

		assertFalse(p2.contains(m3));
		assertFalse(p2.contains(m4));
		assertFalse(p3.contains(m1));
		assertFalse(p3.contains(m2));
		assertFalse(p3.contains(m4));

		assertFalse(m1.contains(m3));// Materials can not contain anything
		assertFalse(m3.contains(m1));
		assertFalse(m2.contains(p3));
		assertFalse(m4.contains(p2));
	}

	@Test
	public void testGetMaterialList() {
		try {
			p1.addPart(p2, 2);
			p1.addPart(p3, 3);
			p1.addPart(m4, 2);
			p2.addPart(m1, 2);
			p2.addPart(m2, 1);
			p3.addPart(m3, 2);

			final MaterialList actualMateriallist = MaterialList.create();
			actualMateriallist.addMaterial(qm1);
			actualMateriallist.addMaterial(qm2);
			actualMateriallist.addMaterial(qm3);
			actualMateriallist.addMaterial(qm4);
			assertEquals(actualMateriallist, p1.getMaterialList());
			assertEquals(actualMateriallist.add(actualMateriallist), p1.getMaterialList().add(p1.getMaterialList()));
			p1.addPart(m4, 1);
			actualMateriallist.addMaterial(QuantifiedComponent.create(1, m4));
			assertEquals(actualMateriallist, p1.getMaterialList());
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void testGetOverallPrice() {
		try {
			assertEquals(10, m1.getOverallPrice());
			assertEquals(20, m2.getOverallPrice());
			assertEquals(30, m3.getOverallPrice());
			assertEquals(40, m4.getOverallPrice());
			p1.addPart(p2, 2);
			p1.addPart(p3, 3);
			p1.addPart(m4, 2);
			p2.addPart(m1, 2);
			p2.addPart(m2, 1);
			p3.addPart(m3, 2);
			assertEquals(41, p2.getOverallPrice());
			assertEquals(70, p3.getOverallPrice());
			assertEquals(377, p1.getOverallPrice());
			p2.addPart(m3, 1);
			assertEquals(41 + m3.getOverallPrice(), p2.getOverallPrice());
			assertEquals(377 + 2 * (m3.getOverallPrice()), p1.getOverallPrice());
			// 2*,because p1 contains p2 two times
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void testAddMaterial() {

		final MaterialList ml1 = MaterialList.create();
		ml1.addMaterial(qm1);
		ml1.addMaterial(qm2);
		ml1.addMaterial(qm3);
		ml1.addMaterial(qm4);
		assertEquals(qm1, ml1.getMaterialsWithQuantity().get(qm1.getName()));
		assertEquals(qm2, ml1.getMaterialsWithQuantity().get(qm2.getName()));
		assertEquals(qm3, ml1.getMaterialsWithQuantity().get(qm3.getName()));
		assertEquals(qm4, ml1.getMaterialsWithQuantity().get(qm4.getName()));
	}

	@Test
	public void testAdd() {

		final MaterialList ml1 = MaterialList.create();
		ml1.addMaterial(qm1);
		ml1.addMaterial(qm2);
		assertEquals(ml1, ml1.add(MaterialList.create()));
		assertEquals(MaterialList.create().add(ml1), ml1);

		final MaterialList ml2 = MaterialList.create();
		ml2.addMaterial(qm3);
		ml2.addMaterial(qm4);
		assertEquals(ml1.add(ml2), ml2.add(ml1));

		final MaterialList list3 = MaterialList.create();
		list3.addMaterial(qm1);
		list3.addMaterial(qm2);
		list3.addMaterial(qm3);
		list3.addMaterial(qm4);
		assertEquals(list3, ml1.add(ml2));
		assertEquals(ml1.add(ml2), list3);
	}

	@Test
	public void testAMul() {
		final MaterialList m1 = MaterialList.create();
		final MaterialList m2 = MaterialList.create();

		m1.addMaterial(qm1);
		m1.addMaterial(qm1);
		m1.addMaterial(qm2);
		m2.addMaterial(qm1);
		m2.addMaterial(qm1);
		m2.addMaterial(qm1);
		m2.addMaterial(qm1);
		m2.addMaterial(qm2);
		m2.addMaterial(qm2);
		// m1 contains the quantified components half as often as m2 does
		assertEquals(m2, m1.mul(2));

		m2.addMaterial(qm1);
		m2.addMaterial(qm1);
		m2.addMaterial(qm1);
		m2.addMaterial(qm1);
		m2.addMaterial(qm2);
		m2.addMaterial(qm2);
		// m2 now contains the quantified components four times as often as m1
		assertEquals(m2, m1.mul(4));

	}

	@Test
	public void testHashcode() {
		final MaterialList m1 = MaterialList.create();
		m1.addMaterial(qm1);
		m1.addMaterial(qm2);
		final MaterialList m2 = MaterialList.create();
		m2.addMaterial(qm1);
		m2.addMaterial(qm2);
		assertEquals(m1.hashCode(), m2.hashCode());

	}

}
