package commands;

import model.QuantifiedComponent;

public class CommandgetOverallPrice {

	private Integer result;
	private final QuantifiedComponent receiver;

	public CommandgetOverallPrice(QuantifiedComponent receiver) {
		super();
		this.receiver = receiver;
	}

	public synchronized void execute() {
		this.result = this.receiver.getOverallPrice();
		this.notify();
	}

	public synchronized int getResult() {
		// Hier wäre State sinnvoll
		if(this.result == null) {
			try {
				this.wait();
			} catch (InterruptedException e) {
			}
		}
		return this.result;

	}

}
