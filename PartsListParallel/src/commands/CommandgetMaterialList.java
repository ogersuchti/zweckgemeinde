package commands;

import model.MaterialList;
import model.QuantifiedComponent;

public class CommandgetMaterialList {

	
	
	private MaterialList result;
	private final QuantifiedComponent receiver;

	public CommandgetMaterialList(QuantifiedComponent receiver) {
		super();
		this.receiver = receiver;
	}

	public synchronized void execute() {
		this.result = this.receiver.getMaterialList();
		this.notify();
	}

	public synchronized MaterialList getResult() {
		// Hier wäre State sinnvoll
		if(this.result == null) {
			try {
				this.wait();
			} catch (InterruptedException e) {
			}
		}
		return this.result;

	}

}
