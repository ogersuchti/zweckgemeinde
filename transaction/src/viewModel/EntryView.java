package viewModel;

import model.CreditEntry;
import model.Entry;
import model.EntryVisitor;

public class EntryView {

	protected static final String DebitPrefix = "Debit: ";
	protected static final String CreditPrefix = "Credit: ";
	protected static final String Amount = "Amount: ";
	protected static final String Purpose = "Purpose: ";

	public static EntryView create(Entry entry) {
		return new EntryView(entry);
	}

	private Entry entry;

	public EntryView(Entry entry) {
		this.entry = entry;
	}

	public String toString() {
		EntryVisitor<String> visitor = new EntryVisitor<String>() {

			@Override
			public String handleDebitEntry(Entry debitEntry) {
				return DebitPrefix + Amount + debitEntry.getAmount() + Purpose + debitEntry.getPurpose();

			}

			@Override
			public String handleCreditEntry(CreditEntry creditEntry) {
				return CreditPrefix + Amount + creditEntry.getAmount() + Purpose + creditEntry.getPurpose();

			}
		};
		return this.entry.acceptEntryVisitor(visitor);
	}

}
