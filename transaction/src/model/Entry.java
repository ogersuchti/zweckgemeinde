package model;

/** An entry (debit or credit) for an account, typically result of a booking process. */
public abstract class Entry {
	
	
	protected final Transfer transfer;
	
	protected Entry(Transfer transfer) {
		this.transfer = transfer;
	}

	public abstract <T> T acceptEntryVisitor(EntryVisitor<T> visitor);

	public long getAmount() {
		return this.transfer.getAmount();
	}

	public String getPurpose() {
		return transfer.getPurpose();
	}
	
}
