package model;

public interface EntryVisitor<T> {


	T handleCreditEntry(CreditEntry creditEntry);

	T handleDebitEntry(Entry debitEntry);

}
