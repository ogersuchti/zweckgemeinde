package model;

public class DebitEntry extends Entry {
	

	public DebitEntry(Transfer transfer) {
		super(transfer);
	}

	@Override
	public <T> T acceptEntryVisitor(EntryVisitor<T> visitor) {
		return visitor.handleDebitEntry(this);
	}

	public static Entry create(Transfer transfer) {
		return new DebitEntry(transfer);
	}

}
