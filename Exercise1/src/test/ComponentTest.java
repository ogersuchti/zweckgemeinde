package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import model.Composite;
import model.CycleException;
import model.Element;

public class ComponentTest {

	Element e1;
	Element e2;
	Composite c1;
	Composite c2;

	Composite lom1;
	Composite lom2;
	Element elom3;
	Composite lom4;
	Element elom5;

	Composite theComposite;
	Element thePart;
	Element otherPart1;
	Element otherPart2;

	@Before
	public void setUp() {

		thePart = Element.create();
		theComposite = Composite.create(thePart);
		otherPart1 = Element.create();
		otherPart2 = Element.create();

		e1 = Element.create();
		e2 = Element.create();
		c1 = Composite.create(e1);
		c2 = Composite.create(e1);

		try {
			elom3 = Element.create();
			elom5 = Element.create();
			lom2 = Composite.create(elom3);
			lom1 = Composite.create(lom2);
			lom4 = Composite.create(elom3);
			lom1.addOtherPart(lom4);
			lom1.addOtherPart(elom5);
			lom2.addOtherPart(lom4);
		} catch (final CycleException e) {
			throw new Error(e);
		}

	}

	@Test
	public void testHierarchyCycleExceptions() {
		try {
			c1.addOtherPart(c1);
			fail();
		} catch (final CycleException e) {
		}

		try {
			c1.addOtherPart(Composite.create(Composite.create(c1)));
			fail();
		} catch (final CycleException e) {
		}

		try {
			c2.addOtherPart(c1);
			c1.addOtherPart(c2);
			fail();
		} catch (final CycleException e) {
		}

		try {
			lom4.addOtherPart(lom1);
			fail();
		} catch (final CycleException e) {
		}

		// try {
		// lom4.setThePart(lom1);
		// fail();
		// } catch (final CycleException e) {
		// }
	}

	@Test
	public void testAddParts() throws CycleException {
		c1.addOtherPart(e2);
		c1.addOtherPart(c2);
		assertTrue(c1.contains(e2));
		assertTrue(c1.contains(e1));
		assertTrue(c1.contains(c2));
	}

	@Test
	public void testAddParts1() throws CycleException {
		c1.addOtherPart(e2);
		c2.addOtherPart(c1);
		assertTrue(c2.contains(e2));

		lom4.addOtherPart(c2);
		assertTrue(lom1.contains(e2));
	}

	// @Test
	// public void testSetThePart() throws CycleException {
	// c1.setThePart(e2);
	// assertFalse(c1.contains(e1));
	// assertTrue(c1.contains(e2));
	// }
	//
	// @Test
	// public void testSetThePart1() throws CycleException {
	// c2.setThePart(e2);
	// c1.setThePart(c2);
	// assertFalse(c1.contains(e1));
	// assertTrue(c1.contains(e2));
	// assertTrue(c1.contains(c2));
	// }

	@Test
	public void testChangeThePart() throws Exception {

		theComposite.addOtherPart(otherPart1);
		theComposite.addOtherPart(otherPart2);
		assertEquals(thePart, theComposite.getThePart());
		theComposite.changeThePart(otherPart1);
		assertTrue(theComposite.contains(thePart));
		assertEquals(otherPart1, theComposite.getThePart());
		assertEquals(2, theComposite.count());
		theComposite.changeThePart(otherPart2);
		assertEquals(otherPart2, theComposite.getThePart());
		assertEquals(2, theComposite.count());

	}

	@Test
	public void testChangeThePartException() {

		try {
			theComposite.changeThePart(e1);
			fail();
		} catch (final Exception e) {
		}
	}

	// 1 von ausen 2 von innen. gr��e der liste bleibt gleich

	@Test
	public void testNumberOfParts() {
		try {
			theComposite.addOtherPart(otherPart1);
			theComposite.addOtherPart(otherPart2);
		} catch (final CycleException e) {
			fail();
		}
		assertEquals(1, e1.numberOfParts());
		assertEquals(2, c1.numberOfParts());
		assertEquals(2, c2.numberOfParts());
		assertEquals(4, theComposite.numberOfParts());
	}
	
	@Test
	public void testAdjust() {
		final Element e1 = Element.create();
		final Element e2 = Element.create();
		final Element e3 = Element.create();
		final Element e4 = Element.create();
		final Composite c2 = Composite.create(e3);
		final Composite c3 = Composite.create(e2);
		final Composite c1 = Composite.create(e1);
		final Composite c4 = Composite.create(e4);
		try {
			c1.addOtherPart(c2);
			c1.addOtherPart(c3);
			c4.addOtherPart(c1);
		} catch (final CycleException e) {
			fail();
		}
		c4.adjust();
		assertEquals(c1, c4.getThePart());
	}

}