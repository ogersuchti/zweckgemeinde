package model;

public interface Component {
	
	public abstract boolean contains(Component component);
	
	public abstract int numberOfParts();

	public abstract void adjust();

}
