package model;

public class Element implements Component{
	
	public static Element create(){
		return new Element();
	}
	private Element(){
	}
	@Override
	public boolean contains(Component component) {
		return this.equals(component);
	}
	@Override
	public int numberOfParts() {
		return 1;
	}
	@Override
	public void adjust() {
		
	}

}
