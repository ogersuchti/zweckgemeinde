package model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class Composite implements Component {

	public static Composite create(Component thePart) {
		return new Composite(thePart);
	}

	private Component thePart;
	private final Collection<Component> theOtherParts;

	private Composite(Component thePart) {
		this.thePart = thePart;
		this.theOtherParts = new LinkedList<Component>();
	}

	/**
	 * Adds an additional part, namely <component> to the receiver.
	 * 
	 * @throws CycleException
	 *             If the addition leads to a cyclic part structure.
	 */
	public void addOtherPart(Component component) throws CycleException {
		if (component.contains(this))
			throw CycleException.create();
		this.theOtherParts.add(component);
	}

	/**
	 * Sets the value of the field <thePart> to <component>.
	 * 
	 * @throws CycleException
	 *             If the setting leads to a cyclic part structure.
	 */
	private void setThePart(Component component) throws CycleException {
		if (component.contains(this)) {
			throw CycleException.create();
		}
		this.thePart = component;
	}

	public Component getThePart() {
		return this.thePart;
	}

	public Collection<Component> getTheOtherParts() {
		return this.theOtherParts;
	}

	public void changeThePart(Component newThePart) throws Exception {
		
		if(this.theOtherParts.contains(newThePart)) {
			this.theOtherParts.remove(newThePart);
			this.theOtherParts.add(this.thePart);
			this.thePart = newThePart;
			return;
		}
		throw new Exception();

//		if (!this.directRemove(component))
//			throw new Exception();
//
//		this.addOtherPart(this.thePart);
//		this.setThePart(component);

	}

	@Override
	public void adjust() {
		this.thePart.adjust();
		Component biggest = this.thePart;
		final Iterator<Component> iterator = this.theOtherParts.iterator();
		while(iterator.hasNext()) {
			final Component current = iterator.next();
			current.adjust();
			if(biggest.numberOfParts() < current.numberOfParts()) {
				biggest = current;
			}
			
		}
		try {
			this.changeThePart(biggest);
		} catch (final Exception e) {
			throw new Error() ;
		}
	}

	/**
	 * @return false if the object was not removed because it is not contained.
	 */
	public boolean directRemove(Component component) {
		final Iterator<Component> iterator = this.theOtherParts.iterator();
		while (iterator.hasNext()) {
			final Component current = iterator.next();
			if (current.equals(component)) {
				this.theOtherParts.remove(current);
				return true;
			}

		}
		return false;
	}

	@Override
	public boolean contains(Component component) {
		if (this.thePart.contains(component) || this.equals(component))
			return true;
		final Iterator<Component> iterator = this.theOtherParts.iterator();
		while (iterator.hasNext()) {
			final Component current = iterator.next();
			if (current.contains(component)) {
				return true;
			}
		}
		return false;
	}

	public int count() {
		int sum = 0;
		final Iterator<Component> iterator = this.theOtherParts.iterator();
		while (iterator.hasNext()) {
			iterator.next();
			sum++;
		}
		return sum;
	}

	@Override
	public int numberOfParts() {
		int sum = 0;
		final Iterator<Component> iterator = this.theOtherParts.iterator();
		while(iterator.hasNext()) {
			final Component current = iterator.next();
			sum = sum + current.numberOfParts();
		}
		return sum + 1 + this.thePart.numberOfParts();
	}
}
