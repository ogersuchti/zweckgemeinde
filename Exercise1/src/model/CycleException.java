package model;

@SuppressWarnings("serial")
public class CycleException extends Exception {

	public static CycleException create() {
		return new CycleException();
	}
	
	private CycleException() {
	}

}
