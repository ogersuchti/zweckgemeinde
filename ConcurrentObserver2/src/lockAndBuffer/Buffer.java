package lockAndBuffer;


public class Buffer<E> {

	@SuppressWarnings("serial")
	public static class StoppException extends Exception {
	}

	private abstract class BufferEntry<T> {
		abstract T getWrapped() throws StoppException;
	}

	private class Stopp<T> extends BufferEntry<T> {
		Stopp() {
		}

		@Override
		T getWrapped() throws StoppException {
			throw new StoppException();
		}
	}

	private class Wrapped<T> extends BufferEntry<T> {
		final private T wrapped;

		Wrapped(T toBeWrapped) {
			this.wrapped = toBeWrapped;
		}

		@Override
		public T getWrapped() {
			return this.wrapped;
		}
	}

	private Lock mutex;
	private Lock reading;
	private Lock writing;
	boolean waitingForNotEmpy;
	private boolean waitingForNotFull = false;

	private Object[] myBuffer;
	private int internalCapacity;
	private int first;
	private int behindLast;

	public Buffer(int capacity) {
		
		capacity = capacity <= 0 ? 1 : capacity;
		this.internalCapacity = capacity + 1;
		this.myBuffer = new Object[this.internalCapacity];
		this.first = 0;
		this.behindLast = 0;
		
		
		this.mutex = new Lock(false);
		this.reading = new Lock(true);
		this.writing = new Lock(true);
		this.waitingForNotEmpy = false;

	}

	

	public void put(E value) {
		this.mutex.lock();
		if(this.isFull()){
			this.waitingForNotFull = true;
			this.mutex.unlock(); // unlock = raus aus dem kritischen Abschnitt
			this.writing.lock();
			this.mutex.lock();
		}
		this.addNextEntry(new Wrapped<E>(value));
		if (this.waitingForNotEmpy) {
			this.waitingForNotEmpy = false;
			this.reading.unlock();
		}

		this.mutex.unlock();
	}

	private void addNextEntry(BufferEntry<E> wrapped) {
		this.myBuffer[this.behindLast] = wrapped;
		this.behindLast = (this.behindLast + 1) % this.internalCapacity;
		
	}

	public E get() throws StoppException {
		this.mutex.lock();
		if (this.isEmpty()) {
			this.waitingForNotEmpy = true;
			this.mutex.unlock();
			this.reading.lock();
			this.mutex.lock();
		}
		E result = this.getNextEntry().getWrapped();
		if(this.waitingForNotFull) {
			this.waitingForNotFull = false;
			this.writing.unlock();
		}
		this.mutex.unlock();
		return result;
	}

	private BufferEntry<E> getNextEntry() {
		@SuppressWarnings("unchecked")
		BufferEntry<E> result = (Buffer<E>.BufferEntry<E>) this.myBuffer[this.first];
		this.first = (this.first + 1) % this.internalCapacity;
		return result;
	}

	public void stopp() {
		this.addNextEntry(new Stopp<E>());
	}

	private boolean isEmpty() {
		return this.first == this.behindLast;
	}
	
	private boolean isFull() {
		return ((this.behindLast + 1) % this.internalCapacity) == this.first;
	}
}
