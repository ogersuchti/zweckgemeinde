package buffer;

public class ErrorEntry implements BufferEntry{
	
	private Exception exception;

	@Override
	public <T> T accept(BufferEntryVisitor<T> visitor) {
		return visitor.handle(this);
	}

	
	public ErrorEntry() {
	}
	
	public ErrorEntry(Exception exception) {
		super();
		this.exception = exception;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof ErrorEntry;
	}
	
	public void printStackTrace() {
		this.getEncapsulatedException().printStackTrace();
	}


	public Exception getEncapsulatedException() {
		return this.exception;
	}


}
