package buffer;


import java.math.BigInteger;

public class IntegerEntry implements BufferEntry{
	
	private BigInteger value;
	
	public IntegerEntry(BigInteger value) {
		this.value = value;
	}
	
	public IntegerEntry(int value) {
		this.value = BigInteger.valueOf(value);
	}
	
	public BigInteger getValue() {
		return this.value;
	}

	@Override
	public <T> T accept(BufferEntryVisitor<T> visitor) {
		return visitor.handle(this);
	}
	
	@Override
	public boolean equals(Object entry){
		if(entry instanceof IntegerEntry) {
			IntegerEntry entryAsIntegerEntry = (IntegerEntry) entry;
			return entryAsIntegerEntry.getValue().equals(this.getValue());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.value.toString();
	}
	

}
