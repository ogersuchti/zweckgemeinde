package buffer;


public interface BufferEntryVisitor<T> {

	public T handle(IntegerEntry integerEntry);

	public T handle(StopCommand stopCommand);

	public T handle(ErrorEntry errorEntry);


}
