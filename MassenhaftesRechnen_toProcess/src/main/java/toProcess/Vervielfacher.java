package toProcess;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.BufferEntryVisitor;
import buffer.ErrorEntry;
import buffer.IntegerEntry;
import buffer.StopCommand;
import model.Process;

/**
 * @author Tim
 *
 */
public class Vervielfacher implements Process{

	private static final String VERVIELFACHER = "Vervielfacher";
	private final Buffer input;
	private final List<Buffer> output;

	@Override
	public void start() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				boolean vervielfachen = true;
				while (vervielfachen) {
					vervielfachen = Vervielfacher.this.input.get().accept(new BufferEntryVisitor<Boolean>() {

						@Override
						public Boolean handle(IntegerEntry integerEntry) {
							Vervielfacher.this.addToOutput(integerEntry);
							return true;
						}

						@Override
						public Boolean handle(StopCommand stopCommand) {
							Vervielfacher.this.addToOutput(stopCommand);
							return false;
						}

						@Override
						public Boolean handle(ErrorEntry errorEntry) {
							Vervielfacher.this.addToOutput(errorEntry);
							return false;
						}

					});
				}
			}
		}, VERVIELFACHER);
		thread.start();
	}

	protected void addToOutput(BufferEntry entry) {
		Iterator<Buffer> it = this.output.iterator();
		while (it.hasNext()) {
			Buffer buffer = it.next();
			buffer.put(entry);
		}
	}

	public Vervielfacher(Buffer input) {
		super();
		this.input = input;
		this.output = new LinkedList<>();
	}

	public Buffer getInputBuffer() {
		return this.input;
	}

	public Buffer newOutPutBuffer() {
		Buffer newBuffer = new Buffer();
		this.output.add(newBuffer);
		return newBuffer;
	}


	public void addToBuffer(List<BufferEntry> entry) {
		Iterator<BufferEntry> it = entry.iterator();
		while (it.hasNext()) {
			BufferEntry current = it.next();
			this.input.put(current);
		}
	}

	public Buffer getOutputBuffer(int i) {
		return this.output.get(i);
	}

	@Override
	public String toString() {
		return "Vervielfacher [input=" + this.input + ", output=" + this.output + "]";
	}
	
	

}
