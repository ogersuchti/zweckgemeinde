package model;

import java.math.BigInteger;
import java.util.List;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.ErrorEntry;
import buffer.IntegerEntry;

public class DivisionProcess extends OperationProcess {

	public static DivisionProcess create(Buffer input1, Buffer input2) {
		List<Buffer> input = OperationProcess.createBinaryInput(input1, input2);
		return new DivisionProcess(input, new Buffer());
	}

	protected DivisionProcess(List<Buffer> inputs, Buffer output) {
		super(inputs, output);
	}

	@Override
	BufferEntry combine(BigInteger first, BigInteger second) {
		try {

			IntegerEntry entry = new IntegerEntry(first.divide(second));
			return entry;
		} catch (ArithmeticException e) {
			return new ErrorEntry(e);
		}
	}

	public static OperationProcess create(Buffer input1, Buffer input2, Buffer output) {

		List<Buffer> input = OperationProcess.createBinaryInput(input1, input2);
		return new DivisionProcess(input, output);
	}

	@Override
	protected String getConcreteOperation() {
		return "Division";
	}

}
