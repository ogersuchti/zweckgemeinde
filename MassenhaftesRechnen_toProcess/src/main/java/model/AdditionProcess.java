package model;

import java.math.BigInteger;
import java.util.List;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.IntegerEntry;

public class AdditionProcess extends OperationProcess {

	
	
	public static AdditionProcess create(Buffer input1, Buffer input2) {
		List<Buffer> input = OperationProcess.createBinaryInput(input1, input2);
		return new AdditionProcess(input, new Buffer());
	}

	protected AdditionProcess(List<Buffer> inputs, Buffer output) {
		super(inputs, output);
	}


	@Override
	BufferEntry combine(BigInteger first, BigInteger second) {
//		System.out.println("Erster Wert:" + first + " Zweiter Wert:" + second + " " + this.toString());
		return new IntegerEntry(first.add(second));
	}

	public static OperationProcess create(Buffer input1, Buffer input2, Buffer output) {
		List<Buffer> input = OperationProcess.createBinaryInput(input1, input2);
		return new AdditionProcess(input, output);
	}

	@Override
	protected String getConcreteOperation() {
		return "Addition";
	}


}
