package model;


import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.BufferEntryVisitor;
import buffer.ErrorEntry;
import buffer.IntegerEntry;
import buffer.StopCommand;

public abstract class OperationProcess implements Process{



	protected List<Buffer> inputs;
	protected Buffer output;
	private Thread myThread;

	
	protected OperationProcess(List<Buffer> inputs,Buffer output) {
		super();
		this.inputs = inputs;
		this.output = output;
		
	}
	
	protected void addInputBuffer(Buffer inputBuffer) {
		this.inputs.add(inputBuffer);
	}

	@Override
	public void start() {
		this.startBinaryOperation();
	}

	public void startBinaryOperation() {
		this.myThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				boolean rechnen = true;
				while(rechnen) {
					rechnen = OperationProcess.this.inputs.get(0).get().accept(new BufferEntryVisitor<Boolean>() {

						@Override
						public Boolean handle(IntegerEntry integerEntry) {
							BigInteger first = integerEntry.getValue();
							return OperationProcess.this.inputs.get(1).get().accept(new BufferEntryVisitor<Boolean>() {

								@Override
								public Boolean handle(IntegerEntry integerEntry) {
									BufferEntry processedEntry = OperationProcess.this.combine(first, integerEntry.getValue());
									OperationProcess.this.addToOutput(processedEntry);
									return true;
								}

								@Override
								public Boolean handle(StopCommand stopCommand) {
									OperationProcess.this.addToOutput(stopCommand);
									return false;
								}

								@Override
								public Boolean handle(ErrorEntry errorEntry) {
									OperationProcess.this.addToOutput(errorEntry);
									return false;
								}
							});
						}


						@Override
						public Boolean handle(StopCommand stopCommand) {
							OperationProcess.this.addToOutput(stopCommand);
							return false;
						}

						@Override
						public Boolean handle(ErrorEntry errorEntry) {
							OperationProcess.this.addToOutput(errorEntry);
							return false;
						}


					});
				}
				
			}
		},this.getConcreteOperation());
		this.myThread.start();
	}
	
	
	protected abstract String getConcreteOperation();
	
	abstract BufferEntry combine(BigInteger first,BigInteger second);

	private void addToOutput(BufferEntry entry) {
		OperationProcess.this.output.put(entry);
	}


	public Buffer getOutputBuffer() {
		return this.output;
	}
	
	protected static List<Buffer> createBinaryInput(Buffer input1, Buffer input2) {
		List<Buffer> inputs = new LinkedList<>();
		inputs.add(input1);
		inputs.add(input2);
		return inputs;
	}
}
