package model;

import java.math.BigInteger;
import java.util.List;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.IntegerEntry;

public class SubtractionProcess extends OperationProcess {

	public static OperationProcess create(Buffer input1, Buffer input2) {
		List<Buffer> input = createBinaryInput(input1, input2);
		return new SubtractionProcess(input, new Buffer());
	}

	protected SubtractionProcess(List<Buffer> inputs, Buffer output) {
		super(inputs, output);
	}


	@Override
	BufferEntry combine(BigInteger first, BigInteger second) {
		return new IntegerEntry(first.subtract(second));
	}

	public static OperationProcess create(Buffer input1, Buffer input2, Buffer output) {
		List<Buffer> input = createBinaryInput(input1, input2);
		return new SubtractionProcess(input, output);
	}

	@Override
	protected String getConcreteOperation() {
		return "Subtraction";
	}
	

}
