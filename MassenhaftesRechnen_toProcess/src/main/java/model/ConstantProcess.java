package model;

import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.IntegerEntry;
import buffer.StopCommand;

public class ConstantProcess {

	private Buffer output;

	public ConstantProcess(BigInteger myNumber) {
		this.output = new ConstantBuffer(myNumber,50);
	}

	public ConstantProcess(BigInteger myNumber, int anzahlRueckgaben) {
		this.output = new ConstantBuffer(myNumber, anzahlRueckgaben);
	}

	public Buffer getOutputBuffer() {
		return this.output;
	}

	private class ConstantBuffer extends Buffer {

		private int anzahlMaxRueckgaben;
		private int getaetigteRueckgaben;
		private BigInteger theNumber;
		
		public ConstantBuffer(BigInteger theNumber, int anzahlMaxRueckgaben) {
			this.anzahlMaxRueckgaben = anzahlMaxRueckgaben;
			this.getaetigteRueckgaben = 0;
			this.theNumber = theNumber;
		}

		@Override
		// Hier synchronized ja nein ?
		public synchronized void put(BufferEntry value) {
			throw new Error("Nothing to be put in the Outputstream of a Constant");
		}

		@Override
		// Hier synchronized ja nein ?
		public BufferEntry get() {
			if(this.getaetigteRueckgaben >= this.anzahlMaxRueckgaben) {
				return new StopCommand();
			}
			this.getaetigteRueckgaben ++;
			return new IntegerEntry(this.theNumber);
		}
	}
}
