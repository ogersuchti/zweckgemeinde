package model;

import java.math.BigInteger;
import java.util.List;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.IntegerEntry;

public class MultiplikationProcess extends OperationProcess{
	
	
	public static MultiplikationProcess create(Buffer input1, Buffer input2) {

		List<Buffer> input = OperationProcess.createBinaryInput(input1, input2);
		return new MultiplikationProcess(input, new Buffer());
	}

	protected MultiplikationProcess(List<Buffer> inputs, Buffer output) {
		super(inputs, output);
	}


	@Override
	BufferEntry combine(BigInteger first, BigInteger second) {
//		System.out.println("Erster Wert:" + first + " Zweiter Wert:" + second + " " + this.toString());
		return new IntegerEntry(first.multiply(second));
	}

	public static OperationProcess create(Buffer input1, Buffer input2, Buffer output) {
		List<Buffer> input = OperationProcess.createBinaryInput(input1, input2);
		return new MultiplikationProcess(input, output);
	}

	@Override
	protected String getConcreteOperation() {
		return "Multiplikation";
	}
}
