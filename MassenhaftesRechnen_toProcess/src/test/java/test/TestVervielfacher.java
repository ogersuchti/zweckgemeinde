package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.IntegerEntry;
import buffer.StopCommand;
import toProcess.Vervielfacher;

public class TestVervielfacher {
	
	private Vervielfacher fuenfer;
	
	@Before
	public void setUp() {
		Buffer input = new Buffer();
		input.put(new IntegerEntry(2));
		input.put(new StopCommand());
		this.fuenfer= new Vervielfacher(input);
		this.fuenfer.newOutPutBuffer();
		this.fuenfer.newOutPutBuffer();
		this.fuenfer.newOutPutBuffer();
		this.fuenfer.newOutPutBuffer();
		this.fuenfer.newOutPutBuffer();
	}

	@Test
	public void test() {
		this.fuenfer.start();
		int i = 0;
		while (i < 5) {
			Buffer output = this.fuenfer.getOutputBuffer(i);
			assertEquals(new IntegerEntry(2),output.get());
			assertEquals(new StopCommand(),output.get());
			i++;
		}
	}

}
