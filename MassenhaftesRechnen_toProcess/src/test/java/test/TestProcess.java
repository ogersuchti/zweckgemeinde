package test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.ErrorEntry;
import buffer.IntegerEntry;
import buffer.StopCommand;
import model.AdditionProcess;
import model.ConstantProcess;
import model.DivisionProcess;
import model.MultiplikationProcess;
import model.OperationProcess;
import model.SubtractionProcess;

public class TestProcess {

	private OperationProcess process;
	private Buffer input1;
	private Buffer input2;

	@Before
	public void setUp() {
		this.input1 = new Buffer();
		this.input1.put(new IntegerEntry(25));
		this.input1.put(new IntegerEntry(25));
		this.input1.put(new IntegerEntry(25));
		this.input1.put(new StopCommand());
		
		this.input2 = new Buffer();
		this.input2.put(new IntegerEntry(25));
		this.input2.put(new IntegerEntry(25));
		this.input2.put(new IntegerEntry(25));
		this.input2.put(new StopCommand());
		
	}

	// TODO komplexe Testf�lle schreiben !!!!!!!!!!!!!
	
	@Test
	public void testAddition() {
		this.process = AdditionProcess.create(this.input1, this.input2);
		this.process.startBinaryOperation();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new IntegerEntry(50), output.get());
		assertEquals(new IntegerEntry(50), output.get());
		assertEquals(new IntegerEntry(50), output.get());
		assertEquals(new StopCommand(), output.get());

	}

	@Test
	public void testMultiplikation() {
		this.process = MultiplikationProcess.create(this.input1, this.input2);
		this.process.startBinaryOperation();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new IntegerEntry(625), output.get());
		assertEquals(new IntegerEntry(625), output.get());
		assertEquals(new IntegerEntry(625), output.get());
		assertEquals(new StopCommand(), output.get());

	}

	@Test
	public void testSubtraktion() {
		this.process = SubtractionProcess.create(this.input1, this.input2);
		this.process.startBinaryOperation();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new IntegerEntry(0), output.get());
		assertEquals(new IntegerEntry(0), output.get());
		assertEquals(new IntegerEntry(0), output.get());
		assertEquals(new StopCommand(), output.get());

	}

	@Test
	public void testDivision() {
		this.input2 = new Buffer();
		this.input2.put(new IntegerEntry(25));
		this.input2.put(new IntegerEntry(25));
		this.input2.put(new IntegerEntry(0));
		this.input2.put(new StopCommand());

		this.process = DivisionProcess.create(this.input1, this.input2);
		this.process.startBinaryOperation();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new IntegerEntry(1), output.get());
		assertEquals(new IntegerEntry(1), output.get());
		assertEquals(new ErrorEntry(), output.get());
		assertEquals(new StopCommand(), output.get());

	}

	@Test
	public void testConstant() {
		ConstantProcess c50 = new ConstantProcess(BigInteger.valueOf(50));
		Buffer outputBuffer = c50.getOutputBuffer();
		int i = 0;
		while (i < 50) {
			assertEquals(new IntegerEntry(50), outputBuffer.get());
			i++;
		}
		if(i == 50) {
			assertEquals(new StopCommand(), outputBuffer.get());
		}
	}

}
