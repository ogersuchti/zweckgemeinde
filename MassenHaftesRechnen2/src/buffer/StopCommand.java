package buffer;


public class StopCommand implements BufferEntry{

	@Override
	public <T> T accept(BufferEntryVisitor<T> visitor) {
		return visitor.handle(this);
		
	}

}
