package buffer;

public class ErrorEntry implements BufferEntry{

	@Override
	public <T> T accept(BufferEntryVisitor<T> visitor) {
		return visitor.handle(this);
	}

}
