package editor;

import java.util.Stack;

import command.Command;
import command.CursorLeftCommand;
import command.CursorRightCommand;
import command.DeleteLeftCommand;
import command.DeleteRightCommand;
import command.KeyTypedCommand;

public class Editor {

	public static Editor createEditor() {
		return new Editor();
	}

	private final StringBuffer text;
	private int position;
	private boolean shiftMode;
	private final Stack<Command> commands;

	private Editor() {
		this.text = new StringBuffer();
		this.position = 0;
		this.shiftMode = false;
		this.commands = new Stack<>();
	}

	public void keyTyped(final Character c) {
		final Command command = KeyTypedCommand.create(this, c);
		this.commands.push(command);
		command.execute();
	}

	public void executeKeyTyped(final Character c) {
		this.getText().insert(this.getPosition(), this.getShiftMode() ? c : Character.toLowerCase(c));
		this.setPosition(this.getPosition() + 1);
	}

	private StringBuffer getText() {
		return this.text;
	}

	public int getPosition() {
		return this.position;
	}

	private void setPosition(final int position) {
		this.position = position;
	}

	public void deleteLeft() {
		if (this.getPosition() > 0) {
			final Command command = DeleteLeftCommand.create(this);
			this.commands.push(command);
			command.execute();
		}
	}

	public char executeDeleteLeft() {
		char c = '�';
		this.setPosition(this.position - 1);
		c = this.text.charAt(this.getPosition());
		this.text.deleteCharAt(this.position);
		return c;
	}

	public void deleteRight() {
		if (this.getPosition() < this.getText().length()) {
			final Command command = DeleteRightCommand.create(this);
			this.commands.push(command);
			command.execute();
		}
	}

	public char executeDeleteRight() {
		char c = '�';
		c = this.text.charAt(this.position);
		this.text.deleteCharAt(this.position);
		return c;
	}

	public void left() {
		final Command command = CursorLeftCommand.create(this);
		this.commands.push(command);
		command.execute();
	}

	public void executeLeft() {
		if (this.getPosition() > 0)
			this.setPosition(this.getPosition() - 1);
	}

	public void right() {
		final Command command = CursorRightCommand.create(this);
		this.commands.push(command);
		command.execute();
	}

	public void executeRight() {
		if (this.getPosition() < this.getText().length())
			this.setPosition(this.getPosition() + 1);
	}

	public void newLine() { // TODO command
		this.getText().insert(this.getPosition(), "\n");
		this.setPosition(this.getPosition() + 1);
	}

	public String getEditorText() {
		return this.getText().toString();
	}

	public void shift() {
		this.setShiftMode(!this.getShiftMode());
	}

	private void setShiftMode(final boolean b) {
		this.shiftMode = b;
	}

	private boolean getShiftMode() {
		return this.shiftMode;
	}

	public void undo() {
		if (!this.commands.isEmpty()) {
			this.commands.peek().undo();
			this.commands.pop();
			return;
		}
		System.out.println("Everything popped by you");
	}

	public void insert(char c) {
		this.getText().insert(this.position, c);
	}

	public void insertAndGoRight(char c) {
		this.getText().insert(this.position, c);
		this.executeRight();
	}

}
