package command;

import editor.Editor;

public class KeyTypedCommand implements Command {

	private final Editor receiver;
	private final char typedCharacter;

	public KeyTypedCommand(Editor receiver,char typedCharacter) {
		this.receiver = receiver;
		this.typedCharacter = typedCharacter;
	}

	@Override
	public void execute() {
		this.receiver.executeKeyTyped(this.typedCharacter);
	}

	@Override
	public void undo() {
		this.receiver.executeDeleteLeft();
	}

	public static Command create(Editor receiver, Character typedCharacter) {
		return new KeyTypedCommand(receiver,typedCharacter);
	}

}
