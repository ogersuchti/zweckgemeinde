package command;

import editor.Editor;

public class DeleteLeftCommand implements Command {

	private final Editor receiver;
	private char deletedC;

	public static DeleteLeftCommand create(Editor receiver) {
		return new DeleteLeftCommand(receiver);
	}

	private DeleteLeftCommand(Editor receiver) {
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		this.deletedC = this.receiver.executeDeleteLeft();
	}

	@Override
	public void undo() {
			this.receiver.insertAndGoRight(deletedC);
	}

}
