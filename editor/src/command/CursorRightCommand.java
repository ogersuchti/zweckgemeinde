package command;

import editor.Editor;

public class CursorRightCommand implements Command{
	
	private final Editor receiver;

	public static CursorRightCommand create(Editor receiver) {
		return new CursorRightCommand(receiver);
	}
	public CursorRightCommand(Editor receiver) {
		this.receiver = receiver;
	}
	@Override
	public void execute() {
		this.receiver.executeRight();
	}
	@Override
	public void undo() {
		this.receiver.executeLeft();
	}

}
