package command;

import editor.Editor;

public class CursorLeftCommand implements Command{
	
	private final Editor receiver;

	@Override
	public void execute() {
		this.receiver.executeLeft();
	}

	@Override
	public void undo() {
		this.receiver.executeRight();
	}

	public static Command create(Editor receiver) {
		return new CursorLeftCommand(receiver);
	}
	
	public CursorLeftCommand(Editor receiver) {
		this.receiver = receiver;
	}

}
