package command;

import editor.Editor;

public class DeleteRightCommand implements Command {

	private final Editor receiver;
	private char deletedChar; // State

	public static DeleteRightCommand create(Editor receiver) {
		return new DeleteRightCommand(receiver);
	}

	public DeleteRightCommand(Editor receiver) {
		this.receiver = receiver;
		this.deletedChar = '�';
	}

	@Override
	public void execute() {
		this.deletedChar = this.receiver.executeDeleteRight();
	}

	@Override
	public void undo() {
		this.receiver.insert(deletedChar);
	}

}
