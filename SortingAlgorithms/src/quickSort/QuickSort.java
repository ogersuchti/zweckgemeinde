package quickSort;

import java.util.List;

import buffer.Buffer;
import general.SortingAlgorithm;

public class QuickSort<E extends Comparable<E>> implements SortingAlgorithm<E> {

	private Buffer<E> output;
	
	public QuickSort() {
		this.output = new Buffer<>();
	}

	@Override
	public List<E> sort(List<E> toBeSorted) {
		this.quickSort(toBeSorted);
		return this.output.toList();
	}

	private void quickSort(List<E> toBeSorted) {
		Buffer<E> input = new Buffer<>();
		input.putListElementsWithStop(toBeSorted);
		QuickSortThread<E> initialQuickSort = new QuickSortThread<>(input,this.output);
		initialQuickSort.quickSortOnce();
	}
}
