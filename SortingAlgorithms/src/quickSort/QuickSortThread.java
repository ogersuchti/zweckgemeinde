package quickSort;

import buffer.Buffer;
import buffer.Buffer.StoppException;

public class QuickSortThread<E extends Comparable<E>> {

	private static final String QUICK_SORT = "QuickSort";
	private final Buffer<E> input;
	private final Buffer<E> splittedOutput1;
	private final Buffer<E> splittedOutput2;
	private final Buffer<E> sortedOutput;
	private E median;

	public QuickSortThread(Buffer<E> input) {
		this.input = input;
		this.splittedOutput1 = new Buffer<>();
		this.splittedOutput2 = new Buffer<>();
		this.sortedOutput = new Buffer<>();
	}

	public QuickSortThread(Buffer<E> input, Buffer<E> sortedOutput) {
		this.input = input;
		this.sortedOutput = sortedOutput;
		this.splittedOutput1 = new Buffer<>();
		this.splittedOutput2 = new Buffer<>();
	}

	public Buffer<E> getSortedOutput() {
		return this.sortedOutput;
	}

	public void quickSortOnce() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				QuickSortThread.this.sort();
			}

		}, QUICK_SORT);
		thread.start();
	}

	private void sort() {
		boolean zeroElements = this.getMedian();
		if (zeroElements) {
			return;
		}
		QuickSortThread<E> quickSort1 = new QuickSortThread<>(this.splittedOutput1);
		quickSort1.quickSortOnce();
		QuickSortThread<E> quickSort2 = new QuickSortThread<>(this.splittedOutput2);
		quickSort2.quickSortOnce();
		this.splitInputByMedian();
		this.writeToSortedOutput(quickSort1.getSortedOutput());
		this.sortedOutput.put(this.median);
		this.writeToSortedOutput(quickSort2.getSortedOutput());
		this.sortedOutput.stopp();
	}

	private void writeToSortedOutput(Buffer<E> output) {
		boolean writeToOutput = true;
		while (writeToOutput) {
			try {
				E entry = output.get();
				this.sortedOutput.put(entry);
			} catch (StoppException e) {
				writeToOutput = false;
			}
		}
	}

	private void splitInputByMedian() {
		boolean readingInput = true;
		while (readingInput) {
			try {
				E entry = this.input.get();
				if (entry.compareTo(this.median) < 0) {
					this.splittedOutput1.put(entry);
				} else {
					this.splittedOutput2.put(entry);
				}
			} catch (StoppException e) {
				this.splittedOutput1.stopp();
				this.splittedOutput2.stopp();
				readingInput = false;
			}
		}
	}

	private boolean getMedian() {
		try {
			this.median = this.input.get();
			return false;
		} catch (StoppException e) {
			this.sortedOutput.stopp();
			return true;
		}
	}

}
