package bubbleSort;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import buffer.Buffer;
import buffer.Buffer.StoppException;
import general.SortingAlgorithm;

public class BubbleSort<E extends Comparable<E>> implements SortingAlgorithm<E>{

	private Collection<EinmalSortierer<E>> sortingThreads;
	private Buffer<E> dieAusgabe;
	
	public BubbleSort() {
		this.sortingThreads = new LinkedList<>();
		this.dieAusgabe = new Buffer<>();
	}
	@Override
	public List<E> sort(List<E> toBeSorted) {
		Buffer<E> toBeSortedBuffer = new Buffer<>();
		toBeSortedBuffer.putListElementsWithStop(toBeSorted);
		this.bubbleSort(toBeSortedBuffer);
		return this.dieAusgabe.toList();
	}

	private void bubbleSort(Buffer<E> zuSortieren) {
		zuSortieren.stopp();
		this.anmelden(zuSortieren);
	}


	public synchronized void anmelden(Buffer<E> input) {
		EinmalSortierer<E> sort = new EinmalSortierer<>(input, this);
		this.sortingThreads.add(sort);
		sort.start();
	}
	
	public synchronized void abmelden(EinmalSortierer<E> sort) {
		Iterator<EinmalSortierer<E>> it = this.sortingThreads.iterator();
		while (it.hasNext()) {
			EinmalSortierer<E> current = it.next();
			if(current.equals(sort)) {
				it.remove();
			}
		}
		if(this.sortingThreads.isEmpty()) {
			this.uebertrageErgebnis(sort.getOutput());
		}
	}

	private void uebertrageErgebnis(Buffer<E> output) {
		boolean auslesen = true;
		while(auslesen) {
			try {
				E entry = output.get();
				this.dieAusgabe.put(entry);
				
				
			} catch (StoppException e) {
				auslesen = false;
				this.dieAusgabe.stopp();
			}
		}
	}


}
