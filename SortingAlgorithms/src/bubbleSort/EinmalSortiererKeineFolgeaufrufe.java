package bubbleSort;

import java.math.BigInteger;

import buffer.Buffer;
import buffer.Buffer.StoppException;

/**
 * Diese Klasse dient nur Testzwecken und ist bei der Entwicklung eines
 * nebenl�ufigen Bubble-Sort Algorhytmus entstanden.
 * 
 * @author Tim
 *
 */
public class EinmalSortiererKeineFolgeaufrufe {

	private final Buffer<BigInteger> input;
	private final Buffer<BigInteger> output;

	public EinmalSortiererKeineFolgeaufrufe(Buffer<BigInteger> input) {
		super();
		this.input = input;
		this.output = new Buffer<>();
	}

	public Buffer<BigInteger> getOutput() {
		return this.output;
	}

	public void start() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				boolean sortieren = true;
				// TODO cool kommentieren
				BigInteger first = null;
				try {
					first = EinmalSortiererKeineFolgeaufrufe.this.input.get();
				} catch (StoppException e) {
					sortieren = false;
					EinmalSortiererKeineFolgeaufrufe.this.output.stopp();
				}
				while (sortieren) {

					try {
						BigInteger second = EinmalSortiererKeineFolgeaufrufe.this.input.get();
						first = EinmalSortiererKeineFolgeaufrufe.this.switchValues(first, second);
					} catch (StoppException e) {
						EinmalSortiererKeineFolgeaufrufe.this.output.put(first);
						EinmalSortiererKeineFolgeaufrufe.this.output.stopp();
						sortieren = false;

					}
				}
			}

		});
		thread.start();
	}

	public BigInteger switchValues(BigInteger first, BigInteger second) {
		if (first.compareTo(second) > 0) {
			// Tauschen
			this.output.put(second);
			return first;
		}
		this.output.put(first);
		return second;
	}

}
