package bubbleSort;


import buffer.Buffer;
import buffer.Buffer.StoppException;

public class EinmalSortierer<T extends Comparable<T>> {

	private final Buffer<T> input;
	private final Buffer<T> output;
	private BubbleSort<T> sortierer;
	private boolean folgeThreadAngemeldet;

	public EinmalSortierer(Buffer<T> input, BubbleSort<T> sortierer) {
		super();
		this.input = input;
		this.sortierer = sortierer;
		this.output = new Buffer<>();
		this.folgeThreadAngemeldet = false;
	}

	public Buffer<T> getOutput() {
		return this.output;
	}

	public void start() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				boolean sorting = true;
				// TODO cool kommentieren
				T first = null;
				try {
					first = EinmalSortierer.this.input.get();
				} catch (StoppException e) {
					sorting = false;
					EinmalSortierer.this.output.stopp();
					EinmalSortierer.this.sortierer.abmelden(EinmalSortierer.this);
				}
				while (sorting) {

					try {
						T second = EinmalSortierer.this.input.get();
						first = EinmalSortierer.this.switchValues(first, second);
					} catch (StoppException e) {
						EinmalSortierer.this.output.put(first);
						EinmalSortierer.this.output.stopp();
						sorting = false;
						EinmalSortierer.this.sortierer.abmelden(EinmalSortierer.this);

					}
				}
			}

		});
		thread.start();
	}

	public T switchValues(T first, T second) {
		if (first.compareTo(second) > 0) {
			// Tauschen
			this.output.put(second);
			if (!this.folgeThreadAngemeldet) {
				this.sortierer.anmelden(this.output);
				this.folgeThreadAngemeldet = true;
			}
			return first;
		}
		this.output.put(first);
		return second;
	}

}
