package bubbleSortTest;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import bubbleSort.BubbleSort;
import buffer.Buffer.StoppException;
import general.SortingAlgorithm;

public class TestBubbleSort {

	@Test
	public void test2Inputs() throws StoppException {
		List<BigInteger> input = new LinkedList<>();
		input.add(BigInteger.valueOf(2));
		input.add(BigInteger.valueOf(2));
		input.add(BigInteger.valueOf(2));
		input.add(BigInteger.valueOf(1));
		input.add(BigInteger.valueOf(3));
		input.add(BigInteger.valueOf(1));
		this.vglOutput(input);
	}
	@Test
	public void testOneInput() throws StoppException {
		List<BigInteger> input = new LinkedList<>();
		input.add(BigInteger.valueOf(1));
		this.vglOutput(input);
	}
	@Test
	public void testSameInput() throws StoppException {
		List<BigInteger> input = new LinkedList<>();
		input.add(BigInteger.valueOf(1));
		input.add(BigInteger.valueOf(1));
		input.add(BigInteger.valueOf(1));
		input.add(BigInteger.valueOf(1));
		this.vglOutput(input);
	}
	@Test
	public void testMultipleInput() throws StoppException {
		List<BigInteger> input = new LinkedList<>();
		input.add(BigInteger.valueOf(3));
		input.add(BigInteger.valueOf(7));
		input.add(BigInteger.valueOf(1));
		input.add(BigInteger.valueOf(5));
		input.add(BigInteger.valueOf(2));
		input.add(BigInteger.valueOf(6));
		input.add(BigInteger.valueOf(4));
		input.add(BigInteger.valueOf(6));
		input.add(BigInteger.valueOf(9));
		input.add(BigInteger.valueOf(10));
		input.add(BigInteger.valueOf(6));
		input.add(BigInteger.valueOf(8));
		this.vglOutput(input);
	}
	
	
	
	public void vglOutput(List<BigInteger> toBeSorted) throws StoppException {
		SortingAlgorithm<BigInteger> sortierer = new BubbleSort<>();
		List<BigInteger> sortedList = sortierer.sort(toBeSorted);
		Collections.sort(toBeSorted);
		assertEquals(toBeSorted, sortedList);
	}

}
