package bubbleSortTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestBubbleSort.class,TestEinmalSortierer.class })
/**
 * Ich fuehre alle oben genannten Testklassen aus.
 * @author tim
 *
 */
public class AllTestsBubbleSort {
	// the class remains empty,
	// used only as a holder for the above annotations
}