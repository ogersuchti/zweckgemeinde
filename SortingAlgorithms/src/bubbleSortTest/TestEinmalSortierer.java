package bubbleSortTest;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.junit.Test;

import bubbleSort.EinmalSortiererKeineFolgeaufrufe;
import buffer.Buffer;
import buffer.Buffer.StoppException;

public class TestEinmalSortierer {

	@Test
	public void test2Inputs() throws StoppException {
		Buffer<BigInteger> input = new Buffer<>();
		input.put(BigInteger.valueOf(2));
		input.put(BigInteger.valueOf(1));
		input.stopp();
		EinmalSortiererKeineFolgeaufrufe onehitWonder = new EinmalSortiererKeineFolgeaufrufe(input);
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(2));
		this.vglOutput(expectedValues, onehitWonder);
	}
	@Test
	public void testOneInput() throws StoppException {
		Buffer<BigInteger> input = new Buffer<>();
		input.put(BigInteger.valueOf(1));
		input.stopp();
		EinmalSortiererKeineFolgeaufrufe onehitWonder = new EinmalSortiererKeineFolgeaufrufe(input);
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		this.vglOutput(expectedValues, onehitWonder);
	}
	@Test
	public void testSameInput() throws StoppException {
		Buffer<BigInteger> input = new Buffer<>();
		input.put(BigInteger.valueOf(1));
		input.put(BigInteger.valueOf(1));
		input.put(BigInteger.valueOf(1));
		input.put(BigInteger.valueOf(1));
		input.stopp();
		EinmalSortiererKeineFolgeaufrufe onehitWonder = new EinmalSortiererKeineFolgeaufrufe(input);
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(1));
		this.vglOutput(expectedValues, onehitWonder);
	}
	@Test
	public void testBehindertInput() throws StoppException {
		Buffer<BigInteger> input = new Buffer<>();
		input.put(BigInteger.valueOf(3));
		input.put(BigInteger.valueOf(7));
		input.put(BigInteger.valueOf(1));
		input.put(BigInteger.valueOf(5));
		input.put(BigInteger.valueOf(2));
		input.put(BigInteger.valueOf(6));
		input.put(BigInteger.valueOf(4));
		input.put(BigInteger.valueOf(6));
		input.put(BigInteger.valueOf(9));
		input.put(BigInteger.valueOf(10));
		input.put(BigInteger.valueOf(6));
		input.put(BigInteger.valueOf(8));
		input.stopp();
		EinmalSortiererKeineFolgeaufrufe onehitWonder = new EinmalSortiererKeineFolgeaufrufe(input);
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(3));
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(5));
		expectedValues.add(BigInteger.valueOf(2));
		expectedValues.add(BigInteger.valueOf(6));
		expectedValues.add(BigInteger.valueOf(4));
		expectedValues.add(BigInteger.valueOf(6));
		expectedValues.add(BigInteger.valueOf(7));
		expectedValues.add(BigInteger.valueOf(9));
		expectedValues.add(BigInteger.valueOf(6));
		expectedValues.add(BigInteger.valueOf(8));
		expectedValues.add(BigInteger.valueOf(10));
		this.vglOutput(expectedValues, onehitWonder);
	}
	
	
	
	public void vglOutput(Collection<BigInteger> expectedValues,EinmalSortiererKeineFolgeaufrufe wonder) throws StoppException {
		wonder.start();
		Buffer<BigInteger> out = wonder.getOutput();
		Iterator<BigInteger> it = expectedValues.iterator();
		while (it.hasNext()) {
			BigInteger current = it.next();
			assertEquals(current, out.get());
		}
	}

}
