package buffer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Buffer<E> {
	
	public List<E> toList() {
		List<E> result = new LinkedList<>();
		boolean noStop = true;
		while (noStop) {
			try {
				
				E entry = this.get();
				result.add(entry);
			} catch (StoppException e) {
				noStop = false;
			}
		}
		return result;
	}
	
	public void putListElementsWithStop(List<E> toBePutInBuffer) {
		Iterator<E> it = toBePutInBuffer.iterator();
		while (it.hasNext()) {
			E current = it.next();
			this.put(current);
		}
		this.stopp();
	}
	
	@SuppressWarnings("serial")
	public static class StoppException extends Exception {}

	private abstract class BufferEntry<T> {
		abstract T getWrapped() throws StoppException;
	}
	private class Stopp<T> extends BufferEntry<T>{
		Stopp(){}
		@Override
		T getWrapped() throws StoppException {
			throw new StoppException();
		}
	}
	private class Wrapped<T> extends BufferEntry<T> {
		final private T wrapped;
		Wrapped(T toBeWrapped){
			this.wrapped = toBeWrapped;
		}
		@Override
		public T getWrapped() {
			return this.wrapped;
		}
	}
	
	List<BufferEntry<E>> implementingList;
	
	public Buffer(){
		this.implementingList = new LinkedList<BufferEntry<E>>();
	}
	
	synchronized public void put(E value) {
		this.implementingList.add(new Wrapped<E>(value));
		this.notify();
	}
	synchronized public E get() throws StoppException {
		while (this.isEmpty())
			try {
				this.wait();
			} catch (InterruptedException e) {e.printStackTrace();}
		E result = this.implementingList.get(0).getWrapped();
		this.implementingList.remove(0);
		return result;
	}
	synchronized public void stopp(){
		this.implementingList.add(new Stopp<E>());
		this.notify();
	}
	private boolean isEmpty(){
		return this.implementingList.isEmpty();
	}



}












