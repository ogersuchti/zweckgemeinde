package general;

import java.util.List;

public interface SortingAlgorithm<E extends Comparable<E>> {
	
	public abstract List<E> sort(List<E> toBeSorted);
	
}
