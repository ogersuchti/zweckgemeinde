package insertionSortTest;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import buffer.Buffer.StoppException;
import general.SortingAlgorithm;
import insertionSort.InsertionSort;

public class TestInsertionSort {

	@Test
	public void test2Input() throws StoppException {
		List<BigInteger> toBeSorted = new LinkedList<>();
		toBeSorted.add(BigInteger.valueOf(2));
		toBeSorted.add(BigInteger.valueOf(2));
		toBeSorted.add(BigInteger.valueOf(2));
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(3));
		toBeSorted.add(BigInteger.valueOf(1));
		this.vglOutadd(toBeSorted);
	}
	@Test
	public void testOneInput() throws StoppException {
		List<BigInteger> toBeSorted = new LinkedList<>();
		toBeSorted.add(BigInteger.valueOf(1));
		this.vglOutadd(toBeSorted);
	}
	@Test
	public void testSameInput() throws StoppException {
		List<BigInteger> toBeSorted = new LinkedList<>();
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(1));
		this.vglOutadd(toBeSorted);
	}
	@Test
	public void testMultipleInput() throws StoppException {
		List<BigInteger> toBeSorted = new LinkedList<>();
		toBeSorted.add(BigInteger.valueOf(3));
		toBeSorted.add(BigInteger.valueOf(7));
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(5));
		toBeSorted.add(BigInteger.valueOf(2));
		toBeSorted.add(BigInteger.valueOf(6));
		toBeSorted.add(BigInteger.valueOf(4));
		toBeSorted.add(BigInteger.valueOf(6));
		toBeSorted.add(BigInteger.valueOf(9));
		toBeSorted.add(BigInteger.valueOf(10));
		toBeSorted.add(BigInteger.valueOf(6));
		toBeSorted.add(BigInteger.valueOf(8));
		this.vglOutadd(toBeSorted);
	}
	
	
	
	private void vglOutadd(List<BigInteger> toBeSorted) throws StoppException {
		SortingAlgorithm<BigInteger> sortierer = new InsertionSort<>();
		List<BigInteger> sortedList = sortierer.sort(toBeSorted);
		Collections.sort(toBeSorted);
		Assert.assertEquals(toBeSorted, sortedList);
	}

}
