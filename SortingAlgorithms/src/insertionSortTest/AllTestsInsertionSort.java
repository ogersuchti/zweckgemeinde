package insertionSortTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestInsertionSort.class,TestSortiererThread.class })
/**
 * Ich fuehre alle oben genannten Testklassen aus.
 * @author tim
 *
 */
public class AllTestsInsertionSort {
	// the class remains empty,
	// used only as a holder for the above annotations
}