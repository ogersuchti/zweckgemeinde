package insertionSortTest;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.Buffer.StoppException;
import insertionSort.SortiererThread;

public class TestSortiererThread {
	
	private Buffer<BigInteger> toBeSorted;
	private List<BigInteger> expectedValues;
	
	@Before
	public void setUp() {
		this.toBeSorted = new Buffer<>();
		this.expectedValues = new LinkedList<>();
	}
	@Test
	public void testSortInEmptyList() throws StoppException {
		this.toBeSorted.stopp();
		this.expectedValues.add(BigInteger.valueOf(25));
		this.testSort(BigInteger.valueOf(25));
	}

	@Test
	public void testSortInOneEntryList() throws StoppException {
		this.toBeSorted.put(BigInteger.valueOf(25));
		this.toBeSorted.stopp();
		this.expectedValues.add(BigInteger.valueOf(25));
		this.expectedValues.add(BigInteger.valueOf(26));
		this.testSort(BigInteger.valueOf(26));
	}
	@Test
	public void testSortInTwoEntryList() throws StoppException {
		this.toBeSorted.put(BigInteger.valueOf(25));
		this.toBeSorted.put(BigInteger.valueOf(27));
		this.toBeSorted.stopp();
		this.expectedValues.add(BigInteger.valueOf(25));
		this.expectedValues.add(BigInteger.valueOf(26));
		this.expectedValues.add(BigInteger.valueOf(27));
		this.testSort(BigInteger.valueOf(26));
		
	}
	@Test
	public void testSortInMultipleEntryList() throws StoppException {
		this.toBeSorted.put(BigInteger.valueOf(23));
		this.toBeSorted.put(BigInteger.valueOf(24));
		this.toBeSorted.put(BigInteger.valueOf(25));
		this.toBeSorted.put(BigInteger.valueOf(26));
		this.toBeSorted.put(BigInteger.valueOf(27));
		this.toBeSorted.stopp();
		this.expectedValues.add(BigInteger.valueOf(23));
		this.expectedValues.add(BigInteger.valueOf(24));
		this.expectedValues.add(BigInteger.valueOf(25));
		this.expectedValues.add(BigInteger.valueOf(26));
		this.expectedValues.add(BigInteger.valueOf(26));
		this.expectedValues.add(BigInteger.valueOf(27));
		this.testSort(BigInteger.valueOf(26));
		
	}
	

	private void testSort(BigInteger toBesortedIn) throws StoppException {
		SortiererThread<BigInteger> sort = new SortiererThread<>(this.toBeSorted, toBesortedIn);
		sort.startSort();
		Buffer<BigInteger> output = sort.getOutput();
		Iterator<BigInteger> it = this.expectedValues.iterator();
		while (it.hasNext()) {
			BigInteger current = it.next();
			Assert.assertEquals(current,output.get());
		}
		this.testLastIsStop(output);
	}

	private void testLastIsStop(Buffer<BigInteger> output) {
		try {
			output.get();
			Assert.fail();
		} catch (StoppException e) {
		}
	}

}
