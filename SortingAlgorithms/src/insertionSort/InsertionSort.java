package insertionSort;

import java.util.Iterator;
import java.util.List;

import buffer.Buffer;
import buffer.Buffer.StoppException;
import general.SortingAlgorithm;

public class InsertionSort<E extends Comparable<E>> implements SortingAlgorithm<E> {

	private final Buffer<E> finalOutput;

	public InsertionSort() {
		this.finalOutput = new Buffer<>();
	}

	@Override
	public List<E> sort(List<E> toBeSorted) {
		this.insertionSort(toBeSorted);
		return this.finalOutput.toList();
	}

	private void insertionSort(List<E> toBeSorted) {
		Iterator<E> it = toBeSorted.iterator();
		Buffer<E> output = new Buffer<>();
		if (it.hasNext()) {
			output.stopp();
			SortiererThread<E> startNewSort = this.startNewSort(it.next(), output);
			output = startNewSort.getOutput();
		} else {
			output.stopp();
		}
		while (it.hasNext()) {
			E current = it.next();
			SortiererThread<E> newSort = this.startNewSort(current, output);
			output = newSort.getOutput();
		}
		this.writeResult(output);
	}

	private void writeResult(Buffer<E> resultBuffer) {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				boolean writing = true;
				while (writing) {
					E entry;
					try {
						entry = resultBuffer.get();
						InsertionSort.this.finalOutput.put(entry);
					} catch (StoppException e) {
						writing = false;
						InsertionSort.this.finalOutput.stopp();
					}
				}
			}
		});
		thread.start();
	}

	private SortiererThread<E> startNewSort(E toBeSorted, Buffer<E> output) {
		SortiererThread<E> sort = new SortiererThread<>(output, toBeSorted);
		sort.startSort();
		return sort;
	}


}
