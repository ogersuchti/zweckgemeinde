package insertionSort;


import buffer.Buffer;
import buffer.Buffer.StoppException;

public class SortiererThread<E extends Comparable<E>> {

	private final Buffer<E> input;
	private final E toBeSortedIn;
	private final Buffer<E> output;

	public SortiererThread(Buffer<E> input, E toBesortedIn) {
		this.input = input;
		this.toBeSortedIn = toBesortedIn;
		this.output = new Buffer<>();
	}

	public Buffer<E> getOutput() {
		return this.output;
	}

	public void startSort() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				SortiererThread.this.sort();
			}

		});
		thread.start();
	}

	private void sort() {
		boolean readingAndWriting = true;
		while (readingAndWriting) {
			try {
				E entry = this.input.get();
				if (this.toBeSortedIn.compareTo(entry) < 0) {
					this.output.put(this.toBeSortedIn);
					this.output.put(entry);
					this.putTheRest();
					readingAndWriting = false;
				} else {
					this.output.put(entry);
				}
			} catch (StoppException e) {
				readingAndWriting = false;
				this.output.put(this.toBeSortedIn);
				this.output.stopp();
			}
		}
	}

	private void putTheRest() {
		boolean readingAndWriting = true;
		while (readingAndWriting) {
			try {
				E entry = this.input.get();
				this.output.put(entry);
			} catch (StoppException e) {
				readingAndWriting = false;
				this.output.stopp();
			}
		}
	}

}
