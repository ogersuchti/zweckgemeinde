package mergeSort;

import buffer.Buffer;
import buffer.Buffer.StoppException;

public class Merger<E extends Comparable<E>> {

	private static final String MERGER = "Merger";
	private Buffer<E> mergedOutput;

	public Merger() {
		this.mergedOutput = new Buffer<>();
	}

	public Buffer<E> getMergedOutput() {
		return this.mergedOutput;
	}

	public void startMerge(Buffer<E> first,Buffer<E> second) {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				Merger.this.merge(first,second);
			}
		}, MERGER);
		thread.start();
	}

	private void merge(Buffer<E> firstInput, Buffer<E> secondInput) {
		boolean merging = true;
		boolean readFromSecond = true;
		E first = null;
		try {
			first = firstInput.get();
		} catch (StoppException e) {
			this.writeRestFromBuffer(secondInput);
			this.mergedOutput.stopp();
			merging = false;
		}
		E second = null;
		while (merging) {
				if (readFromSecond) {

					try {
						second = secondInput.get();
					} catch (StoppException e) {
						this.mergedOutput.put(first);
						this.writeRestFromBuffer(firstInput);
						merging = false;
						break; // TODO zweiter Ist null beim vgl. unten
					}
				}else {
					
					try {
						first = firstInput.get();
					} catch (StoppException e) {
						this.mergedOutput.put(second);
						this.writeRestFromBuffer(secondInput);
						merging = false;
					}
				}
				if(first.compareTo(second) < 0) {
					this.mergedOutput.put(first);
					readFromSecond = false;
				} else {
					this.mergedOutput.put(second);
					readFromSecond = true;
				}

		}
	}

	private void writeRestFromBuffer(Buffer<E> input) {
		boolean writing = true;
				while (writing) {
					try {
						E entry = input.get();
						this.mergedOutput.put(entry);
					} catch (StoppException e) {
						writing = false;
						this.mergedOutput.stopp();
					}
				}
	}

}
