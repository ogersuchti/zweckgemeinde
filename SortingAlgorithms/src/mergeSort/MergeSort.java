package mergeSort;

import java.util.List;

import buffer.Buffer;
import buffer.Buffer.StoppException;
import general.SortingAlgorithm;

public class MergeSort<E extends Comparable<E>> implements SortingAlgorithm<E> {

	private final Buffer<E> output;

	public MergeSort() {
		this.output = new Buffer<>();
	}

	@Override
	public List<E> sort(List<E> toBeSorted) {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				MergeSort.this.mergeSort(toBeSorted);
			}
		});
		thread.start();
		return this.output.toList();
	}

	protected void mergeSort(List<E> toBeSorted) {
		Buffer<E> initialInput = new Buffer<E>();
		initialInput.putListElementsWithStop(toBeSorted);
		Splitter<E> splitter = new Splitter<>(initialInput);
		splitter.splitAndMerge();
		this.writeOutput(splitter.getMergerOutput());
	}

	private void writeOutput(Buffer<E> buffer) {
		boolean readAndWrite = true;
		while (readAndWrite) {
			try {
				E entry = buffer.get();
				this.output.put(entry);
			} catch (StoppException e) {
				this.output.stopp();
				readAndWrite = false;
			}
		}
	}

}
