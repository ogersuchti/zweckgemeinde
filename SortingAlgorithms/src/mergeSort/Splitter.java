package mergeSort;

import buffer.Buffer;
import buffer.Buffer.StoppException;

public class Splitter<E extends Comparable<E>> {

	private static final String SPLITTER = "Splitter";
	private final Buffer<E> input;
	private final Buffer<E> splittedOutput1;
	private final Buffer<E> splittedOutput2;
	private Merger<E> myMerger;

	public Splitter(Buffer<E> input) {
		super();
		this.input = input;
		this.splittedOutput1 = new Buffer<>();
		this.splittedOutput2 = new Buffer<>();
		this.myMerger = new Merger<>();
	}

	public Buffer<E> getSplittedOutput1() {
		return this.splittedOutput1;
	}

	public Buffer<E> getSplittedOutput2() {
		return this.splittedOutput2;
	}

	public void startSplit() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				Splitter.this.split();
			}
		}, SPLITTER);
		thread.start();
	}
	public void startSplitAndMerge() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				Splitter.this.splitAndMerge();
			}
		}, SPLITTER);
		thread.start();
	}

	protected void splitAndMerge() {
		boolean splitting = true;
		int splitCount = 0;
		while (splitting) {
			try {
				E entry1 = this.input.get();
				this.splittedOutput1.put(entry1);
				splitCount ++;
				E entry2 = this.input.get();
				this.splittedOutput2.put(entry2);
			} catch (StoppException e) {
				splitting = false;
				this.splittedOutput1.stopp();
				this.splittedOutput2.stopp();
				if(splitCount <= 1) {
					this.myMerger.startMerge(this.splittedOutput1,this.splittedOutput2);
				} else {
					Splitter<E> splitter1 = new Splitter<>(this.splittedOutput1);
					splitter1.splitAndMerge();
					Splitter<E> splitter2 = new Splitter<>(this.splittedOutput2);
					splitter2.splitAndMerge();
					this.myMerger.startMerge(splitter1.getMergerOutput(), splitter2.getMergerOutput());
				}
			}
		}
	}

	public Buffer<E> getMergerOutput() {
		return this.myMerger.getMergedOutput();
	}

	private void split() {
		boolean splitting = true;
		while (splitting) {
			try {
				E entry1 = this.input.get();
				this.splittedOutput1.put(entry1);
				E entry2 = this.input.get();
				this.splittedOutput2.put(entry2);
			} catch (StoppException e) {
				splitting = false;
				this.splittedOutput1.stopp();
				this.splittedOutput2.stopp();
			}
		}
	}

}
