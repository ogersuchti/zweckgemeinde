package mergeSortTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import mergeSortTest.TestMerge;
import mergeSortTest.TestMergeSort;
import mergeSortTest.TestSplit;
import mergeSortTest.TestSplitAndMerge;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestMergeSort.class,TestSplitAndMerge.class, TestMerge.class,TestSplit.class })
/**
 * Ich fuehre alle oben genannten Testklassen aus.
 * @author tim
 *
 */
public class AllTestsMergeSort {
	// the class remains empty,
	// used only as a holder for the above annotations
}