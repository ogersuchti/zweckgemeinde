package mergeSortTest;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;

import buffer.Buffer;
import buffer.Buffer.StoppException;
import mergeSort.Merger;
import mergeSort.Splitter;

public class TestSplitAndMerge {

	@Test
	public void testSingletonSplitAndMergeNotSorted() throws StoppException {
		Buffer<BigInteger> toBeSplittedAndMerged = new Buffer<>();
		toBeSplittedAndMerged.put(BigInteger.valueOf(2));
		toBeSplittedAndMerged.put(BigInteger.valueOf(1));
		toBeSplittedAndMerged.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(2));
		this.testSplitAndMerge(toBeSplittedAndMerged, expectedValues);
	}

	@Test
	public void testSingletonSplitAndMergeSorted() throws StoppException {
		Buffer<BigInteger> toBeSplittedAndMerged = new Buffer<>();
		toBeSplittedAndMerged.put(BigInteger.valueOf(1));
		toBeSplittedAndMerged.put(BigInteger.valueOf(2));
		toBeSplittedAndMerged.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(2));
		this.testSplitAndMerge(toBeSplittedAndMerged, expectedValues);
	}

	@Test
	public void test2xSplitAndMerge() throws StoppException {
		Buffer<BigInteger> toBeSplittedAndMerged = new Buffer<>();
		toBeSplittedAndMerged.put(BigInteger.valueOf(2));
		toBeSplittedAndMerged.put(BigInteger.valueOf(4));
		toBeSplittedAndMerged.put(BigInteger.valueOf(1));
		toBeSplittedAndMerged.put(BigInteger.valueOf(3));
		toBeSplittedAndMerged.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(2));
		expectedValues.add(BigInteger.valueOf(3));
		expectedValues.add(BigInteger.valueOf(4));
		this.test2xSplitAndMerge(toBeSplittedAndMerged, expectedValues);
	}

	private void testSplitAndMerge(Buffer<BigInteger> toBeSplittedAndMerged, Collection<BigInteger> expectedValues) throws StoppException {
		Splitter<BigInteger> splitter = new Splitter<>(toBeSplittedAndMerged);
		splitter.startSplit();
		Merger<BigInteger> merger = new Merger<>();
		merger.startMerge(splitter.getSplittedOutput1(), splitter.getSplittedOutput2());
		Buffer<BigInteger> out = merger.getMergedOutput();
		Iterator<BigInteger> it = expectedValues.iterator();
		while (it.hasNext()) {
			BigInteger current = it.next();
				Assert.assertEquals(current, out.get());
		}
		this.testNextIsStop(out);
	}

	private void test2xSplitAndMerge(Buffer<BigInteger> toBeSplittedAndMerged, Collection<BigInteger> expectedValues) throws StoppException {
		Splitter<BigInteger> splitter = new Splitter<>(toBeSplittedAndMerged);
		splitter.startSplit();
		Splitter<BigInteger> splitter2 = new Splitter<>(splitter.getSplittedOutput1());
		splitter2.startSplit();
		Splitter<BigInteger> splitter3 = new Splitter<>(splitter.getSplittedOutput2());
		splitter3.startSplit();
		Merger<BigInteger> merger1 = new Merger<>();
		merger1.startMerge(splitter3.getSplittedOutput1(), splitter3.getSplittedOutput2());
		Merger<BigInteger> merger = new Merger<>();
		Merger<BigInteger> merger3 = new Merger<>();
		merger3.startMerge(merger.getMergedOutput(), merger1.getMergedOutput());
		merger.startMerge(splitter2.getSplittedOutput1(), splitter2.getSplittedOutput2());
		Buffer<BigInteger> finalOutput = merger3.getMergedOutput();
		Iterator<BigInteger> it = expectedValues.iterator();
		while (it.hasNext()) {
			BigInteger current = it.next();
			Assert.assertEquals(current, finalOutput.get());
		}
		this.testNextIsStop(finalOutput);
		
	}

	private void testNextIsStop(Buffer<BigInteger> buffer) {
		try {
			buffer.get();
			Assert.fail();
		} catch (StoppException e) {
		}
	}

}
