package mergeSortTest;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;

import buffer.Buffer;
import buffer.Buffer.StoppException;
import mergeSort.Splitter;

public class TestSplit {


	@Test
	public void testSplit() throws StoppException {
		Buffer<BigInteger> toBeSplitted = new Buffer<>();
		toBeSplitted.put(BigInteger.valueOf(1));
		toBeSplitted.put(BigInteger.valueOf(2));
		toBeSplitted.put(BigInteger.valueOf(3));
		toBeSplitted.put(BigInteger.valueOf(4));
		toBeSplitted.stopp();
		Collection<BigInteger> expectedValues1 = new LinkedList<>();
		expectedValues1.add(BigInteger.valueOf(1));
		expectedValues1.add(BigInteger.valueOf(3));
		Collection<BigInteger> expectedValues2 = new LinkedList<>();
		expectedValues2.add(BigInteger.valueOf(2));
		expectedValues2.add(BigInteger.valueOf(4));
		this.testSplitter(toBeSplitted,expectedValues1,expectedValues2);
	}
	
	@Test
	public void testSplitZeroElements() throws StoppException {
		Buffer<BigInteger> toBeSplitted = new Buffer<>();
		toBeSplitted.stopp();
		this.testSplitter(toBeSplitted,new LinkedList<>(),new LinkedList<>());
	}
	@Test
	public void testSplitOneElements() throws StoppException {
		Buffer<BigInteger> toBeSplitted = new Buffer<>();
		toBeSplitted.put(BigInteger.valueOf(25));
		toBeSplitted.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(25));
		this.testSplitter(toBeSplitted,expectedValues, new LinkedList<>());
	}

	private void testSplitter(Buffer<BigInteger> toBeSplitted,Collection<BigInteger> expectedValues1, Collection<BigInteger> expectedValues2) throws StoppException   {
		Splitter<BigInteger> splitter = new Splitter<>(toBeSplitted);
		splitter.startSplit();
		Buffer<BigInteger> splittedOutput1 = splitter.getSplittedOutput1(); 
		Buffer<BigInteger> splittedOutput2 = splitter.getSplittedOutput2();
		Iterator<BigInteger> it1 = expectedValues1.iterator();
		while (it1.hasNext()) {
			BigInteger current = it1.next();
				BigInteger entry;
					entry = splittedOutput1.get();
				Assert.assertEquals(current, entry);
		}
		Iterator<BigInteger> it2 = expectedValues2.iterator();
		while (it2.hasNext()) {
			BigInteger current = it2.next();
			BigInteger entry = splittedOutput2.get();
			Assert.assertEquals(current, entry);
		}
		this.testNextIsStop(splittedOutput1);
		this.testNextIsStop(splittedOutput2);
	}
	private void testNextIsStop(Buffer<BigInteger> buffer) {
		try {
			buffer.get();
			Assert.fail();
		} catch (StoppException e) {
		}
	}
	
}
