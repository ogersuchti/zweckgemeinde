package mergeSortTest;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;

import buffer.Buffer;
import buffer.Buffer.StoppException;
import mergeSort.Merger;

public class TestMerge {

	
	
	@Test
	public void testMergeZeroFirst() throws StoppException {
		Buffer<BigInteger> first = new Buffer<>();
		first.stopp();
		Buffer<BigInteger> second = new Buffer<>();
		second.put(BigInteger.valueOf(1));
		second.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
	}
	@Test
	public void testMergeZeroSecond() throws StoppException {
		Buffer<BigInteger> first = new Buffer<>();
		first.put(BigInteger.valueOf(1));
		first.stopp();
		Buffer<BigInteger> second = new Buffer<>();
		second.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		this.testMerger(first,second,expectedValues);
	}
	
	
	@Test
	public void testMergeSingletons() throws StoppException {
		Buffer<BigInteger> first = new Buffer<>();
		first.put(BigInteger.valueOf(1));
		first.stopp();
		Buffer<BigInteger> second = new Buffer<>();
		second.put(BigInteger.valueOf(4));
		second.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(4));
		this.testMerger(first,second,expectedValues);
	}
	
	@Test
	public void testMergeSimple() throws StoppException {
		Buffer<BigInteger> first = new Buffer<>();
		first.put(BigInteger.valueOf(1));
		first.put(BigInteger.valueOf(2));
		first.stopp();
		Buffer<BigInteger> second = new Buffer<>();
		second.put(BigInteger.valueOf(3));
		second.put(BigInteger.valueOf(4));
		second.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(2));
		expectedValues.add(BigInteger.valueOf(3));
		expectedValues.add(BigInteger.valueOf(4));
		this.testMerger(first,second,expectedValues);
	}
	
	@Test
	public void testMergeComplicated() throws StoppException {
		Buffer<BigInteger> first = new Buffer<>();
		first.put(BigInteger.valueOf(1));
		first.put(BigInteger.valueOf(2));
		first.put(BigInteger.valueOf(3));
		first.put(BigInteger.valueOf(4));
		first.put(BigInteger.valueOf(5));
		first.put(BigInteger.valueOf(6));
		first.put(BigInteger.valueOf(7));
		first.stopp();
		Buffer<BigInteger> second = new Buffer<>();
		second.put(BigInteger.valueOf(8));
		second.put(BigInteger.valueOf(9));
		second.stopp();
		Collection<BigInteger> expectedValues = new LinkedList<>();
		expectedValues.add(BigInteger.valueOf(1));
		expectedValues.add(BigInteger.valueOf(2));
		expectedValues.add(BigInteger.valueOf(3));
		expectedValues.add(BigInteger.valueOf(4));
		expectedValues.add(BigInteger.valueOf(5));
		expectedValues.add(BigInteger.valueOf(6));
		expectedValues.add(BigInteger.valueOf(7));
		expectedValues.add(BigInteger.valueOf(8));
		expectedValues.add(BigInteger.valueOf(9));
		this.testMerger(first,second,expectedValues);
	}

	private void testMerger(Buffer<BigInteger> first, Buffer<BigInteger> second,
			Collection<BigInteger> expectedValues) throws StoppException {
		Merger<BigInteger> merger = new Merger<>();
		merger.startMerge(first, second);
		Buffer<BigInteger> mergedOutput = merger.getMergedOutput();
		Iterator<BigInteger> it = expectedValues.iterator();
		while (it.hasNext()) {
			BigInteger current = it.next();
			BigInteger entry = mergedOutput.get();
			Assert.assertEquals(current,entry);
		}
		try {
			mergedOutput.get();
			Assert.fail();
		}
		catch(StoppException e) {
		}
	}
}
