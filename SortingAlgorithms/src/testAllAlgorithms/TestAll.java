package testAllAlgorithms;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import bubbleSortTest.AllTestsBubbleSort;
import insertionSortTest.AllTestsInsertionSort;
import mergeSortTest.AllTestsMergeSort;
import quickSortTest.TestQuickSort;

@RunWith(Suite.class)
@Suite.SuiteClasses({AllTestsMergeSort.class,AllTestsBubbleSort.class,AllTestsInsertionSort.class,TestQuickSort.class})
/**
 * Ich fuehre alle oben genannten Testklassen aus.
 * @author tim
 *
 */
public class TestAll {
	// the class remains empty,
	// used only as a holder for the above annotations
}