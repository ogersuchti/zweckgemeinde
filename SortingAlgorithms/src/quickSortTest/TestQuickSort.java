package quickSortTest;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import buffer.Buffer.StoppException;
import general.SortingAlgorithm;
import quickSort.QuickSort;

public class TestQuickSort {

	@Test
	public void testZeroInput() throws StoppException {
		List<BigInteger> tobeSorted = new LinkedList<>();
		this.vglOutput(tobeSorted);
	}

	@Test
	public void testOneInput() throws StoppException {
		List<BigInteger> tobeSorted = new LinkedList<>();
		tobeSorted.add(BigInteger.valueOf(1));
		this.vglOutput(tobeSorted);
	}

	@Test
	public void test2Input() throws StoppException {
		List<BigInteger> toBeSorted = new LinkedList<>();
		toBeSorted.add(BigInteger.valueOf(2));
		toBeSorted.add(BigInteger.valueOf(1));
		this.vglOutput(toBeSorted);
	}

	@Test
	public void testSameInput() throws StoppException {
		List<BigInteger> toBeSorted = new LinkedList<>();
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(1));
		toBeSorted.add(BigInteger.valueOf(1));
		this.vglOutput(toBeSorted);
	}

	@Test
	public void testMultipleInput() throws StoppException {
		List<BigInteger> toBeSorted1 = new LinkedList<>();
		toBeSorted1.add(BigInteger.valueOf(3));
		toBeSorted1.add(BigInteger.valueOf(7));
		toBeSorted1.add(BigInteger.valueOf(1));
		toBeSorted1.add(BigInteger.valueOf(5));
		toBeSorted1.add(BigInteger.valueOf(2));
		toBeSorted1.add(BigInteger.valueOf(6));
		toBeSorted1.add(BigInteger.valueOf(4));
		toBeSorted1.add(BigInteger.valueOf(6));
		toBeSorted1.add(BigInteger.valueOf(9));
		toBeSorted1.add(BigInteger.valueOf(10));
		toBeSorted1.add(BigInteger.valueOf(6));
		toBeSorted1.add(BigInteger.valueOf(8));
		this.vglOutput(toBeSorted1);

		List<BigInteger> toBeSorted2 = new LinkedList<>();
		toBeSorted2.add(BigInteger.valueOf(2));
		toBeSorted2.add(BigInteger.valueOf(7));
		toBeSorted2.add(BigInteger.valueOf(19));
		toBeSorted2.add(BigInteger.valueOf(23));
		toBeSorted2.add(BigInteger.valueOf(100));
		toBeSorted2.add(BigInteger.valueOf(6));
		toBeSorted2.add(BigInteger.valueOf(4));
		toBeSorted2.add(BigInteger.valueOf(22));
		toBeSorted2.add(BigInteger.valueOf(9));
		toBeSorted2.add(BigInteger.valueOf(10));
		toBeSorted2.add(BigInteger.valueOf(11));
		toBeSorted2.add(BigInteger.valueOf(8));
		this.vglOutput(toBeSorted2);
	}

	private void vglOutput(List<BigInteger> toBeSorted) throws StoppException {
		SortingAlgorithm<BigInteger> sortierer = new QuickSort<>();
		List<BigInteger> sortedList = sortierer.sort(toBeSorted);
		Collections.sort(toBeSorted);
		Assert.assertEquals(toBeSorted, sortedList);
	}

}
