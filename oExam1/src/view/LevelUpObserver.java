package view;

import model.DownEvent;
import model.Level;
import model.LevelEvent;
import model.LevelVisitor;
import model.UpEvent;

public class LevelUpObserver extends AbstractLevelObserver {
	
	public static LevelUpObserver create(final Level level){
		return new LevelUpObserver(level);
	}
	
	private LevelUpObserver(final Level level) {
		super(level);
	}

	@Override
	public void concreteUpdate(final LevelEvent e) {
		e.accept(new LevelVisitor() {
			
			@Override
			public void handleUp(final UpEvent upEvent) {
				LevelUpObserver.this.setCount(LevelUpObserver.this.getCount() + 1);
				
			}
			
			@Override
			public void handleDown(final DownEvent downEvent) {
				
			}
		});
	}

}
