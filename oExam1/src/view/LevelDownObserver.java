package view;

import model.DownEvent;
import model.Level;
import model.LevelEvent;
import model.LevelVisitor;
import model.UpEvent;

public class LevelDownObserver extends AbstractLevelObserver {
	
	public static LevelDownObserver create(final Level observee){
		return new LevelDownObserver(observee);
	}
	
	private LevelDownObserver(final Level observee) {
		super(observee);
	}

	@Override
	public void concreteUpdate(final LevelEvent e) {
		e.accept(new LevelVisitor() {
			
			@Override
			public void handleUp(final UpEvent upEvent) {
				
			}
			
			@Override
			public void handleDown(final DownEvent downEvent) {
				LevelDownObserver.this.setCount(LevelDownObserver.this.getCount() + 1);
				
			}
		});
		
		
	}

}
