package view;


import model.Level;
import model.LevelEvent;
import model.Observer;

public abstract class AbstractLevelObserver implements Observer {
	
	public static final int STARTCOUNT = 0;
	
	private int count;
	
	private final Level level;
	
	protected AbstractLevelObserver(final Level level){
		this.count = STARTCOUNT;
		this.level = level;
		level.register(this);
	}

	public int getCount() {
		return count;
	}

	protected void setCount(final int count) {
		this.count = count;
	}

	protected Level getLevel() {
		return level;
	}

	/**
	 * Hint: Update realised by "Template Method"!
	 */
	@Override
	public void update(final LevelEvent e){
		this.concreteUpdate(e);
	}

	abstract protected void concreteUpdate(LevelEvent e);

}
