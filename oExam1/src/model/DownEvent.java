package model;

public class DownEvent implements LevelEvent {

	private static DownEvent theDownEvent;

	public static DownEvent getInstance() {
		if (theDownEvent == null) {
			theDownEvent = new DownEvent();
		}
		return theDownEvent;
	}

	private DownEvent() {

	}

	@Override
	public void accept(final LevelVisitor v) {
		v.handleDown(this);

	}

}
