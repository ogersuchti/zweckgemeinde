package model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class Observee {

	private final Collection<Observer> currentObservers;
	
	protected Observee(){
		this.currentObservers = new LinkedList<>();
	}
	
	public void register(final Observer observer){
		this.currentObservers.add(observer);
	}
	public void deregister(final Observer observer){
		this.currentObservers.remove(observer);
	}
	protected void notifyObservers(final LevelEvent e){
		final Iterator<Observer> observerIterator = this.currentObservers.iterator();
		while (observerIterator.hasNext()){
			final Observer current = observerIterator.next();
			current.update(e);
		}
	}
}
