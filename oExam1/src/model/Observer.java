package model;

public interface Observer {
	void update(LevelEvent e);
}
