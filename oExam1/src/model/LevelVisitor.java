package model;

public interface LevelVisitor {

	void handleDown(DownEvent downEvent);

	void handleUp(UpEvent upEvent);

}
