package model;

public interface LevelEvent {

	void accept(LevelVisitor v);
}
