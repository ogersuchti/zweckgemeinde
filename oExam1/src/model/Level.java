package model;

public class Level extends Observee {

	public static final int STARTLEVEL = 0;

	public static Level create() {
		return new Level(STARTLEVEL);
	}

	private int currentLevel;

	private Level(final int level) {
		super();
		this.currentLevel = level;
	}

	public void setCurrentLevel(final int level) {
		if (currentLevel < level) {
			this.notifyObservers(UpEvent.getInstance());
		} else if (currentLevel > level) {
			this.notifyObservers(DownEvent.getInstance());

		}
		this.currentLevel = level;
	}

	public int getCurrentLevel() {
		return this.currentLevel;
	}
}
