package model;

public class UpEvent implements LevelEvent{

	
	private static UpEvent theUpEvent;

	public static UpEvent getInstance() {
		if (theUpEvent == null) {
			theUpEvent = new UpEvent();
		}
		return theUpEvent;
	}

	private UpEvent() {

	}
	
	@Override
	public void accept(final LevelVisitor v) {
		v.handleUp(this);
		
	}
	
}
