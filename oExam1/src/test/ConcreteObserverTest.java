package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Level;
import view.AbstractLevelObserver;
import view.LevelDownObserver;
import view.LevelUpObserver;

public class ConcreteObserverTest {

	@Test
	public void test() {
		final Level observee = Level.create();
		final LevelUpObserver upObserver = LevelUpObserver.create(observee);
		final LevelDownObserver downObserver = LevelDownObserver.create(observee);

		observee.setCurrentLevel(Level.STARTLEVEL + 1);
		assertEquals(AbstractLevelObserver.STARTCOUNT + 1, upObserver.getCount());
		assertEquals(AbstractLevelObserver.STARTCOUNT, downObserver.getCount());
		
		observee.setCurrentLevel(Level.STARTLEVEL + 2);
		assertEquals(AbstractLevelObserver.STARTCOUNT + 2, upObserver.getCount());
		assertEquals(AbstractLevelObserver.STARTCOUNT, downObserver.getCount());
		
		observee.setCurrentLevel(Level.STARTLEVEL + 1);
		assertEquals(AbstractLevelObserver.STARTCOUNT + 2, upObserver.getCount());
		assertEquals(AbstractLevelObserver.STARTCOUNT + 1, downObserver.getCount());
		
		observee.setCurrentLevel(Level.STARTLEVEL + 2);
		assertEquals(AbstractLevelObserver.STARTCOUNT + 3, upObserver.getCount());
		assertEquals(AbstractLevelObserver.STARTCOUNT + 1, downObserver.getCount());
		
		observee.setCurrentLevel(Level.STARTLEVEL - 5);
		assertEquals(AbstractLevelObserver.STARTCOUNT + 3, upObserver.getCount());
		assertEquals(AbstractLevelObserver.STARTCOUNT + 2, downObserver.getCount());
		
		observee.setCurrentLevel(Level.STARTLEVEL);
		assertEquals(AbstractLevelObserver.STARTCOUNT + 4, upObserver.getCount());
		assertEquals(AbstractLevelObserver.STARTCOUNT + 2, downObserver.getCount());

		observee.setCurrentLevel(Level.STARTLEVEL);
		assertEquals(AbstractLevelObserver.STARTCOUNT + 4, upObserver.getCount());
		assertEquals(AbstractLevelObserver.STARTCOUNT + 2, downObserver.getCount());
}

}
