package start;

import model.Counter;
import view.CounterObserver;

public class Observer2Starter {

	public static void main(final String[] args) {
		final int InitialMargin = 50;
		final int MarginDelta = 220;
		int currentMargin = InitialMargin;
		final Counter theCounter1 = Counter.createCounter();
		final Counter theCounter2 = Counter.createCounter();
		final CounterObserver observer1 = CounterObserver.createCounterObserver(theCounter1, theCounter2);
		observer1.setLocation(currentMargin, currentMargin);
		observer1.setVisible(true);
		currentMargin = currentMargin + MarginDelta;
		final CounterObserver observer2 = CounterObserver.createCounterObserver(theCounter1, theCounter2);
		observer2.setLocation(currentMargin, currentMargin);
		observer2.setVisible(true);
		currentMargin = currentMargin + MarginDelta;
		final CounterObserver observer3 = CounterObserver.createCounterObserver(theCounter1, theCounter2);
		observer3.setLocation(currentMargin, currentMargin);
		observer3.setVisible(true);
	}

}
