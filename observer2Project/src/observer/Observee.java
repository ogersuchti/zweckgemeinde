package observer;

import java.util.Collection;
import java.util.LinkedList;

public abstract class Observee {
	private final Collection<Observer> observers;

	/**
	 * Constructor of the class
	 */
	protected Observee() {
		this.observers = new LinkedList<>();
	}

	/**
	 * @param o
	 *            Observer that will be registered
	 */
	public void register(final Observer o) {
		this.observers.add(o);
	}

	/**
	 * @param o
	 *            Observer that will be deregistered
	 */
	public void deregister(final Observer o) {
		this.observers.remove(o);
	}

	/**
	 * notifies all Observers that are registered
	 * 
	 * @param e
	 *            Event that will be transmitted
	 */
	protected void notifyObservers(final Event e) {
		for (final Observer observer : observers) {
			observer.update(e);
		}
	}

}
