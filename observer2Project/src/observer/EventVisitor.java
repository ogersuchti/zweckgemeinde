package observer;

public interface EventVisitor {

	void handleValueChangedEvent(ValueChangedEvent e);

}
