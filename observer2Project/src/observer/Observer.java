package observer;

public interface Observer {

	/**
	 * updates the elements of this Observer
	 * 
	 * @param e
	 *            Event that will be transmitted
	 */
	void update(Event e);
}
