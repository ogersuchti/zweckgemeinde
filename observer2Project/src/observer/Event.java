package observer;

public interface Event {
	void accept(EventVisitor v);
}
