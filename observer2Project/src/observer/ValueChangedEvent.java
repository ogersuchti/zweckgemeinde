package observer;

public class ValueChangedEvent implements Event {

	private static ValueChangedEvent instance;

	private ValueChangedEvent() {
	}

	@Override
	public void accept(final EventVisitor v) {
		v.handleValueChangedEvent(this);
	}

	public static ValueChangedEvent getInstance() {
		if (instance == null) {
			instance = new ValueChangedEvent();
		}
		return instance;
	}

}
