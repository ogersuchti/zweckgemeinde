package view;

import model.Counter;
import observer.Event;
import observer.Observer;

@SuppressWarnings("serial")
public class CounterObserver extends View {

	public static CounterObserver createCounterObserver(final Counter counter1, final Counter counter2) {
		return new CounterObserver(counter1, counter2);
	}

	private final Counter counter1;
	private final Counter counter2;

	private final Observer adapter1;
	private final Observer adapter2;

	private CounterObserver(final Counter counter1, final Counter counter2) {
		this.counter1 = counter1;
		this.counter2 = counter2;
		adapter1 = new Observer() {
			@Override
			public void update(final Event e) {
				refresh1();
			}
		};
		adapter2 = new Observer() {

			@Override
			public void update(final Event e) {
				refresh2();
			}
		};
	}

	private Counter getCounter1() {
		return this.counter1;
	}

	private Counter getCounter2() {
		return this.counter2;
	}

	private void refresh1() {
		super.refreshView1(this.getCounter1().getCurrentValue());
	}

	private void refresh2() {
		super.refreshView2(this.getCounter2().getCurrentValue());
	}

	@Override
	protected void deregister1() {
		this.counter1.deregister(adapter1);
	}

	@Override
	protected void register1() {
		this.counter1.register(adapter1);
	}

	@Override
	protected void deregister2() {
		this.counter2.deregister(adapter2);
	}

	@Override
	protected void register2() {
		this.counter2.register(adapter2);
	}

	@Override
	protected void down1() {
		this.getCounter1().down();
		this.refresh1();
	}

	@Override
	protected void up1() {
		this.getCounter1().up();
		this.refresh1();
	}

	@Override
	protected void down2() {
		this.getCounter2().down();
		this.refresh2();
	}

	@Override
	protected void up2() {
		this.getCounter2().up();
		this.refresh2();
	}
}
