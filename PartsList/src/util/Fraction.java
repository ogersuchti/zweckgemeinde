package util;

import java.math.BigInteger;

public class Fraction {
	
	final private BigInteger enumerator;
	final private BigInteger denominator;

	public static final Fraction ZERO = new Fraction(BigInteger.ZERO, BigInteger.ONE);
	public static final Fraction ONE = new Fraction(BigInteger.ONE, BigInteger.ONE);

	private static final String SEPARATOR_FRACTION = "/";
	private static final String SEPARATOR_DECIMAL = ".";
	private static final String SEPARATOR_SCIENCE = "E";
	private static final String ONE_STRING = "1";
	private static final char MINUS_CHAR = '-';
	private static final int SEPARATOR_FRACTION_LENGTH = SEPARATOR_FRACTION.length();
	private static final int SEPARATOR_DECIMAL_LENGTH = SEPARATOR_DECIMAL.length();
	private static final int SEPARATOR_SCIENCE_LENGTH = SEPARATOR_SCIENCE.length();
	

	
	private Fraction(final BigInteger enumerator, final BigInteger denominator) {
		this.enumerator = enumerator;
		this.denominator = denominator;
	}

	/**
	 * Returns the fraction the toString of which is fractionString
	 * 
	 * @throws FractionConstructionException when construction fails.
	 */
	public static Fraction create(String fractionString) throws FractionConstructionException {
		fractionString = normaliseFractionString(fractionString);
		final int positionTrennzeichen = fractionString.indexOf(SEPARATOR_FRACTION);
		try {
			final String enumeratorString = fractionString.substring(0, positionTrennzeichen);
			final String denominatorString = fractionString.substring(positionTrennzeichen + SEPARATOR_FRACTION_LENGTH,
					fractionString.length());
			return create(new BigInteger(enumeratorString), new BigInteger(denominatorString));
		} catch (final IndexOutOfBoundsException | NumberFormatException e) {
			throw new FractionConstructionException(e);
		}
	}

	private static String normaliseFractionString(String fractionString) throws FractionConstructionException {
		String normalisedFractionString;
		if (fractionString.startsWith(SEPARATOR_FRACTION)) {
			normalisedFractionString = ONE_STRING + fractionString;
		} else {
			if (fractionString.contains(SEPARATOR_DECIMAL)) {
				final int positionTrennzeichen = fractionString.indexOf(SEPARATOR_DECIMAL);
				try {
					final String beforePoint = fractionString.substring(0, positionTrennzeichen);
					final String afterPoint = fractionString.substring(positionTrennzeichen + SEPARATOR_DECIMAL_LENGTH,
							fractionString.length());
					normalisedFractionString = beforePoint + afterPoint + SEPARATOR_FRACTION
							+ BigInteger.TEN.pow(afterPoint.length()).toString();
				} catch (final IndexOutOfBoundsException | NumberFormatException e) {
					throw new FractionConstructionException(e);
				}
			} else {
				if (fractionString.contains(SEPARATOR_SCIENCE)) {
					final int positionTrennzeichen = fractionString.indexOf(SEPARATOR_SCIENCE);
					final String beforeE = fractionString.substring(0, positionTrennzeichen);
					final String exponent = fractionString.substring(positionTrennzeichen + 2);
					// fractionString.substring(positionTrennzeichen,positionTrennzeichen
					// + "E".length()).equals("-"))
					if (fractionString.charAt(positionTrennzeichen + SEPARATOR_SCIENCE_LENGTH) == MINUS_CHAR) {
						normalisedFractionString = beforeE + SEPARATOR_FRACTION
								+ BigInteger.TEN.pow(new BigInteger(exponent).intValue());
					} else {
						normalisedFractionString = new BigInteger(beforeE).multiply(
								BigInteger.TEN.pow(new BigInteger(exponent).intValue())) + SEPARATOR_FRACTION + ONE_STRING;
					}

				} else {
					if (fractionString.contains(SEPARATOR_FRACTION)) {
						normalisedFractionString = fractionString;
					} else {
						normalisedFractionString = fractionString + SEPARATOR_FRACTION + ONE_STRING;
					}
				}
			}
		}
		return normalisedFractionString;
	}

	/**
	 * Returns a fraction in normalised form, i. e. enumerator and denominator
	 * have been divided by its gcd .
	 * 
	 * @param enumerator
	 *            is the intended enumerator of this result.
	 * @param denominator
	 *            is the intended denominator of this result.
	 * @return the normalised fraction equal to enumerator / denominator.
	 * @throws Exception
	 *             FractionConstructionException if the given denominator is
	 *             equal to zero.
	 */
	public static Fraction create(BigInteger enumerator, BigInteger denominator) throws FractionConstructionException {
		if (denominator.equals(BigInteger.ZERO))
			throw new FractionConstructionException();
		return new Fraction(enumerator, denominator).normalise();
	}

	@Override
	public boolean equals(final Object object) {
		if (object instanceof Fraction) {
			final Fraction argument = (Fraction) object;
			return this.enumerator.multiply(argument.denominator)
					.equals(argument.enumerator.multiply(this.denominator));
		} else {
			return false;
		}
	}

	private Fraction normalise() {
		final BigInteger gcd = this.denominator.gcd(this.enumerator);
		return new Fraction(this.enumerator.divide(gcd), this.denominator.divide(gcd));
	}

	@Override
	public int hashCode() {
		return this.normalise().enumerator.hashCode();
	}

	@Override
	/**
	 * @return The String representing the value of the receiver object.
	 */
	public String toString() {
		return this.enumerator + "/" + this.denominator;
	}

	/**
	 * Return the fraction representing the sum of receiver and summand.
	 * 
	 * @param summand
	 * @return the sum of receiver and summand.
	 */
	public Fraction add(Fraction summand) {

		try {
			return create(enumerator.multiply(summand.denominator).add(summand.enumerator.multiply(this.denominator)),
					this.denominator.multiply(summand.denominator));
		} catch (final Exception e) {
			throw new Error(e);
		}
	}

	/**
	 * Returns the fraction representing the receiver subtracted by minuend.
	 * 
	 * @param minuend
	 * @return the receiver subtracted by minuend.
	 */
	public Fraction subtract(Fraction minuend) {

		try {
			return create(
					enumerator.multiply(minuend.denominator).subtract(minuend.enumerator.multiply(this.denominator)),
					this.denominator.multiply(minuend.denominator));
		} catch (final Exception e) {
			throw new Error(e);
		}
	}

	/**
	 * 
	 * @param factor
	 *            for the returned product.
	 * @return the product from the receiver and factor.
	 */
	public Fraction multiply(Fraction factor) {

		try {
			return create(this.enumerator.multiply(factor.enumerator), this.denominator.multiply(factor.denominator));
		} catch (final Exception e) {
			throw new Error(e);
		}
	}

	/**
	 * Returns the quotient of the receiver (as divident) and the argument
	 * divisor.
	 * 
	 * @param divisor
	 * @return Quotient of reveiver and divisor.
	 */
	public Fraction divide(Fraction divisor) throws FractionConstructionException {
		return this.multiply(divisor.inverseFraction());
	}


	private Fraction inverseFraction() throws FractionConstructionException {
		if (this.enumerator.equals(BigInteger.ZERO))
			throw new FractionConstructionException();
		return create(this.denominator, this.enumerator);
	}
}
