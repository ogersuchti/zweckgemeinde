package util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

public class FractionTest {
	public static Fraction f1_2;
	public static Fraction f1_3;
	public static Fraction f1_4;
	public static Fraction f1_5;
	public static Fraction f2_5;

	@Before
	public void setUp() {
		try {
			f1_2 = Fraction.create(BigInteger.ONE, new BigInteger("2"));
			f1_3 = Fraction.create(BigInteger.ONE, new BigInteger("3"));
			f1_4 = Fraction.create(BigInteger.ONE, new BigInteger("4"));
			f1_5 = Fraction.create(BigInteger.ONE, new BigInteger("5"));
			f2_5 = Fraction.create(new BigInteger("2"), new BigInteger("5"));
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void createFromBigInteger() {
		try {
			assertEquals(Fraction.create(BigInteger.ONE, BigInteger.TEN),
					Fraction.create(BigInteger.ONE, BigInteger.TEN));
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void testFractionCreationException() {
		try {
			assertEquals(Fraction.create(BigInteger.ONE, BigInteger.TEN),
					Fraction.create(BigInteger.ONE, BigInteger.ZERO));
			fail();
		} catch (final Exception e) {
		}

	}

	@Test
	public void AdditionTest() {
		assertEquals(f1_2, f1_4.add(f1_4));
		try {
			assertEquals(Fraction.ONE, f1_4.add(f1_4).add(f1_4).add(f1_4));
		} catch (final Exception e) {
			fail();
		}

	}

	@Test
	public void SubtractTest() {
		assertEquals(f1_5, f2_5.subtract(f1_5));
		try {
			assertEquals(Fraction.ZERO, f2_5.subtract(f1_5).subtract(f1_5));
		} catch (final Exception e) {
			fail();
		}

	}

	@Test
	public void MulTest() {
		assertEquals(f1_4, f1_2.multiply(f1_2));
		assertEquals(Fraction.ZERO, f1_2.multiply(Fraction.ZERO));

	}

	@Test
	public void DivideTest() {
		try {
			assertEquals(Fraction.create(BigInteger.ONE, BigInteger.ONE), f1_4.divide(f1_4));
		} catch (final Exception e) {
			fail();
		}

		try {
			assertEquals(Fraction.create(BigInteger.ONE, BigInteger.ONE),
					f1_4.divide(Fraction.create(BigInteger.ZERO, BigInteger.TEN)));
			fail();
		} catch (final FractionConstructionException e) {
		}
	}

	@Test
	public void createFromStringWithOutSeparatorTest() {
		try {
			assertEquals(Fraction.ONE, Fraction.create("1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.ZERO, Fraction.create("0"));
		} catch (final Exception e) {
			fail();
		}

	}

	@Test
	public void createFromStringInconsistentParts() {
		try {
			Fraction.create("hugo/egon");
			fail();
		} catch (final Exception e) {
		}
	}

	@Test
	public void createFromStringWithSeparator() {
		try {
			assertEquals(f1_2, Fraction.create("1/2"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_3, Fraction.create("/3"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_2, Fraction.create("/2"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_3, Fraction.create("3/9"));
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void createFromDecimalString() {
		try {
			assertEquals(f1_2, Fraction.create("0.5"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.create(new BigInteger("3"), new BigInteger("2")), Fraction.create("1.5"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_2, Fraction.create("0.50000"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_4, Fraction.create("0.25"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_2, Fraction.create(".5"));
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void createFromScientificNotation() {

		try {
			assertEquals(f1_2, Fraction.create("5E-1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.create(new BigInteger("3"), new BigInteger("2")), Fraction.create("15E-1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.create(new BigInteger("50"), BigInteger.ONE), Fraction.create("5E+1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.ONE, Fraction.create("1E-0"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.ONE, Fraction.create("1E+0"));
		} catch (final Exception e) {
			fail();
		}
	}
}
