package partslist;

public interface Component {
	/**
	 * Modifies the receiver by adding the component part quantity times as
	 * subpart
	 * 
	 * @throws Exception if a Cycle is getting created by adding the part.
	 */
	public abstract void addPart(Component part, int quantitiy) throws Exception;

	/**
	 * Returns true if the receiver directly or indirectly contains part.
	 */
	public abstract boolean contains(Component part);

	/**
	 * @return the attribute name from the receiver.
	 */
	public abstract String getName();

	/**
	 * @return the price for the Component receiver.
	 */
	public abstract int getOverallPrice();

	/**
	 * 
	 * @return the Material-List for the component receiver.
	 */
	public abstract MaterialList getMaterialList();

}
