package partslist;

public class QuantifiedComponent {

	private int quantity;
	private final Component component;

	public static QuantifiedComponent create(final int quantity, final Component component) {
		return new QuantifiedComponent(quantity, component);
	}

	/**
	 * Constructor filling the fields correctly with the given arguments.
	 */
	private QuantifiedComponent(final int quantity, final Component component) {
		this.quantity = quantity;
		this.component = component;
	}

	/**
	 * @return true if the receiver directly or indirectly contains part.
	 */
	public boolean contains(final Component part) {
		return this.component.contains(part);
	}

	public boolean isQuantificationFor(final Component component) {
		return this.component.equals(component);
	}

	/**
	 * Adds the quantity summand to the quantity of the receiver object.
	 */
	public void addQuantity(final int summand) {
		this.quantity = this.quantity + summand;

	}

	/**
	 * Adds the quantity of material to the quantity of the receiver object.
	 */
	public void addQuantityOf(QuantifiedComponent material) {
		this.quantity += material.quantity;

	}

	/**
	 * @return the overall price for the receiver product.
	 */
	public int getOverallPrice() {
		return this.quantity * this.component.getOverallPrice();
	}

	public String getName() {
		return this.component.getName();
	}

	/**
	 * Multiply the receiver quantity by the given factor.
	 */
	public void multiplyQuantity(int factor) {
		this.quantity = this.quantity * factor;
	}

	@Override
	/**
	 * Delegates equals to the component of the receiver.
	 */
	public boolean equals(Object obj) {
		if (obj instanceof QuantifiedComponent) {
			final QuantifiedComponent argument = (QuantifiedComponent) obj;
			return this.component.equals(argument.component);
		}
		return false;

	}

	@Override
	/**
	 * Returns a clone of the receiver object.
	 */
	public QuantifiedComponent clone() {
		return create(this.quantity, this.component);
	}

	/**
	 * 
	 * @return the Material-List needed to build the quantified Component
	 *         receiver.
	 */
	public MaterialList getMaterialList() {
		return this.component.getMaterialList().mul(this.quantity);
	}

	@Override
	public String toString() {
		return this.component.toString() + " " + this.quantity;
	}

	public int hashCode() {
		return this.quantity + this.component.hashCode();
	}
}
