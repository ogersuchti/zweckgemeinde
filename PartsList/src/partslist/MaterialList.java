package partslist;

import java.util.Iterator;
import java.util.Vector;

public class MaterialList {
	
	private final Vector<QuantifiedComponent> material;
	/**
	 * Create empty List
	 */
	public static MaterialList create() {
		return new MaterialList();
	}
	/**
	 * Create by using the given vector material to fill the field.
	 */
	public static MaterialList create(Vector<QuantifiedComponent> material) {
		return new MaterialList(material);
	}
	/**
	 * Creates Materialist with one entry. The entry is a Quantified componnent with the given Component and quantity. 
	 */
	public static MaterialList create(ComponentCommon material, int quantity) {
		final Vector<QuantifiedComponent> vector = new Vector<QuantifiedComponent>();
		vector.add(QuantifiedComponent.create(quantity,material));
		return new MaterialList(vector);
	}
	
	private MaterialList() {
		this.material = new Vector<QuantifiedComponent>();
	}
	
	private MaterialList(Vector<QuantifiedComponent> material) {
		this.material = material;
	}
	/**
	 * Returns the receiver object with all entries from summand added by using the method addMaterial.
	 */
	public MaterialList add(MaterialList summand) {
		final MaterialList newMaterialList = create(this.material);
		
		final Iterator<QuantifiedComponent> materialIterator = summand.material.iterator();
		
		while(materialIterator.hasNext()) {
			final QuantifiedComponent current = materialIterator.next();
			newMaterialList.addMaterial(current);
		}
		
		return newMaterialList;
		
	}
	/**
	 * Adds the given QuantifiedComponent material to the receiver list of QuantifiedComponent's. If the material is already part of the list.
	 * Just the quantitiy gets added up in the listed object. 
	 */
	public void addMaterial(QuantifiedComponent material) {
		
		final Iterator<QuantifiedComponent> materialIterator = this.material.iterator();
		
		while(materialIterator.hasNext()) {
			final QuantifiedComponent current = materialIterator.next();
			if(current.equals(material)) {
				current.addQuantityOf(material);
				return;
			}
		}
		final QuantifiedComponent newMaterial = material.clone();
		this.material.add(newMaterial);
		
	}
	/**
	 * Return the receiver list with each entry's quantitiy multiplicated by factor.
	 */
	public MaterialList mul(int factor) {
		final Iterator<QuantifiedComponent> materialIterator = this.material.iterator();
		
		while(materialIterator.hasNext()) {
			final QuantifiedComponent current = materialIterator.next();
			current.multiplyQuantity(factor);
			
		}
		return this; // evtl. in Zukunft neues Object
	}

	@Override
	public String toString() {

		final Iterator<QuantifiedComponent> materialIterator = this.material.iterator();
		String out = "";
		while(materialIterator.hasNext()) {
			final QuantifiedComponent current = materialIterator.next();
			out = out + current.toString() + "\n";
			
		}
		return out;
		
	}


	
	

}
