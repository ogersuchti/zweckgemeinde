package partslist;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Product extends ComponentCommon {

	private final Map<String, QuantifiedComponent> parts;
	private int lastOverallPrice;
	// hier State Pattern mit 4 Zust�nden notC, priceC, matC, bothC
	// 5 Klassen 1 abstracte jeweils 1 zu den Zust�nden bothC erbt priceC und matC
	// �berg�nge priceChanged, structureChanged

	private static final String CycleMessage = "Keine Zyklen erlaubt";

	private Product(final String name, final int price, final Map<String, QuantifiedComponent> parts) {
		super(name,price);
		this.parts = parts;
	}

	public static ComponentCommon create(final String name, final int price) {
		return new Product(name, price, new HashMap<String, QuantifiedComponent>());

	}

	@Override
	public void addPart(final Component part, final int quantity) throws Exception {
		if (part.contains(this))
			throw new Exception(CycleMessage);
		final QuantifiedComponent OldEntry = this.parts.get(part.getName());
		if (OldEntry == null) {
			this.parts.put(part.getName(), QuantifiedComponent.create(quantity, part));
		} else {
			OldEntry.addQuantity(quantity);
		}

	}

	@Override
	protected boolean ContainsDetails(final Component part) {
		final Iterator<QuantifiedComponent> iterator = this.parts.values().iterator();
		while (iterator.hasNext()) {
			final QuantifiedComponent current = iterator.next();
			if (current.contains(part))
				return true;
		}
		return false;
	}

	@Override
	public int getOverallPrice() {
		final Iterator<QuantifiedComponent> iterator = this.parts.values().iterator();
		int sum = 0; // neutrales Element da wir einen iterator zum aufaddieren nutzen
		while(iterator.hasNext()) {
			final QuantifiedComponent current  = iterator.next();
			 sum += current.getOverallPrice();
		}
		sum  = sum + this.price; // hier immer price da wir ein neutrales Element brauchen 
		this.lastOverallPrice = sum;
		return sum;
	}

	@Override
	public MaterialList getMaterialList() {
		final MaterialList out = MaterialList.create();
		final Iterator<QuantifiedComponent> iterator = this.parts.values().iterator();
		while(iterator.hasNext()) {
			final QuantifiedComponent current  = iterator.next();
			 out.add(current.getMaterialList());
		}
		
		return out;
	}
	

}
