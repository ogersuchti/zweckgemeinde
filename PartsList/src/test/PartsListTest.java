package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import partslist.ComponentCommon;
import partslist.Material;
import partslist.MaterialList;
import partslist.Product;
import partslist.QuantifiedComponent;

public class PartsListTest {

	public static QuantifiedComponent qm1;
	public static QuantifiedComponent qm2;
	public static QuantifiedComponent qm3;

	public static ComponentCommon p1;
	public static ComponentCommon p2;
	public static ComponentCommon p3;

	public static ComponentCommon m1;
	public static ComponentCommon m2;
	public static ComponentCommon m3;

	@Before
	public void setUp() throws Exception {
		m1 = Material.create("M1", 1);
		m2 = Material.create("M2", 2);
		m3 = Material.create("M3", 3);

		p1 = Product.create("Product 1", 5);
		p2 = Product.create("Product 2", 10);
		p3 = Product.create("Product 3", 1);

		qm1 = QuantifiedComponent.create(25, m1);
		qm2 = QuantifiedComponent.create(25, m2);
		qm3 = QuantifiedComponent.create(25, m3);

	}

	@Test
	public void addPartsTest() {

		try {
			p1.addPart(p1, 2);
			fail(); // Reflexive Cycle
		} catch (Exception e) {
		}
		try {
			p1.addPart(p2, 2);
			p2.addPart(p1, 2);
			fail(); // Direct Cycle
		} catch (Exception e) {
		}
		try {
			p2.addPart(m1, 2); // set up indirect Cycle
			p2.addPart(p3, 2);
		} catch (Exception e) {
			fail();
		}

		try {
			p3.addPart(p1, 2); // Indirect Cycle
			fail();
		} catch (Exception e) {
		}

	}

	@Test
	public void containsTest() {
		assertTrue(p1.contains(p1)); // Reflexive contains
		assertFalse(p1.contains(p2));
		try {
			p1.addPart(p2, 2);
			p2.addPart(p3, 2);
			p3.addPart(m1, 2);
		} catch (Exception e) {
			fail();
		}
		assertTrue(p1.contains(p2)); // Direct contains
		assertTrue(p1.contains(p3)); // Indirect contains
		assertTrue(p1.contains(m1)); // Indirect contains
		assertFalse(p1.contains(m2));

	}

	@Test
	public void testMaterialList() {
		try {
			p1.addPart(m1, 1);
			p1.addPart(p2, 2);
			p2.addPart(m2, 2);
			p2.addPart(m3, 3);
			System.out.println(p1.getMaterialList());
		} catch (final Exception e) {
			fail();
		}
	}

	public void testAdd() {
		final MaterialList m1 = MaterialList.create();
		m1.addMaterial(qm1);
		m1.addMaterial(qm2);
		m1.addMaterial(qm1);
		System.out.println(m1);
		final MaterialList m2 = MaterialList.create();
		m2.addMaterial(qm3);
		m2.addMaterial(qm1);
		System.out.println(m2.add(m1));
		System.out.println(m1.add(m2));
	}

	public void testAMul() {
		final MaterialList m1 = MaterialList.create();
		m1.addMaterial(qm1);
		m1.addMaterial(qm2);
		m1.addMaterial(qm1);
		System.out.println(m1);
		m1.mul(2);
		System.out.println(m1);
	}

}
