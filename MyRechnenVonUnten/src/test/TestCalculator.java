package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import model.Add;
import model.Constant;
import model.Divide;
import model.DividedByZeroException;
import model.Expression;
import model.Multiply;
import model.Subtraction;
import model.Variable;

public class TestCalculator {

	private static Expression E1;
	private static Expression E2;
	private static Expression E3;
	private static Expression E4;
	private static Variable X;
	private static Variable Y;
	private static Variable Z;
	private static Constant C10;
	private static Constant C5;
	
	// alter der nicht traktiert wurde

	@Before
	public void setUp() throws Exception {
		X = Variable.createVariable("X");
		X.setValue(new BigInteger("30"));
		Y = Variable.createVariable("Y");
		Y.setValue(new BigInteger("2"));
		Z = Variable.createVariable("Z");
		Z.setValue(new BigInteger("5"));
		C10 = Constant.create(BigInteger.TEN);
		C5 = Constant.create(new BigInteger("5"));
		E2 = Subtraction.create(X, Y);
		E4 = Divide.create(C10, C5);
		E3 = Multiply.create(E4, Z);
		E1 = Add.create(E2, E3);
	}

	@Test
	/**
	 * getValue Test by using the created Expression as seen in the pdf
	 * document. Then values are getting changed by 1, so Observer gets Tested.
	 */
	public void getValueTest() {
		assertEquals(new BigInteger("38"), E1.getValue());
		assertEquals(new BigInteger("28"), E2.getValue());
		assertEquals(new BigInteger("10"), E3.getValue());
		assertEquals(new BigInteger("2"), E4.getValue());

		// Cached data. Wie soll man das richtig Testen ? getState ?
		assertEquals(new BigInteger("38"), E1.getValue());
		assertEquals(new BigInteger("28"), E2.getValue());
		assertEquals(new BigInteger("10"), E3.getValue());
		assertEquals(new BigInteger("2"), E4.getValue());

		// Values changed : Y = 2 - 1 = 1
		Y.down();
		// --> E2 = 29 && E1 = 39
		assertEquals(new BigInteger("29"), E2.getValue());
		assertEquals(new BigInteger("39"), E1.getValue());
		// Change Z-- --> Z = 3
		Z.down();
		Z.down();
		// --> E3 = 6 && E1 = 35
		assertEquals(new BigInteger("6"), E3.getValue());
		assertEquals(new BigInteger("35"), E1.getValue());

		// Division by Zero has to throw a DividedByZeroException
		final Expression e = Divide.create(X, Constant.create(BigInteger.ZERO));
		try {

			e.computeValue();
			fail();
		} catch (final DividedByZeroException exception) {

		}

	}

	@Test
	/**
	 * 
	 */
	public void getNameTest() {
		assertEquals("10 / 5", E4.getName());
		assertEquals("10 / 5 * Z", E3.getName());
		assertEquals("(X - Y)", E2.getName());
		assertEquals("((X - Y) + 10 / 5 * Z)", E1.getName());
		assertEquals("X", X.getName());
		assertEquals("Y", Y.getName());
		assertEquals("Z", Z.getName());
		assertEquals("10", C10.getName());
		assertEquals("5", C5.getName());
	}

	@Test
	/**
	 * ToString gibt die Expression aus und in Klammern dahinter den Wert der
	 * Expression
	 */
	public void toStringTest() {
		assertEquals("10 / 5(2)", E4.toString());
		assertEquals("10 / 5 * Z(10)", E3.toString());
		assertEquals("(X - Y)(28)", E2.toString());
		assertEquals("((X - Y) + 10 / 5 * Z)(38)", E1.toString());
		assertEquals("X(30)", X.toString());
		assertEquals("Y(2)", Y.toString());
		assertEquals("Z(5)", Z.toString());
		assertEquals("10", C10.toString());
		assertEquals("5", C5.toString());
	}

	@Test
	/**
	 * Eigentlich nicht n�tig. da erst bei calc2
	 * Tests the contains method. Direct and indirect contains !
	 */
	public void testContains() {
		// True Tests :
		// Direct Expression
		assertTrue(E1.contains(E2));
		assertTrue(E1.contains(E3));
		assertTrue(E3.contains(E4));
		// Indirect Variable
		assertTrue(E1.contains(X));
		assertTrue(E1.contains(Y));
		// Indirect Expression
		assertTrue(E1.contains(E4));
		// Indirect Variables
		assertTrue(E1.contains(Z));
		// Indirect Constants
		assertTrue(E1.contains(C5));
		assertTrue(E1.contains(C10));

		// False Tests :
		// Expressions
		assertFalse(E2.contains(E1));
		assertFalse(E2.contains(E3));
		assertFalse(E2.contains(E4));
		assertFalse(E3.contains(E1));
		assertFalse(E3.contains(E2));
		// Constants
		assertFalse(E2.contains(C10));
		assertFalse(E2.contains(C5));
		// Variables
		assertFalse(E2.contains(Z));
		assertFalse(E3.contains(X));
		assertFalse(E4.contains(Y));

	}

}
