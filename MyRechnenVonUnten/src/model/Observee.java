package model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class Observee {

	protected Collection<Observer> currentObservers;

	/**
	 * Constructor initializes the Collection with an empty LinkedList.
	 */
	protected Observee() {
		this.currentObservers = new LinkedList<Observer>();
	}
	
	/**
	 * Adds the Observer o to the receiver field currentObservers.
	 */
	public void register(Observer o) {
		this.currentObservers.add(o);
	}
	
	/**
	 * Removes the Observer o from the receiver field currentObservers.
	 */
	public void deregister(Observer o) {
		this.currentObservers.remove(o);
	}

	/**
	 * Notifys all currentObservers using update on them.
	 */
	public void notifyObservers() {
		final Iterator<Observer> iterator = this.currentObservers.iterator();
		while (iterator.hasNext()) {
			final Observer current = iterator.next();
			current.update();
		}
	}
}
