package model;

import java.math.BigInteger;

public class Add extends Operator{

	private static final String Operator_Representation = " + ";
	
	/**
	 * @param first expression
	 * @param second expression
	 * @return a new Object representing the Expression second added to first.
	 */
	public static Add create(Expression first, Expression second) {
		return new Add(first, second);

	}

	private Add(Expression first, Expression second) {
		super(first, second);
	}
	
	@Override
	public BigInteger computeValue(){
		return this.first.computeValue().add( this.second.computeValue());
	}
	
	@Override
	public String getName() {
		return ValueOpenBracket + this.first.getName() + Operator_Representation + this.second.getName() + ValueCloseBracket;
	}

	@Override
	public BigInteger combine(BigInteger first, BigInteger second) {
		return first.add(second);
	}

}
