package model;

import java.math.BigInteger;

public class Subtraction extends Operator {

	// private static final int InitialValue = 0;
	private static final String Operator_Representation = " - ";

	public static Subtraction create(Expression first, Expression second) {
		return new Subtraction(first, second);

	}

	private Subtraction(Expression first, Expression second) {
		super(first, second);
	}

	@Override
	public String getName() {
		return ValueOpenBracket + this.first.getName() + Operator_Representation + this.second.getName() + ValueCloseBracket;
	}

	@Override
	public BigInteger computeValue() throws DividedByZeroException{
		return this.first.computeValue().subtract(this.second.computeValue());
	}

	@Override
	public BigInteger combine(BigInteger first, BigInteger second) {
		return first.subtract(second);
	}
}
