package variableSubstituteState;

import java.math.BigInteger;

import model.Expression;
import model.Variable;

public class Substitute implements VariableSubstituteState {

	private Expression mySubstitude;

	public static Substitute create(Expression mySubstitude) {
		return new Substitute(mySubstitude);
	}

	private Substitute(Expression mySubstitude) {
		super();
		this.mySubstitude = mySubstitude;
	}

	@Override
	public BigInteger getValue(Variable v) {
		return this.mySubstitude.getValue();
	}

	@Override
	public void newSubstitude(Variable v, Expression newSub) {
		this.mySubstitude = newSub;

	}

	@Override
	public void up(Variable v) {
		// IDC im substituted
		System.out.println("Im substituted");
	}

	@Override
	public void down(Variable v) {
		// IDC im substituted
		System.out.println("Im substituted");

	}

}
