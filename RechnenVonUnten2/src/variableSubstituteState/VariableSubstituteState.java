package variableSubstituteState;

import java.math.BigInteger;

import model.Expression;
import model.Variable;

public interface VariableSubstituteState {
	
	abstract BigInteger getValue(Variable v);
	
	abstract void newSubstitude(Variable v, Expression newSub);
	
	void up(Variable v);
	
	void down(Variable v);

}
