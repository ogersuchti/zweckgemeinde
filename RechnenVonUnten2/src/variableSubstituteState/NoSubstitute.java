package variableSubstituteState;

import java.math.BigInteger;

import model.Expression;
import model.Variable;

public class NoSubstitute implements VariableSubstituteState {

	private static NoSubstitute theNoSubstitude;

	/**
	 * Singleton
	 */
	public static NoSubstitute getInstance() {
		if (theNoSubstitude == null) {
			theNoSubstitude = new NoSubstitute();
		}
		return theNoSubstitude;
	}

	private NoSubstitute() {
	}

	@Override
	public BigInteger getValue(Variable v) {
		return v.computeValue();

	}

	@Override
	public void newSubstitude(Variable v, Expression newSub) {
		v.setVariableState(Substitute.create(newSub));

	}

	@Override
	public void up(Variable v) {
		v.reallyUp();
	}

	@Override
	public void down(Variable v) {
		v.reallyDown();
	}

}
