package model;

public interface Observer {
	
	/**
	 * Update the Observer.
	 */
	void update();

}
