package model;

import java.math.BigInteger;

public class Divide extends Operator {

	private static final String Operator_Representation = " / ";

	public static Divide create(Expression first, Expression second) {
		return new Divide(first, second);
	}

	private Divide(Expression first, Expression second) {
		super(first, second);
	}

	@Override
	public String getName() {
		return this.first.getName() + Operator_Representation + this.second.getName();
	}

	@Override
	public BigInteger combine(BigInteger first, BigInteger second) {
		if (second.equals(BigInteger.ZERO))
			throw new DividedByZeroException("Dividing by Zero is not possible !");
		return first.divide(second);
	}
}
