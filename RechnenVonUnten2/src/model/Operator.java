package model;

import java.math.BigInteger;

import operationState.NoValue;
import operationState.OperationState;

public abstract class Operator extends ExpressionCommon implements  Observer {

	protected final Expression first;
	protected final Expression second;
	protected OperationState state;

	protected static final String ValueOpenBracket = "(";
	protected static final String ValueCloseBracket = ")";
	protected static final String ValueEquals = " = ";
	
	/**
	 * Constructs the wanted Operator by using fields.
	 * State is initialized by NoValue.
	 * first and second register the receiver to their Collection of currentObservers. 
	 */
	protected Operator(Expression first, Expression second) {
		super();
		if(first.contains(this) || second.contains(this)) {
			throw new CycleRuntimeException();
		}
		this.first = first;
		this.second = second;
		this.state = NoValue.create();
		first.register(this);
		second.register(this);
	}
	
	/**
	 *	Setter for the state field. 
	 */
	public void setState(OperationState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return this.getName() + ValueOpenBracket + this.getValue() + ValueCloseBracket;
	}

	@Override
	public abstract String getName();
	
	@Override
	public BigInteger getValue() {
		return this.state.getValue(this);
	}
	
	@Override
	public BigInteger computeValue() {
		return this.combine(this.first.getValue(),this.second.getValue());
	}
	
	public abstract BigInteger combine(BigInteger first, BigInteger second);

	@Override
	/**
	 * Updates the state field about the changed value.
	 */
	public void update() {
		this.state.valueChanged(this);
		this.notifyObservers();
	}
	
	@Override
	public boolean contains(Expression expression) {
		return this.equals(expression) || this.first.contains(expression) || this.second.contains(expression);
	}

}
