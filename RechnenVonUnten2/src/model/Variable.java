package model;

import java.math.BigInteger;

import variableSubstituteState.NoSubstitute;
import variableSubstituteState.Substitute;
import variableSubstituteState.VariableSubstituteState;

public class Variable extends ExpressionCommon implements Observer {

	private static final BigInteger InitialVariableValue = BigInteger.ZERO;
	private static final String ValueOpenBracket = "(";
	private static final String ValueCloseBracket = ")";
	private static final BigInteger IncrementValue = BigInteger.ONE;

	public static Variable createVariable(final String name) {
		return new Variable(name, InitialVariableValue);
	}

	private final String name;
	private BigInteger value;
	private VariableSubstituteState variableState;

	private Variable(final String name, final BigInteger initialValue) {
		this.name = name;
		this.setValue(initialValue);
		this.setVariableState(NoSubstitute.getInstance());
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public boolean equals(Object argument) {
		return super.equals(argument);
	}

	@Override
	public String toString() {
		return this.getName() + ValueOpenBracket + this.getValue() + ValueCloseBracket;
	}

	public void up() {
		this.variableState.up(this);
	}

	public void reallyUp() {
		this.setValue(this.getValue().add(IncrementValue));
		this.notifyObservers();
	}

	public void down() {
		this.variableState.down(this);
	}

	public void reallyDown() {
		this.setValue(this.getValue().subtract(IncrementValue));
		this.notifyObservers();
	}

	@Override
	public BigInteger getValue() {
		return this.variableState.getValue(this);
	}

	public void setValue(final BigInteger newValue) {
		this.value = newValue;
	}

	@Override
	public BigInteger computeValue() {
		return this.value;
	}

	@Override
	public boolean contains(Expression expression) {
		return this.equals(expression);
	}

	public void setVariableState(Substitute newVariableState) {
		this.variableState = newVariableState;
	}

	public void setVariableState(VariableSubstituteState variableState) {
		this.variableState = variableState;
	}

	public void substitute(Expression expression) {
		if (expression.contains(this))
			throw new CycleRuntimeException();
		expression.register(this); // Variable beobachtet seine Substitution
		this.notifyObservers();
		this.variableState.newSubstitude(this, expression);
	}

	@Override
	public void update() {
		this.notifyObservers();
	}
}
