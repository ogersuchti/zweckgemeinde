package model;

import java.math.BigInteger;

public interface Expression {

	/**
	 * @return the computed Value for the receiver expression.
	 */
	public abstract BigInteger computeValue();

	/**
	 * @return the Value for the receiver expression.
	 */
	public abstract BigInteger getValue();

	/**
	 * @return the String which represents the Expression as Arithmetik
	 *         Expression.
	 */
	public abstract String getName();

	/**
	 * Abstract Observee Method. Adds the Observer o in the Collection of
	 * Observers given in the subclasses.
	 */
	public abstract void register(Observer o);

	/**
	 * Abstract Observee Method. Removes the Observer o in the Collection of
	 * Observers given in the subclasses.
	 */
	public abstract void deregister(Observer o);

	/**
	 * Abstract Observee Method. All Observers in the Collection will get updated about the changement.
	 */
	public abstract void notifyObservers();
	
	/**
	 * ist eig unn�tig brauchen wir erst bei calc2
	 */
	public abstract boolean contains(Expression expression);

}
