package model;

import java.math.BigInteger;

public class Multiply extends Operator {
	
	
	private static final String Operator_Representation = " * ";

	public static Multiply create(Expression first, Expression second) {
		return new Multiply(first, second);

	}

	private Multiply(Expression first, Expression second) {
		super(first, second);
	}
	
	@Override
	public String getName() {
		return  this.first.getName() + Operator_Representation + this.second.getName() ;
	}

	@Override
	public BigInteger combine(BigInteger first, BigInteger second) {
		return first.multiply(second);
	}

}
