package operationState;

import java.math.BigInteger;

import model.Operator;

public class Value implements OperationState{
	
	private final BigInteger cachedValue;
	
	public static Value create(BigInteger cachedValue) {
		return new Value(cachedValue);
	}

	private Value(BigInteger cachedValue) {
		this.cachedValue = cachedValue;
	}
	
	@Override
	public BigInteger getValue(Operator operator) {
		return this.cachedValue;
	}

	@Override
	public void valueChanged(Operator operator) {
		operator.setState(NoValue.create());
	}

}
