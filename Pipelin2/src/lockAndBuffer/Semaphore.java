package lockAndBuffer;

public class Semaphore implements AbstractSemaphore{

	private int counter;
	private Lock mutex = new Lock(false);
	private Lock waiting = new Lock(true); // Wieso true ?
	
	/**Creates a semaphore with the internal counter set to the value
	 * of the parameter.
	 */
	public Semaphore(int count) {
		
	}
	@Override
	public void down() {
		this.mutex.lock();
		while(this.counter == 0) {
			this.mutex.unlock();
			this.waiting.lock();
			this.mutex.lock();
		}
		this.counter --;
		this.mutex.unlock();
	}

	@Override
	public void up() {
		this.mutex.lock();
		this.counter++;
		this.waiting.unlock();
		this.mutex.unlock();
	}

}
