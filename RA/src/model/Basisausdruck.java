package model;

import util.Automat;

public class Basisausdruck extends RA{
	
	private final char c;
	
	public static Basisausdruck create(final char c) {
		return new Basisausdruck(c);
	}
	
	private Basisausdruck(final char c) {
		this.c = c;
	}

	@Override
	public boolean contains(final RA regEx) {
		return this.equals(regEx);
	}

	@Override
	public void addExpression(final RA regEx) throws Exception {
		throw new NoSubstructureException();
	}

	@Override
	public Automat toAutomaton() {
		throw new UnsupportedOperationException();
	}

}
