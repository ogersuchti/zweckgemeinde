package model;

import util.Automat;

public class Auswahl extends Composite{
	
	public static Auswahl create() {
		return new Auswahl();
	}
	
	private Auswahl() {
		super();
	}

	@Override
	public Automat toAutomaton() {
		throw new UnsupportedOperationException();
	}

}
