package model;

import util.Automat;

public abstract class RA {
	
	private boolean iterated;
	
	public abstract boolean contains(final RA regEx);
	
	public abstract void addExpression(RA regEx) throws Exception;

	public abstract Automat toAutomaton();

	public void setIterated(boolean b) {
		this.iterated = b;
		
	}
	
	public boolean getIterated() {
		return this.iterated;
	}

}
