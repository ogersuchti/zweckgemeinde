package util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class Configuration {

	private final String input;
	private final Automat automat;
	private final Collection<State> currentStates;
	
	public static Configuration create(String input, Automat automat, Collection<State> currentstates) {
		return new Configuration(input, automat, currentstates);
	}
	
	public static Configuration create(String input, Automat automat) {
		return new Configuration(input, automat);
	}
	
	private Configuration(String input, Automat automat, Collection<State> currentStates) {
		this.input = input;
		this.automat = automat;
		this.currentStates = currentStates;
	}
	
	private Configuration(String input, Automat automat) {
		this.input = input;
		this.automat = automat;
		this.currentStates = new HashSet<State>();
	}

	public Configuration step() {
		
		if(this.input.isEmpty()) {
			return this;
		}
		else {
			final char firstDigit = this.input.charAt(0);
			final HashSet<State> newStates = new HashSet<State>();
			final Iterator<State> iterator = this.currentStates.iterator();
			while(iterator.hasNext()) {
				final State current = iterator.next();
				newStates.addAll(this.automat.nextStates(firstDigit, current));
			}
			return create(this.input.substring(1), this.automat, newStates);
		}

	}

	public Configuration run() {
		return this.input.isEmpty() ? this : this.step().run();
	}

	public Collection<State> getCurrentStates() {
		return this.currentStates ;
	}
	
	@Override
	public boolean equals(Object arg) {
		if(arg instanceof Configuration) {
			final Configuration argAsConfig = (Configuration) arg;
			return argAsConfig.input.equals(this.input) && this.currentStates.equals(argAsConfig.currentStates) && this.automat.equals(argAsConfig.automat);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "input: " + this.input + this.currentStates.toString();
	}

}
