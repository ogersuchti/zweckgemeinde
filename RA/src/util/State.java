package util;

public class State {

	private final Automat automat;

	public static State create(Automat automat) {
		return new State(automat);
	}

	private State(Automat automat) {
		this.automat = automat;
	}

	public void addTransition(char c, State after) {
		this.automat.addTransition(Transition.create(c, this, after));
		this.automat.addState(this);
		this.automat.addState(after);
	}

}
