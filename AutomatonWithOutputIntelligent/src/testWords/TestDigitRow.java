package testWords;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import inputRepresentation.EmptyWord;
import inputRepresentation.NoNextDigitException;
import inputRepresentation.Word;

public class TestDigitRow {

	@Test
	public void testABC() throws NoNextDigitException {
		String randomString = "abc";
		Word abc = Word.makeDigitRowFromString(randomString);
		assertEquals('a',abc.getCurrentDigit());
		abc = abc.nextDigits();
		assertEquals('b',abc.getCurrentDigit());
		abc = abc.nextDigits();
		assertEquals('c',abc.getCurrentDigit());
		try {
			abc = abc.nextDigits();
			abc.getCurrentDigit();
			fail();
		} catch (NoNextDigitException e) {
		}
	}
	@Test
	public void testEmpty() {
		String randomString = "";
		Word abc = Word.makeDigitRowFromString(randomString);
		try {
			abc.getCurrentDigit();
			fail();
		} catch (NoNextDigitException e) {
		}
		Word emptyWord = new EmptyWord();
		try {
			emptyWord.addNextDigits(abc);
			fail();
		} catch (NoNextDigitException e) {
		}
		try {
			emptyWord.getCurrentDigit();
			fail();
		} catch (NoNextDigitException e) {
		}
	}
	

}
