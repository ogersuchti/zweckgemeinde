package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import automaton.Automat;
import automaton.NotRecognizedWordException;
import automaton.State;
import automaton.Transition;

/**
 * Testet die Implementierung endlicher Automaten mit Ausgabe anhand des
 * Automaten aus testResources/TestAutomat1.pdf
 */
public class TestAutomaton3 {

	private Automat testAutomat3;

	@Before
	/**
	 * Baut den Automaten für das Testen auf. Siehe
	 * testResources/TestAutomat3.pdf für das Transitionsdiagramm des Automaten.
	 */
	public void setUp() {
		this.testAutomat3 = Automat.create();
		State z0 = this.testAutomat3.getStartState();
		State z1 = this.testAutomat3.createState();
		State z2 = this.testAutomat3.createState();
		State z3 = this.testAutomat3.createState();
		State zE = this.testAutomat3.getEndState();
		this.testAutomat3.addTransition(Transition.create('a', 'y', z0, z1));
		this.testAutomat3.addTransition(Transition.create('b', 'z', z1, z2));
		this.testAutomat3.addTransition(Transition.create('c', 'x', z2, z3));
		this.testAutomat3.addTransition(Transition.create('b', 'y', z3, zE));
		this.testAutomat3.addTransition(Transition.create('c', 'z', z3, z1));
	}

	@Test
	/**
	 * Die Sprache des gegebenen Automaten entspricht dem regulären Ausdruck 		abc(cbc)*b.
	 * Die Ausgabesprache des gegebenen Automaten enspricht dem regulären Ausdruck	yzx(zzx)*y.
	 * Einige Wörter dieser Sprache werden zum Testen herangezoogen.
	 */
	public void testRecognizesTrue() throws NotRecognizedWordException {
		// Iteration x0
		assertEquals("yzxy", this.testAutomat3.recognizes("abcb"));
		// Iteration x1
		assertEquals("yzxzzxy", this.testAutomat3.recognizes("abccbcb"));
		// Iteration x2
		assertEquals("yzxzzxzzxy", this.testAutomat3.recognizes("abccbccbcb"));
		// Iteration x3
		assertEquals("yzxzzxzzxzzxy", this.testAutomat3.recognizes("abccbccbccbcb"));
		// Iteration x4
		assertEquals("yzxzzxzzxzzxzzxy", this.testAutomat3.recognizes("abccbccbccbccbcb"));
		// Iteration x5
		assertEquals("yzxzzxzzxzzxzzxzzxy", this.testAutomat3.recognizes("abccbccbccbccbccbcb"));
		// Iteration x6
		assertEquals("yzxzzxzzxzzxzzxzzxzzxy", this.testAutomat3.recognizes("abccbccbccbccbccbccbcb"));
	}

	@Test
	/**
	 * Der gegebene Automat aus testResources/Testautomat3.pdf soll die hier
	 * angegebenen Wörter nicht erkennen. Um dies zu testen wird eine
	 * entsprechende Methode aufgerufen.
	 */
	public void testRecognizesFalse() {
		this.testNotRecognizedInput("");
		this.testNotRecognizedInput("abc");
		this.testNotRecognizedInput("abcc");
		this.testNotRecognizedInput("a");
		this.testNotRecognizedInput("1234356789");
		this.testNotRecognizedInput("aaaaaaaaaaaaaaac");
		this.testNotRecognizedInput("aabaaaaaaaaaaaaa");
		this.testNotRecognizedInput("b");
		this.testNotRecognizedInput("casasd");
		this.testNotRecognizedInput("abcba");
		this.testNotRecognizedInput("abcb?");
	}

	/**
	 * * Der gegebene Automat aus testResources/Testautomat3.pdf soll die hier
	 * angegebenen Wörter nicht erkennen. D.h. eine
	 * {@link NotRecognizedWordException} wird erwartet. Tritt diese Ausnahme
	 * nicht auf, ist der Testfall fehlgeschlagen.
	 */
	private void testNotRecognizedInput(String input) {
		try {
			this.testAutomat3.recognizes(input);
			fail();
		} catch (NotRecognizedWordException e) {
			// can be empty --> we want an Exception to occure.
		}
	}
}
