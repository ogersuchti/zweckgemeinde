package automaton;

/**
 * 
 */
public class State {

	private Automat automat;

	public static State create(Automat automat) {
		return new State(automat);
	}

	private State(Automat automat) {
		this.automat = automat;
	}

	public void addTransition(char input,char output, State after) {
		this.automat.addTransition(Transition.create(input,output, this, after));
		this.automat.addState(this);
		this.automat.addState(after);
	}

	@Override
	public boolean equals(Object arg) {
		if (arg instanceof State) {
			final State argAsState = (State) arg;
			return this == argAsState;
		}
		return false;
	}
}
