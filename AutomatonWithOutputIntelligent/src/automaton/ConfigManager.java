package automaton;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import lock.Lock;

/**
 * Klasse zur Verwaltung der laufenden Configurationen eines Automaten. Merkt
 * sich alle aktuell laufenden Configurationen durch hinzufügen / löschen aus
 * der Menge <currentRunningConfigs>
 * 
 */
public class ConfigManager {

	private final Collection<Configuration> currentRunningConfigs;
	private String output;
	private Lock recognizedLock;
	private boolean recognized;

	public ConfigManager() {
		this.currentRunningConfigs = new LinkedList<>();
		this.recognized = false;
		this.recognizedLock = new Lock(true);
	}

	/**
	 * Lässt initial die Configuration startConfiguration des Automaten laufen.
	 * Gibt die Ausgabe zur zugehörigen Eingabe der startConfiguration zurück.
	 * Falls keine laufende Configuration Erfolg meldet und alle Misserfolg
	 * melden, wird eine {@link NotRecognizedWordException} geworfen.
	 */
	public String runConfigurations(Configuration startConfiguration) throws NotRecognizedWordException {
		this.currentRunningConfigs.add(startConfiguration);
		startConfiguration.run();
		this.recognizedLock.lock();
		if (!this.recognized)
			throw new NotRecognizedWordException();
		return this.output;

	}

	/**
	 * Meldet das erfolgreiche Erkennen des Eingabewortes durch eine
	 * Configuration. Dabei wird die zugehörige Ausgabe des Automaten bei
	 * Einlesen des Eingabewortes übergeben. Zusätzlich werden alle noch
	 * laufenden Configurationen gestoppt. Und die Methode runConfigurations
	 * wird benachrichtigt.
	 */
	public synchronized void success(String output) {
		this.output = output;
		this.recognized = true;
		Iterator<Configuration> it = this.currentRunningConfigs.iterator();
		while (it.hasNext()) {
			Configuration current = it.next();
			current.stoppThread();
		}
		this.recognizedLock.unlock();
	}

	/**
	 * Lässt die übergebene Configuration nextConfig laufen. Merkt sich die
	 * nextConfig in der Menge der laufenden Configuartionen.
	 */
	public synchronized void runNewConfig(Configuration nextConfig) {
		this.currentRunningConfigs.add(nextConfig);
		nextConfig.run();
	}

	/**
	 * Meldet dem ConfigurationsManager eine nicht erfolgreichen oder
	 * unterbrochenen Configurationslauf. Diese Configuartion wird dann aus der
	 * Menge der laufenden Configurationen entfernt. Falls alle laufenden
	 * Configurationen aus der Menge der Configurationen entfernt wurden, wird
	 * das recognized Lock geöffnet. Daraufhin kann die Methode runConfiguration
	 * terminieren.
	 */
	public synchronized void noSuccess(Configuration terminated) {
		this.currentRunningConfigs.remove(terminated);
		if (this.currentRunningConfigs.isEmpty()) {
			this.recognizedLock.unlock();
		}
	}

}
