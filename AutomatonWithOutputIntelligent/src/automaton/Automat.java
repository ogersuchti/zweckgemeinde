package automaton;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import inputRepresentation.Word;

/**
 * Repräsentation des mathematischen Modells von endlichen Automaten mit
 * Ausgabe. <br>
 * Hat <br>
 * genau einen Startzustand <startState> <br>
 * genau einen Endzustand <endState> <br>
 * eine Menge von Zuständen <states> <br>
 * eine Menge von Transitionen <transitions> <br>
 * 
 * Die Methode recognizes kann bestimmen ob ein gegebenes Wort zur Sprache des
 * Automaten gehört. <br>
 * 
 * Ein Automat lässt sich über die Methoden addTransition und createState
 * aufbauen.
 * 
 */
public class Automat {

	private State startState;
	private State endState;
	private final Collection<State> states;
	private final Collection<Transition> transitions;

	public State getEndState() {
		return this.endState;
	}

	public State getStartState() {
		return this.startState;
	}

	public Collection<Transition> getTransitions() {
		return this.transitions;
	}

	/**
	 * Erstellt einen Zustand zugehörig zum aktuellen Automaten.
	 * 
	 * @return der erstellte Zustand.
	 */
	public State createState() {
		final State state = State.create(this);
		this.addState(state);
		return state;
	}

	/**
	 * 
	 * @return
	 * @throws NotRecognizedWordException
	 */
	public String recognizes(String input) throws NotRecognizedWordException {
		ConfigManager manager = new ConfigManager();
		Configuration startConfig = Configuration.create(Word.makeDigitRowFromString(input), "", this, this.startState, manager);
		return manager.runConfigurations(startConfig);
	}

	/**
	 * Adds the given state to the Field <states> which is a Collection<State>.
	 */
	public void addState(State state) {
		if (!this.states.contains(state)) {

			this.states.add(state);
		}
	}

	/**
	 * Adds the given transition to the Field <delta> which is a Collection
	 * <transition>.
	 */
	public void addTransition(Transition transition) {
		this.transitions.add(transition);
	}

	/**
	 * Gibt den Nullautomaten zur�ck der keine W�rter erkennt.
	 */
	public static Automat create() {
		return new Automat();
	}

	/**
	 * Erstellt einen neuen Automaten, wobei Anfangs- und Endzustand verschieden
	 * sind und somit festgelegt werden.
	 */
	private Automat() {
		this.startState = State.create(this);
		this.endState = State.create(this);
		this.states = new HashSet<State>();
		this.transitions = new HashSet<Transition>();
	}

	/**
	 * Gibt die Menge aller Transitionen zurück, welche den übergebenen Zustand
	 * State als Vorzustand before haben, sowie den übergebenen Charakter input
	 * als Eingabebuchstaben haben.
	 */
	public Collection<Transition> getValidTransitions(State currentState, char input) {
		Collection<Transition> result = new LinkedList<>();
		Iterator<Transition> iterator = this.transitions.iterator();
		while (iterator.hasNext()) {
			Transition current = iterator.next();
			if (current.getBefore().equals(currentState) && current.getInput() == input) {
				result.add(current);
			}
		}
		return result;
	}

}
