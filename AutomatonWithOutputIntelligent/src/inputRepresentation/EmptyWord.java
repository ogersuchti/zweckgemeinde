package inputRepresentation;

/**
 * Diese Klasse repräsentiert das leere Wort.
 * D.h. sie markiert das Ende eines Wortes.
 *
 */
public class EmptyWord implements Word{

	@Override
	public Word nextDigits() throws NoNextDigitException {
		throw new NoNextDigitException();
	}

	@Override
	public boolean isEmptyWord() {
		return true;
	}

	@Override
	public void addNextDigits(Word nextDigit) throws NoNextDigitException {
		throw new NoNextDigitException();
	}

	@Override
	public char getCurrentDigit() throws NoNextDigitException {
		throw new NoNextDigitException();
	}


}
