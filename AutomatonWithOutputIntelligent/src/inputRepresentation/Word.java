package inputRepresentation;

public interface Word {

	/**
	 * Gibt den aktuellen Buchstaben der im Zugriff ist zurück
	 */
	public abstract char getCurrentDigit() throws NoNextDigitException;

	/**
	 * Gibt das nächste Wort-Objekt zurück, falls es dieses gibt. Ein leeres
	 * Wort hat keinen nächstes Wort-Objekt.
	 */
	public abstract Word nextDigits() throws NoNextDigitException;

	/**
	 * Gibt true zurück wenn this das leere Wort ist. Gibt false zurück wenn
	 * this nicht das leere Wort ist.
	 */
	public abstract boolean isEmptyWord();

	/**
	 * Fügt dem aktuellen Wort Objekt ein Folgewortobjekt hinzu, sodass
	 * nextDigits dieses in Zukunft zurückgeben kann. Wenn this das leere Wort
	 * ist tritt die Aufnahme {@link NoNextDigitException} auf.
	 */
	public abstract void addNextDigits(Word nextDigit) throws NoNextDigitException;

	/**
	 * Statische Methode die als Eingabe einen String bekommt und diesen in eine
	 * äquivalentes Objekt vom Typ Word umformt und dieses zurückgibt.
	 */
	public static Word makeDigitRowFromString(String input) {
		if (input.isEmpty()) {
			return new EmptyWord();
		}
		char firstDigit = input.charAt(0);
		input = input.substring(1);
		Word result = new DigitSequence(firstDigit);
		Word nextDigits = result;
		try {

			while (!input.isEmpty()) {
				char nextDigit = input.charAt(0);
				input = input.substring(1);
				nextDigits.addNextDigits(new DigitSequence(nextDigit));
				nextDigits = nextDigits.nextDigits();
			}
			nextDigits.addNextDigits(new EmptyWord());
		} catch (NoNextDigitException e) {
			throw new Error("Should never occure");
		}
		return result;
	}

}
