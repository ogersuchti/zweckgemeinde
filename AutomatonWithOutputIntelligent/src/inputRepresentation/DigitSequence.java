package inputRepresentation;

public class DigitSequence implements Word {

	/**
	 * Der nächste Buchstabe in der Kette von Buchstaben. Kann auch das leere
	 * Wort sein, sodass die Kette zu einem Ende kommt.
	 */
	private Word nextDigits;
	
	/**
	 * Der aktuelle Buchstabe zugehörig zu this
	 */
	private final char digit;

	public DigitSequence(char digit) {
		this.digit = digit;
	}

	@Override
	public Word nextDigits() throws NoNextDigitException {
		return this.nextDigits;
	}

	@Override
	public boolean isEmptyWord() {
		return false;
	}

	@Override
	public void addNextDigits(Word nextDigit) throws NoNextDigitException {
		this.nextDigits = nextDigit;
	}

	@Override
	public char getCurrentDigit() {
		return this.digit;
	}

}
