package model;

import view.ConcreteObserverView;

public class ConcreteObserverParallel extends ConcreteObserver{
	
	public static ConcreteObserver create(ConcreteObserverView view) {
		return new ConcreteObserverParallel(view);
	}
	private Thread myUpdateThread;
	private Buffer buffer;
	
	private ConcreteObserverParallel(ConcreteObserverViewer view) {
		super(view);
		this.buffer = new Buffer();
		this.myUpdateThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					ConcreteObserverParallel.this.doTheUpdate(buffer.get());
				}
			}});
		this.myUpdateThread.start();
	}
	@Override
	public void update(int value) {
		this.buffer.put(value);
	}


}










