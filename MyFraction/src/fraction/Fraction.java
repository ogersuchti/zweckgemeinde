package fraction;

import java.math.BigInteger;

public class Fraction implements Comparable<Fraction> {

	final private BigInteger enumerator;
	final private BigInteger denominator;

	public static final Fraction ZERO = new Fraction(BigInteger.ZERO, BigInteger.ONE);
	public static final Fraction ONE = new Fraction(BigInteger.ONE, BigInteger.ONE);

	private static final BigInteger MINUS_ONE = new BigInteger("-1");

	private static final String SEPARATOR_FRACTION = "/";
	private static final String SEPARATOR_DECIMAL = ".";
	private static final String SEPARATOR_SCIENCE = "E";
	private static final int SEPARATOR_FRACTION_LENGTH = SEPARATOR_FRACTION.length();
	private static final int SEPARATOR_DECIMAL_LENGTH = SEPARATOR_DECIMAL.length();
	private static final int SEPARATOR_SCIENCE_LENGTH = SEPARATOR_SCIENCE.length();
	private static final char MINUS_CHAR = '-';

	/**
	 * Constructor using the provided input to fill all fields correctly.
	 */
	private Fraction(final BigInteger enumerator, final BigInteger denominator) {
		this.enumerator = enumerator;
		this.denominator = denominator;
	}

	/**
	 * Returns the fraction representing the value of the fractionString.
	 */
	public static Fraction create(String fractionString) throws FractionConstructionException {
		final Fraction f = getFractionFromString(fractionString);
		return f;
	}

	/**
	 * 
	 */
	private static Fraction getFractionFromString(String fractionString) throws FractionConstructionException {
		if (fractionString.startsWith(SEPARATOR_FRACTION)) {
			return createFromJustSeparator(fractionString);
		} else {
			if (fractionString.contains(SEPARATOR_DECIMAL)) {
				return createFromDecimal(fractionString);
			} else {
				if (fractionString.contains(SEPARATOR_SCIENCE)) {
					return createFromScience(fractionString);

				} else {
					if (fractionString.contains(SEPARATOR_FRACTION)) {
						return createFromSlashSeparated(fractionString);
					} else {
						return createFromJustNumber(fractionString);
					}
				}
			}
		}
	}

	/**
	 * Returns a fraction in normalized form, i. e. enumerator and denominator
	 * have been divided by its greatest common divisor .
	 * 
	 * @param enumerator
	 *            is the intended enumerator of this result.
	 * @param denominator
	 *            is the intended denominator of this result.
	 * @return the normalized fraction equal to enumerator / denominator.
	 * @throws Exception
	 *             FractionConstructionException if the given denominator is
	 *             equal to zero.
	 */
	public static Fraction create(BigInteger enumerator, BigInteger denominator) throws FractionConstructionException {
		if (denominator.equals(BigInteger.ZERO))
			throw new FractionConstructionException();
		return new Fraction(enumerator, denominator).normalize();
	}

	/**
	 * Overrides equals from the supertype object. Returns fraction if object is
	 * not a fraction. Then uses equals from BigInteger to compare the receiver
	 * enumerator and the object enumerator after finding a common denominator.
	 */
	@Override
	public boolean equals(final Object object) {
		if (object instanceof Fraction) {
			final Fraction argument = (Fraction) object;
			return this.enumerator.multiply(argument.denominator)
					.equals(argument.enumerator.multiply(this.denominator));
		} else {
			return false;
		}
	}

	/**
	 * 
	 * If enumerator Xor denominator is negative the returned fraction always
	 * has a negative enumerator. Denominator and enumerator get cancelled by
	 * their greatest common divisor.
	 * 
	 * @return a new normalized fraction by the listed criteria.
	 */
	private Fraction normalize() {

		// TODO hier reicht eine ggt Teilung vorher die dann gespeichert wird
		final BigInteger gcd = this.denominator.gcd(this.enumerator);
		if (this.denominator.compareTo(BigInteger.ZERO) < 0) {
			return new Fraction(this.enumerator.divide(gcd).multiply(MINUS_ONE),
					this.denominator.divide(gcd).multiply(MINUS_ONE));
		}
		return new Fraction(this.enumerator.divide(gcd), this.denominator.divide(gcd));
	}

	@Override
	/**
	 * Overrides hashCode from the object super class. Fraction is normalized
	 * and the hasCode from enumerator (hashCode from BigInteger) is returned.
	 */
	public int hashCode() {
		return this.normalize().enumerator.hashCode();
	}

	@Override
	/**
	 * @return The String representing the value of the receiver object.
	 */
	public String toString() {
		return this.enumerator + "/" + this.denominator;
	}

	/**
	 * Return the fraction representing the sum of receiver and summand.
	 * 
	 * @param summand
	 * @return the sum of receiver and summand.
	 */
	public Fraction add(Fraction summand) {

		try {
			return create(enumerator.multiply(summand.denominator).add(summand.enumerator.multiply(this.denominator)),
					this.denominator.multiply(summand.denominator));
		} catch (final Exception e) {
			throw new Error(e);
		}
	}

	/**
	 * Returns the fraction representing the receiver subtracted by minuend.
	 * 
	 * @param minuend
	 * @return the receiver subtracted by minuend.
	 */
	public Fraction subtract(Fraction minuend) {

		try {
			return create(
					enumerator.multiply(minuend.denominator).subtract(minuend.enumerator.multiply(this.denominator)),
					this.denominator.multiply(minuend.denominator));
		} catch (final Exception e) {
			throw new Error(e);
		}
	}

	/**
	 * 
	 * @param factor
	 *            for the returned product.
	 * @return the product from the receiver and factor.
	 */
	public Fraction multiply(Fraction factor) {

		try {
			return create(this.enumerator.multiply(factor.enumerator), this.denominator.multiply(factor.denominator));
		} catch (final Exception e) {
			throw new Error(e);
		}
	}

	/**
	 * Returns the quotient of the receiver (as dividend) and the argument
	 * divisor.
	 * 
	 * @param divisor
	 * @return Quotient of receiver and divisor.
	 */
	public Fraction divide(Fraction divisor) throws FractionConstructionException {
		return this.multiply(divisor.inverseFraction());
	}

	/**
	 * 
	 * @return a new fraction with swapped enumerator and denominator from the
	 *         receiver.
	 * @throws FractionConstructionException
	 */
	private Fraction inverseFraction() throws FractionConstructionException {
		return create(this.denominator, this.enumerator);
	}

	/**
	 * Returns true if the receiver is less or equals to other else returns
	 * false;
	 * 
	 * @param other
	 *            : The other Fraction the receiver is compared to with lessEq.
	 * @return boolean
	 */
	@Override
	public int compareTo(Fraction other) {
		final BigInteger thisZaehler = this.enumerator.multiply(other.denominator);
		final BigInteger otherZaehler = this.denominator.multiply(other.enumerator);
		return thisZaehler.compareTo(otherZaehler);

	}

	public boolean lessEq(Fraction other) {
		return this.compareTo(other) <= 0;
	}

	// -----
	/**
	 * @param fractionString
	 * @return The Fraction representing fractionString.
	 * @throws FractionConstructionException
	 */
	private static Fraction createFromSlashSeparated(String fractionString) throws FractionConstructionException {
		final int positionTrennzeichen = fractionString.indexOf(SEPARATOR_FRACTION);
		try {
			final String enumeratorString = fractionString.substring(0, positionTrennzeichen);
			final String denominatorString = fractionString.substring(positionTrennzeichen + SEPARATOR_FRACTION_LENGTH,
					fractionString.length());
			return create(new BigInteger(enumeratorString), new BigInteger(denominatorString));
		} catch (final IndexOutOfBoundsException | NumberFormatException e) {
			throw new FractionConstructionException(e);
		}
	}

	/**
	 * @param fractionString
	 * @return The Fraction representing fractionString.
	 * @throws FractionConstructionException
	 */
	private static Fraction createFromJustSeparator(String fractionString) throws FractionConstructionException {
		final BigInteger newDenom = new BigInteger(fractionString.substring(1));
		return create(BigInteger.ONE, newDenom);

	}

	/**
	 * @param fractionString
	 * @return The Fraction representing fractionString.
	 * @throws FractionConstructionException
	 */
	private static Fraction createFromDecimal(String fractionString) throws FractionConstructionException {
		final int positionTrennzeichen = fractionString.indexOf(SEPARATOR_DECIMAL);
		final String beforePoint = fractionString.substring(0, positionTrennzeichen);
		final String afterPoint = fractionString.substring(positionTrennzeichen + SEPARATOR_DECIMAL_LENGTH,
				fractionString.length());
		final BigInteger newEnumerator = new BigInteger(beforePoint + afterPoint);
		final BigInteger newDenominator = BigInteger.TEN.pow(afterPoint.length());

		return create(newEnumerator, newDenominator);

	}

	/**
	 * @param fractionString
	 * @return The Fraction representing fractionString.
	 * @throws FractionConstructionException
	 */
	private static Fraction createFromScience(String fractionString) throws FractionConstructionException {
		final int positionTrennzeichen = fractionString.indexOf(SEPARATOR_SCIENCE);
		final String beforeE = fractionString.substring(0, positionTrennzeichen);
		final String exponent = fractionString.substring(positionTrennzeichen + 2);
		final BigInteger newEnum;
		final BigInteger newDenom;
		if (fractionString.charAt(positionTrennzeichen + SEPARATOR_SCIENCE_LENGTH) == MINUS_CHAR) {
			newEnum = new BigInteger(beforeE);
			newDenom = BigInteger.TEN.pow(new BigInteger(exponent).intValue());
		} else {
			newEnum = new BigInteger(beforeE).multiply(BigInteger.TEN.pow(new BigInteger(exponent).intValue()));
			newDenom = BigInteger.ONE;
		}
		return create(newEnum, newDenom);
	}

	/**
	 * @param fractionString
	 * @return The Fraction representing fractionString.
	 * @throws FractionConstructionException
	 */
	private static Fraction createFromJustNumber(String fractionString) throws FractionConstructionException {
		return create(new BigInteger(fractionString), BigInteger.ONE);

	}

	/**
	 * Ausnahmefälle abhacken
	 */
	public Fraction power(BigInteger exponent) throws FractionConstructionException {
		if (this.equals(Fraction.ZERO) && exponent.equals(BigInteger.ZERO))
			throw new FractionConstructionException();
		if (this.equals(Fraction.ZERO))
			return Fraction.ZERO;
		if (exponent.compareTo(BigInteger.ZERO) < 0)
			return this.inverseFraction().computePower(exponent.negate());
		return this.computePower(exponent);
	}

	/**
	 * Compute Power with fancy stuff.
	 */
	private Fraction computePower(BigInteger exponent) {
		if (exponent.equals(BigInteger.ZERO)) {
			return Fraction.ONE;
		} else {
			final BigInteger two = new BigInteger("2");
			final Fraction divideAndConquer = this.computePower(exponent.divide(two)).square();
			if(Fraction.isEven(exponent)) {
				return divideAndConquer;
			}
			else {
				return divideAndConquer.multiply(this);
			}
		}
	}
	/**
	 * @ return the square of the receiver.
	 */
	private Fraction square() {
		return this.multiply(this);
	}
	/**
	 * @return
	 */
	private static boolean isEven(BigInteger number) {
		return number.mod(new BigInteger("2")).equals(BigInteger.ZERO);
	}

}