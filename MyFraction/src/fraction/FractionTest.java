package fraction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

public class FractionTest {
	public static Fraction f1_2;
	public static Fraction fn1_2;
	public static Fraction f1_3;
	public static Fraction f1_4;
	public static Fraction fn1_4;
	public static Fraction f1_5;
	public static Fraction f2_5;

	// TODO 15-20 Testf�lle machen und Umkehrfunktionen sowie negative Sachen
	// und Nuller

	@Before
	public void setUp() {
		try {
			fn1_2 = Fraction.create(BigInteger.ONE, new BigInteger("-2"));
			f1_2 = Fraction.create(BigInteger.ONE, new BigInteger("2"));
			f1_3 = Fraction.create(BigInteger.ONE, new BigInteger("3"));
			fn1_4 = Fraction.create(BigInteger.ONE, new BigInteger("-4"));
			f1_4 = Fraction.create(BigInteger.ONE, new BigInteger("4"));
			f1_5 = Fraction.create(BigInteger.ONE, new BigInteger("5"));
			f2_5 = Fraction.create(new BigInteger("2"), new BigInteger("5"));
		} catch (final Exception e) {
			fail();
		}

	}

	@Test
	public void normalise() {
		try {
			assertEquals(Fraction.create(new BigInteger("-1"), BigInteger.TEN).toString(),
					Fraction.create(BigInteger.ONE, new BigInteger("-10")).toString());
			assertEquals(Fraction.create(new BigInteger("1"), new BigInteger("10")).toString(),
					Fraction.create(new BigInteger("-1"), new BigInteger("-10")).toString());
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void createFromBigInteger() {
		try {
			assertEquals(Fraction.create(BigInteger.ONE, BigInteger.TEN),
					Fraction.create(BigInteger.ONE, BigInteger.TEN));
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void testFractionCreationException() {
		try {
			Fraction.create(BigInteger.TEN, BigInteger.ZERO);
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create(BigInteger.ZERO, BigInteger.ZERO);
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create(BigInteger.ONE, BigInteger.ZERO);
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("1/0");
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("1/-0");
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("-1/0");
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("0/0");
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("-0/0");
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("0/-0");
			fail();
		} catch (final Exception e) {
		}

	}

	@Test
	public void AdditionTest() {
		assertEquals(f1_2, f1_4.add(f1_4));
		assertEquals(Fraction.ONE, f1_4.add(f1_4).add(f1_4).add(f1_4));
		assertEquals(Fraction.ZERO, f1_4.add(fn1_4));
		assertEquals(f1_4, f1_4.add(f1_4).add(fn1_4));

		assertEquals(f1_4, f1_4.add(f2_5).subtract(f2_5));
		assertEquals(f1_4, f1_4.add(f1_2).subtract(f1_2));
		assertEquals(f1_4, f1_4.add(f1_5).subtract(f1_5));

		try {
			assertEquals(Fraction.create("-1/2"), fn1_4.add(fn1_4));
		} catch (final FractionConstructionException e) {
			fail();
		}
		assertEquals(f1_4, f1_4.add(fn1_4).subtract(fn1_4));

		assertEquals(Fraction.ONE, f1_4.add(f1_4).add(f1_4).add(f1_4));

	}

	@Test
	public void SubtractTest() {
		assertEquals(f1_5, f2_5.subtract(f1_5));
		assertEquals(f1_4, f1_2.subtract(f1_4));
		assertEquals(Fraction.ZERO, f2_5.subtract(f1_5).subtract(f1_5));
		assertEquals(Fraction.ZERO, f2_5.subtract(f2_5));
		assertEquals(Fraction.ONE, Fraction.ONE.subtract(Fraction.ZERO));
		assertEquals(Fraction.ZERO, f2_5.subtract(f1_5).subtract(f1_5));

		assertEquals(f1_5, f1_5.subtract(f1_5).add(f1_5));
		assertEquals(f2_5, f2_5.subtract(f1_4).add(f1_4));
		assertEquals(f2_5, f2_5.subtract(Fraction.ONE).add(Fraction.ONE).subtract(Fraction.ZERO));

		assertEquals(f1_2, f1_4.subtract(fn1_4));
		assertEquals(f1_4, f1_4.subtract(fn1_4).add(fn1_4));
		assertEquals(Fraction.ZERO, f1_4.subtract(f1_4));

	}

	@Test
	public void MulTest() {
		assertEquals(f1_4, f1_2.multiply(f1_2));
		assertEquals(f1_4, fn1_2.multiply(fn1_2));
		assertEquals(fn1_4, f1_2.multiply(fn1_2));
		assertEquals(fn1_4, fn1_2.multiply(f1_2));

		try {
			assertEquals(f1_4, f1_4.multiply(f2_5).divide(f2_5));
			assertEquals(f1_4, f1_4.multiply(fn1_2).divide(fn1_2));
			assertEquals(f1_4, f1_4.multiply(fn1_4).divide(fn1_4));
		} catch (final FractionConstructionException e) {
			fail();
		}

		assertEquals(Fraction.ZERO, Fraction.ZERO.multiply(Fraction.ZERO));
		assertEquals(Fraction.ZERO, f1_2.multiply(Fraction.ZERO));
		assertEquals(Fraction.ZERO, Fraction.ZERO.multiply(f1_3));

	}

	@Test
	public void DivideTest() {
		try {
			assertEquals(Fraction.create(BigInteger.ONE, BigInteger.ONE), f1_4.divide(f1_4));
		} catch (final Exception e) {
			fail();
		}

		try {
			assertEquals(Fraction.create(BigInteger.ONE, BigInteger.ONE),
					f1_4.divide(Fraction.create(BigInteger.ZERO, BigInteger.TEN)));
			fail();
		} catch (final FractionConstructionException e) {
		}
	}

	@Test
	public void createFromStringWithOutSeparatorTest() {
		try {
			assertEquals(Fraction.ONE, Fraction.create("1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.ZERO, Fraction.create("0"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.create(new BigInteger("125"), BigInteger.ONE), Fraction.create("125"));
			assertEquals(Fraction.create(new BigInteger("125"), BigInteger.ONE), Fraction.create("00000125")); // F�hrende
																												// Nullen
			assertEquals(Fraction.create(new BigInteger("13"), BigInteger.ONE), Fraction.create("13"));
			assertEquals(Fraction.create(new BigInteger("42"), BigInteger.ONE), Fraction.create("42"));
			assertEquals(Fraction.create(new BigInteger("-42"), BigInteger.ONE), Fraction.create("-42"));
			assertEquals(Fraction.create(new BigInteger("-42"), BigInteger.ONE), Fraction.create("-000000000042"));
		} catch (final Exception e) {
			fail();
		}

	}

	@Test
	public void createFromStringInconsistentParts() {
		try {
			Fraction.create("hugo/egon");
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("hugo/2");
			fail();
		} catch (final Exception e) {
		}
		try {
			Fraction.create("1/egon");
			fail();
		} catch (final Exception e) {
		}
	}

	@Test
	public void createFromStringWithSeparator() {
		try {
			assertEquals(f1_2, Fraction.create("1/2"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_3, Fraction.create("/3"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_2, Fraction.create("/2"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_3, Fraction.create("3/9"));
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void createFromDecimalString() {
		try {
			assertEquals(f1_2, Fraction.create("0.5"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.create(new BigInteger("3"), new BigInteger("2")), Fraction.create("1.5"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_2, Fraction.create("0.50000"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_4, Fraction.create("0.25"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(f1_2, Fraction.create(".5"));
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void createFromScientificNotation() {

		try {
			assertEquals(f1_2, Fraction.create("5E-1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.create(new BigInteger("3"), new BigInteger("2")), Fraction.create("15E-1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.create(new BigInteger("50"), BigInteger.ONE), Fraction.create("5E+1"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.ONE, Fraction.create("1E-0"));
		} catch (final Exception e) {
			fail();
		}
		try {
			assertEquals(Fraction.ONE, Fraction.create("1E+0"));
		} catch (final Exception e) {
			fail();
		}
	}

	@Test
	public void lessEqTest() {
		assertTrue(Fraction.ZERO.lessEq(Fraction.ONE));
		assertTrue(Fraction.ZERO.lessEq(Fraction.ZERO));
		assertTrue(Fraction.ONE.lessEq(Fraction.ONE));
		assertTrue(Fraction.ZERO.lessEq(f1_2));
		assertTrue(f1_2.lessEq(Fraction.ONE));
		assertTrue(f1_3.lessEq(f1_2));
		// hier fehlt negativ kleiner positiv und negativ kleiner negativ

		assertFalse(Fraction.ONE.lessEq(Fraction.ZERO));
		assertFalse(f1_4.lessEq(f1_5));
		assertFalse(f1_2.lessEq(f1_3));
		assertFalse(f2_5.lessEq(f1_5));

	}
	
	@Test
    public void powerTest() {
        try {
            Fraction.ZERO.power(BigInteger.ZERO);
            fail();
        } catch (final FractionConstructionException e) {
        }
        try {
            assertEquals(Fraction.ZERO,Fraction.ZERO.power(new BigInteger("42")));
            assertEquals(Fraction.ONE,Fraction.create("7").power(BigInteger.ZERO));
            
            
            assertEquals(f1_2, f1_2.power(BigInteger.ONE));
            assertEquals(f1_4, f1_2.power(new BigInteger("2")));
            assertEquals(Fraction.create("16"), f1_4.power(new BigInteger("-2")));
            assertEquals(Fraction.create("1/4"), Fraction.create("-1/2").power(new BigInteger("2")));
            assertEquals(Fraction.create("-1/8"), Fraction.create("-1/2").power(new BigInteger("3")));
            assertEquals(Fraction.create("9"), Fraction.create("-1/3").power(new BigInteger("-2")));
            assertEquals(Fraction.create("-27"), Fraction.create("-1/3").power(new BigInteger("-3")));
            assertEquals(Fraction.create("1/4"), Fraction.create("2").power(new BigInteger("-2")));
            assertEquals(Fraction.create("-27/8"), Fraction.create("-2/3").power(new BigInteger("-3")));
            assertEquals(Fraction.create("-8/27"), Fraction.create("-2/3").power(new BigInteger("3")));
            
        } catch (final Exception e) {
            fail();
        }
    }

}
