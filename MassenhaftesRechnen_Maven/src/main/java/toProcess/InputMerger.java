package toProcess;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.BufferEntryVisitor;
import buffer.DoubleIntegerEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;
import buffer.StopCommand;
import model.ArithmeticProcess;

public class InputMerger {
	
	private static final String Error_Message = "SingleIntegerEntry erwartet und nicht: ";
	private Buffer mergedBuffer;
	private ArithmeticProcess first;
	private ArithmeticProcess second;
	
	public InputMerger(ArithmeticProcess first, ArithmeticProcess second) {
		this.first = first;
		this.second = second;
		this.mergedBuffer = new Buffer();

	}
	
	
	
	public void startMerging() {
		Buffer firstOutput = this.first.getOutputBuffer();
		Buffer secondOutput = this.second.getOutputBuffer();
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				boolean rechnen = true;
				while(rechnen) {
					
					rechnen = firstOutput.get().accept(new BufferEntryVisitor<Boolean>() {
						
						@Override
						public Boolean handle(SingleIntegerEntry firstIntegerEntry) {
							return secondOutput.get().accept(new BufferEntryVisitor<Boolean>() {

								@Override
								public Boolean handle(SingleIntegerEntry secondIntegerEntry) {
									DoubleIntegerEntry mergedEntry = firstIntegerEntry.merge(secondIntegerEntry);
									InputMerger.this.addToOutput(mergedEntry);
									return true;
								}

								@Override
								public Boolean handle(StopCommand stopCommand) {
									InputMerger.this.addToOutput(stopCommand);
									return false;
								}

								@Override
								public Boolean handle(ErrorEntry errorEntry) {
									InputMerger.this.addToOutput(errorEntry);
									return false;
								}

								@Override
								public Boolean handle(DoubleIntegerEntry doubleIntegerEntry) {
									InputMerger.this.addToOutput(new ErrorEntry(new Exception(Error_Message + doubleIntegerEntry.toString())));
									return false;								}
							});
						}
						
						@Override
						public Boolean handle(StopCommand stopCommand) {
							InputMerger.this.addToOutput(stopCommand);
							return false;
						}
						
						@Override
						public Boolean handle(ErrorEntry errorEntry) {
							InputMerger.this.addToOutput(errorEntry);
							return false;
						}
						
						@Override
						public Boolean handle(DoubleIntegerEntry doubleIntegerEntry) {
							InputMerger.this.addToOutput(new ErrorEntry(new Exception(Error_Message + doubleIntegerEntry.toString())));
							return false;
						}
					});
				}
			}
		});
		thread.start();
	}
	
	private void addToOutput(BufferEntry entry) {
		InputMerger.this.mergedBuffer.put(entry);
	}
	
	public Buffer getMergedBuffer() {
		return this.mergedBuffer;
	}

}
