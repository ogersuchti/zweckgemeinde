package toProcess;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.BufferEntryVisitor;
import buffer.DoubleIntegerEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;
import buffer.StopCommand;

public class Vervielfacher {
	
	private final Buffer input;
	private final Buffer output;
	private int anzahlMultiplikationen;
	
	public void start() {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				boolean vervielfachen = true;
				while(vervielfachen) {
					vervielfachen = Vervielfacher.this.input.get().accept(new BufferEntryVisitor<Boolean>() {

						@Override
						public Boolean handle(SingleIntegerEntry integerEntry) {
							Vervielfacher.this.addToOutput(integerEntry,Vervielfacher.this.anzahlMultiplikationen);
							return true;
						}

						@Override
						public Boolean handle(StopCommand stopCommand) {
							Vervielfacher.this.addToOutput(stopCommand,Vervielfacher.this.anzahlMultiplikationen);
							return false;
						}

						@Override
						public Boolean handle(ErrorEntry errorEntry) {
							Vervielfacher.this.addToOutput(errorEntry,Vervielfacher.this.anzahlMultiplikationen);
							return false;
						}

						@Override
						public Boolean handle(DoubleIntegerEntry doubleIntegerEntry) {
							Vervielfacher.this.addToOutput(doubleIntegerEntry,Vervielfacher.this.anzahlMultiplikationen);
							return true;
						}
					});
				}
			}
		});
		thread.start();
	}
	
	protected void addToOutput(BufferEntry entry, int anzahlMultiplikationen2) {
		int i = 0;
		while (i < anzahlMultiplikationen2) {
			this.output.put(entry);
			i++;
		}
	}

	public Vervielfacher(Buffer input, int anzahlMultiplikationen) {
		super();
		this.input = input;
		this.output = new Buffer();
		this.anzahlMultiplikationen = anzahlMultiplikationen;
	}

	public int getAnzahlMultiplikationen() {
		return this.anzahlMultiplikationen;
	}



	public Buffer getOutput() {
		return this.output;
	}

	public void increaseFaktor() {
		this.anzahlMultiplikationen = this.anzahlMultiplikationen + 1;
	}
	
	
	

}
