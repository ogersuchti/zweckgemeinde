package model;

import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;

public class Division extends ArithmeticProcess{
	
	public static Division create(Buffer input) {
		return new Division(input, new Buffer());
	}

	protected Division(Buffer input, Buffer output) {
		super(input, output);
	}

	@Override
	BufferEntry doThings(SingleIntegerEntry integerEntry) {
		// TODO nachdenken Was tun am Besten eigene Exception
		// Uncomplete Input Exception oder Sowas
		throw new RuntimeException();
	}

	@Override
	BufferEntry combine(BigInteger first, BigInteger second) {
		try {
			
			SingleIntegerEntry entry = new SingleIntegerEntry(first.divide(second));
			return entry;
		}
		catch (ArithmeticException e) {
			return new ErrorEntry(e);
		}
	}


}
