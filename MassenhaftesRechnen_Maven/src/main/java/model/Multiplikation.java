package model;

import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.SingleIntegerEntry;

public class Multiplikation extends ArithmeticProcess{
	
	
	public static Multiplikation create(Buffer input) {
		return new Multiplikation(input, new Buffer());
	}

	protected Multiplikation(Buffer input, Buffer output) {
		super(input, output);
	}

	@Override
	BufferEntry doThings(SingleIntegerEntry integerEntry) {
		// TODO nachdenken Was tun am Besten eigene Exception
		// Uncomplete Input Exception oder Sowas
		throw new RuntimeException();
	}

	@Override
	BufferEntry combine(BigInteger first, BigInteger second) {
		return new SingleIntegerEntry(first.multiply(second));
	}
}
