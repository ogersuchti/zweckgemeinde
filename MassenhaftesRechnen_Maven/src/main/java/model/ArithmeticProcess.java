package model;


import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.BufferEntryVisitor;
import buffer.DoubleIntegerEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;
import buffer.StopCommand;

public abstract class ArithmeticProcess {

	private Buffer input;
	private Buffer output;
	private Thread myThread;

	
	protected ArithmeticProcess(Buffer input, Buffer output) {
		super();
		this.input = input;
		this.output = output;
		
	}



	public void start() {
		this.myThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				boolean rechnen = true;
				while(rechnen) {
					rechnen = ArithmeticProcess.this.input.get().accept(new BufferEntryVisitor<Boolean>() {

						@Override
						public Boolean handle(SingleIntegerEntry integerEntry) {
							// Einzeleinträge werden einfach weitergegeben TODO überlegen soll das so
							ArithmeticProcess.this.addToOutput(ArithmeticProcess.this.doThings(integerEntry));
							return true;
						}


						@Override
						public Boolean handle(StopCommand stopCommand) {
							return false;
						}

						@Override
						public Boolean handle(ErrorEntry errorEntry) {
							// TODO throw Exception ?
							ArithmeticProcess.this.addToOutput(errorEntry);
							errorEntry.printStackTrace();
							return false;
						}

						@Override
						public Boolean handle(DoubleIntegerEntry doubleIntegerEntry) {
							BigInteger first = doubleIntegerEntry.getFirstValue();
							BigInteger second = doubleIntegerEntry.getSecondValue();
							BufferEntry entry = ArithmeticProcess.this.combine(first, second);
							ArithmeticProcess.this.addToOutput(entry);
							return true;
						}

					});
				}
				
			}
		});
		this.myThread.start();
	}
	
	abstract BufferEntry doThings(SingleIntegerEntry integerEntry);
	
	abstract BufferEntry combine(BigInteger first,BigInteger second);

	private void addToOutput(BufferEntry entry) {
		ArithmeticProcess.this.output.put(entry);
	}


	public Buffer getOutputBuffer() {
		return this.output;
	}
	
}
