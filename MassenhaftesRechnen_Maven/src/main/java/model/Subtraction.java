package model;

import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.SingleIntegerEntry;

public class Subtraction extends ArithmeticProcess{

	public static Subtraction create(Buffer input) {
		return new Subtraction(input, new Buffer());
	}

	protected Subtraction(Buffer input, Buffer output) {
		super(input, output);
	}

	@Override
	BufferEntry doThings(SingleIntegerEntry integerEntry) {
		// TODO nachdenken Was tun am Besten eigene Exception
		// Uncomplete Input Exception oder Sowas
		throw new RuntimeException();
	}

	@Override
	BufferEntry combine(BigInteger first, BigInteger second) {
		return new SingleIntegerEntry(first.subtract(second));
	}


}
