package model;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.SingleIntegerEntry;

public class Constant {
	
	private Buffer output;

	public Constant(BigInteger myNumber) {
		this.output = new ConstantBuffer(myNumber);
	}
	
	public Buffer getOutputBuffer() {
		return this.output;
	}

}
class ConstantBuffer extends Buffer{
	
		
		List<BufferEntry> implementingList;
		
		public ConstantBuffer(BigInteger theNumber){
			this.implementingList = new LinkedList<>();
			this.implementingList.add(new SingleIntegerEntry(theNumber));
		}

		@Override
		public synchronized void put(BufferEntry value) {
		throw new Error("Nothing to be put in the Outputstream of a Constant");
		}
		
		@Override
		synchronized public BufferEntry get() {
			while (this.isEmpty())
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			BufferEntry result = this.implementingList.get(0);
			return result;
		}
		private boolean isEmpty(){
			return this.implementingList.isEmpty();
		}
	}
