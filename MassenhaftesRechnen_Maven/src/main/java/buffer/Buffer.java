package buffer;



import java.util.LinkedList;
import java.util.List;


// test
public class Buffer {
	
	List<BufferEntry> implementingList;
	
	public Buffer(){
		this.implementingList = new LinkedList<>();
	}

	synchronized public void put(BufferEntry value) {
		this.implementingList.add(value);
		this.notify();
	}

	synchronized public BufferEntry get() {
		while (this.isEmpty())
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		BufferEntry result = this.implementingList.get(0);
		this.implementingList.remove(0);
		return result;
	}
	private boolean isEmpty(){
		return this.implementingList.isEmpty();
	}
}
