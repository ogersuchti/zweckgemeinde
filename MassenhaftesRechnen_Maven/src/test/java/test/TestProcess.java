package test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.DoubleIntegerEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;
import buffer.StopCommand;
import model.Addition;
import model.ArithmeticProcess;
import model.Constant;
import model.Division;
import model.Multiplikation;
import model.Subtraction;

public class TestProcess {

	private ArithmeticProcess process;
	private Buffer input;

	@Before
	public void setUp() {
		this.input = new Buffer();
		this.input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(33)));
		this.input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(25)));
		this.input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(-25)));
		this.input.put(new StopCommand());
	}

	@Test
	public void testAddition() {
		this.process = Addition.create(this.input);
		this.process.start();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(58)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(50)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(0)), output.get());

	}

	@Test
	public void testMultiplikation() {
		this.process = Multiplikation.create(this.input);
		this.process.start();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(825)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(625)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(-625)), output.get());

	}

	@Test
	public void testSubtraktion() {
		this.process = Subtraction.create(this.input);
		this.process.start();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(-8)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(0)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(50)), output.get());

	}

	@Test
	public void testDivision() {
		this.input = new Buffer();
		this.input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(33)));
		this.input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(25)));
		this.input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(-25)));
		this.input.put(new DoubleIntegerEntry(BigInteger.valueOf(55), BigInteger.ZERO));
		this.input.put(new StopCommand());

		this.process = Division.create(this.input);
		this.process.start();
		Buffer output = this.process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(0)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(1)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(-1)), output.get());
		assertEquals(new ErrorEntry(), output.get());

	}

	@Test
	public void testConstant() {
		Constant c50 = new Constant(BigInteger.valueOf(50));
		Buffer outputBuffer = c50.getOutputBuffer();
		int i = 0;
		while (i < 500) {
			assertEquals(new SingleIntegerEntry(BigInteger.valueOf(50)), outputBuffer.get());
			i++;
		}
	}

}
