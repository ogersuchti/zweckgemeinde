package test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.DoubleIntegerEntry;
import buffer.StopCommand;
import model.Addition;
import model.ArithmeticProcess;
import toProcess.InputMerger;

public class TestMerger {
	
	private InputMerger merger;
	
	@Before
	public void setUp() {
		Buffer firstInput = new Buffer();
		firstInput.put(new DoubleIntegerEntry(BigInteger.valueOf(2),BigInteger.valueOf(2)));
		firstInput.put(new DoubleIntegerEntry(BigInteger.valueOf(6),BigInteger.valueOf(2)));
		firstInput.put(new DoubleIntegerEntry(BigInteger.valueOf(2),BigInteger.valueOf(-2)));
		firstInput.put(new StopCommand());
		Buffer secondInput = new Buffer();
		secondInput.put(new DoubleIntegerEntry(BigInteger.valueOf(2),BigInteger.valueOf(777)));
		secondInput.put(new DoubleIntegerEntry(BigInteger.valueOf(2),BigInteger.valueOf(33)));
		secondInput.put(new DoubleIntegerEntry(BigInteger.valueOf(2),BigInteger.valueOf(10)));
		secondInput.put(new StopCommand());
		ArithmeticProcess first = Addition.create(firstInput);
		ArithmeticProcess second = Addition.create(secondInput);
		first.start();
		second.start();
		this.merger = new InputMerger(first, second);
		
	}
	@Test
	public void testMerging() {
		this.merger.startMerging();
		Buffer out = this.merger.getMergedBuffer();
		assertEquals(new DoubleIntegerEntry(BigInteger.valueOf(4), BigInteger.valueOf(779)),out.get());
		assertEquals(new DoubleIntegerEntry(BigInteger.valueOf(8), BigInteger.valueOf(35)),out.get());
		assertEquals(new DoubleIntegerEntry(BigInteger.valueOf(0), BigInteger.valueOf(12)),out.get());
	}

}
