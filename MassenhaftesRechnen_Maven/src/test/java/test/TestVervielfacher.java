package test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.SingleIntegerEntry;
import buffer.StopCommand;
import toProcess.Vervielfacher;

public class TestVervielfacher {
	
	private Vervielfacher fuenfer;
	
	@Before
	public void setUp() {
		Buffer input = new Buffer();
		input.put(new SingleIntegerEntry(BigInteger.valueOf(2)));
		input.put(new StopCommand());
		this.fuenfer= new Vervielfacher(input, 5);
	}

	@Test
	public void test() {
		this.fuenfer.start();
		Buffer output = this.fuenfer.getOutput();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(2)),output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(2)),output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(2)),output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(2)),output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(2)),output.get());
		assertEquals(new StopCommand(),output.get());
		assertEquals(new StopCommand(),output.get());
		assertEquals(new StopCommand(),output.get());
		assertEquals(new StopCommand(),output.get());
		assertEquals(new StopCommand(),output.get());
	}

}
