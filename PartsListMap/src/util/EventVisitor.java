package util;

public interface EventVisitor {
    
	/**
     * handles the PriceChangedEvent
     */
	void handle(PriceChangedEvent priceChangedEvent);
	
	/**
     * handles the StructureChangedEvent
     */
	void handle(StructureChangedEvent structureChangedEvent);

}
