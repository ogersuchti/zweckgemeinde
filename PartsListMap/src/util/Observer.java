package util;

public interface Observer {
	
	/**
	 * the Observer reacts to the Event using update
	 */
	public abstract void update(Event e);

}
