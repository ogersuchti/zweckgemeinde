package state;

import model.MaterialList;
import model.Product;

public class PMLCached extends State {

	private final int price;
	private final MaterialList matList;

	public PMLCached( MaterialList matList, int price) {
		this.price = price;
		this.matList = matList;
	}

	@Override
	public void priceChanged(Product product) {
		product.setState(new MLCached(this.matList));
	}

	@Override
	public void structureChanged(Product product) {
		product.setState(NothingCached.create());

	}

	@Override
	public int getPrice(Product product) {
		return this.price;
	}

	@Override
	public MaterialList getMaterialList(Product product) {
		return this.matList;
	}

}
