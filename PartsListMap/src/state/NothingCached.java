package state;

import model.MaterialList;
import model.Product;

public class NothingCached extends State {

	private static NothingCached singleObject;

	/**
	 * returns the singleton NothingCached(State)
	 */
	public static NothingCached create() {
		if (singleObject == null) {
			singleObject = new NothingCached();
		}
		return singleObject;
	}

	private NothingCached() {
	}

	@Override
	public void priceChanged(Product product) {
	}

	@Override
	public void structureChanged(Product product) {
	}

	@Override
	public int getPrice(Product product) {
		product.setState(new PCached(product.computeOverallPrice()));
		return product.getOverallPrice();
	}

	@Override
	public MaterialList getMaterialList(Product product) {
		final MaterialList newMateriallist = product.computeMaterialList();
		product.setState(new MLCached(newMateriallist));
		return newMateriallist;
	}

}
