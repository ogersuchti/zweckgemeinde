package model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import state.NothingCached;
import state.State;
import util.Event;
import util.EventVisitor;
import util.Observer;
import util.PriceChangedEvent;
import util.StructureChangedEvent;

public class Product extends ComponentCommon implements Observer{

	private final Map<String, QuantifiedComponent> parts;
	private State state;

	private static final String CycleMessage = "Keine Zyklen erlaubt";

	private Product(final String name, final int price, final Map<String, QuantifiedComponent> parts) {
		super(name, price);
		this.parts = parts;
		this.state = NothingCached.create();
	}

	/**
	 * @param name
	 *            used to fill the field name
	 * @param price
	 *            used to fill the field price
	 * @return A new object from type Product. Filling the fields with the given
	 *         parameters and an empty HashMap.
	 */
	public static Product create(final String name, final int price) {
		return new Product(name, price, new HashMap<String, QuantifiedComponent>());

	}

	@Override
	/**
	 * Modifies the receiver object by adding a new
	 * QuantifiedComponent(constructed by the given parameters) to the field
	 * parts. If the field parts already contains the new QuantifiedComponent
	 * their quantities are get added up.
	 */
	public void addPart(final ComponentCommon part, final int quantity) throws Exception {
		if (part.contains(this)) // register !!!
			throw new Exception(CycleMessage);
		final QuantifiedComponent OldEntry = this.parts.get(part.getName());
		if (OldEntry == null) {
			this.parts.put(part.getName(), QuantifiedComponent.create(quantity, part));
			part.register(this);
		} else {
			OldEntry.addQuantity(quantity);
		}
		part.notifyObservers(new StructureChangedEvent());

	}

	@Override
	protected boolean ContainsDetails(final Component part) {
		final Iterator<QuantifiedComponent> iterator = this.parts.values().iterator();
		while (iterator.hasNext()) {
			final QuantifiedComponent current = iterator.next();
			if (current.contains(part))
				return true;
		}
		return false;
	}

	@Override
	/**
	 * @return The overall price for the receiver. Adding up all costs for the
	 *         parts and adding the receiver cost.
	 */
	public int getOverallPrice() {
		return this.state.getPrice(this);
	}

	/**
	 * @return The overall price for the receiver by computing it.
	 */
	public int computeOverallPrice() {
		final Iterator<QuantifiedComponent> iterator = this.parts.values().iterator();
		int sum = 0;
		while (iterator.hasNext()) {
			final QuantifiedComponent current = iterator.next();
			sum += current.getOverallPrice();
		}
		return sum + this.price;
	}

	@Override
	/**
	 * @return The MaterialList needed to produce the receiver by asking the
	 *         cache in form of the state.
	 */
	public MaterialList getMaterialList() {
		return this.state.getMaterialList(this);
	}

	/**
	 * @return The computed MaterialList needed to produce the receiver.
	 */
	@Override
	public MaterialList computeMaterialList() {
		MaterialList out = MaterialList.create();
		final Iterator<QuantifiedComponent> iterator = this.parts.values().iterator();
		while (iterator.hasNext()) {
			final QuantifiedComponent current = iterator.next();
			out = out.add(current.getMaterialList());
		}

		return out;
	}

	/**
	 * @param state
	 *            new state that overrides the field state.
	 *
	 */
	public void setState(final State state) {
		this.state = state;
	}

	@Override
	public Component copy() {
		throw new Error();
	}

	@Override
	public void update(final Event e) {
		e.accept(new EventVisitor() {
			
			@Override
			public void handle(final StructureChangedEvent structureChangedEvent) {
				Product.this.state.structureChanged(Product.this);
				Product.this.notifyObservers(e);
				
			}
			
			@Override
			public void handle(final PriceChangedEvent priceChangedEvent) {
				Product.this.state.priceChanged(Product.this);
				Product.this.notifyObservers(e);
				
			}
		});
		
	}

}
