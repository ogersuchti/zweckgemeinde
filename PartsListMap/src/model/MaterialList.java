package model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MaterialList {

	private final Map<String, QuantifiedComponent> materialsWithQuantity;

	/**
	 * Create empty MaterialList
	 */
	public static MaterialList create() {
		return new MaterialList();
	}

	/**
	 * Creates Materialist with one entry. The entry is a Quantified componnent
	 * with the given Component and quantity.
	 */
	public static MaterialList create(final ComponentCommon material, final int quantity) {
		final HashMap<String, QuantifiedComponent> map = new HashMap<String, QuantifiedComponent>();
		map.put(material.getName(), QuantifiedComponent.create(quantity, material));
		return new MaterialList(map);
	}

	/**
	 * Create empty HashMap as materialsWithQuantity
	 */
	private MaterialList() {
		this.materialsWithQuantity = new HashMap<String, QuantifiedComponent>();
	}

	/**
	 * Create MaterialList from given HashMap
	 */
	private MaterialList(final Map<String, QuantifiedComponent> material) {
		this.materialsWithQuantity = material;
	}

	/**
	 * Returns the receiver object with all entries from summand added by using
	 * the method addMaterial.
	 */
	public MaterialList add(final MaterialList summand) {
		final MaterialList newMaterialList = this.clone();

		final Iterator<QuantifiedComponent> materialIterator = summand.materialsWithQuantity.values().iterator();

		while (materialIterator.hasNext()) {
			final QuantifiedComponent current = materialIterator.next();
			newMaterialList.addMaterial(current);
		}

		return newMaterialList;

	}

	/**
	 * Adds the given QuantifiedComponent material to the receiver list of
	 * QuantifiedComponent's. If the material is already part of the list. Just
	 * the quantity gets added up in the listed object.
	 */
	public void addMaterial(final QuantifiedComponent material) {

		final QuantifiedComponent oldEntry = this.materialsWithQuantity.get(material.getName());

		if (oldEntry == null) {
			final QuantifiedComponent newMaterial = material.clone();
			this.materialsWithQuantity.put(material.getName(), newMaterial);
			return;
		}
		oldEntry.addQuantityOf(material);

	}

	/**
	 * Return the receiver list with each entry's quantity multiplicated by
	 * factor.
	 */
	public MaterialList mul(final int factor) {
		final Iterator<QuantifiedComponent> materialIterator = this.materialsWithQuantity.values().iterator();
		final MaterialList newMaterialList = this.clone();

		while (materialIterator.hasNext()) {
			final QuantifiedComponent current = materialIterator.next();
			final QuantifiedComponent newComponent = current.clone();
			newComponent.multiplyQuantity(factor);
			newMaterialList.materialsWithQuantity.put(newComponent.getName(), newComponent);

		}
		return newMaterialList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((materialsWithQuantity == null) ? 0 : materialsWithQuantity.hashCode());
		return result;
	}

	@Override

	public boolean equals(final Object obj) {
		if (obj instanceof MaterialList) {
			final MaterialList argument = (MaterialList) obj;
			return this.materialsWithQuantity.equals(argument.materialsWithQuantity);
		}
		return false;
	}

	/**
	 * 
	 * @return the field materialsWithQuantity
	 */
	public Map<String, QuantifiedComponent> getMaterialsWithQuantity() {
		return this.materialsWithQuantity;
	}

	@Override
	/**
	 * @returns a copy of the receiver object as new object.
	 */
	public MaterialList clone() {
		final MaterialList newMaterialList = create();

		final Iterator<QuantifiedComponent> iterator = this.materialsWithQuantity.values().iterator();
		while (iterator.hasNext()) {
			final QuantifiedComponent current = iterator.next();
			final QuantifiedComponent newQuantifiedComponent = current.clone();
			newMaterialList.materialsWithQuantity.put(newQuantifiedComponent.getName(), newQuantifiedComponent);

		}

		return newMaterialList;

	}

	@Override
	public String toString() {
		String out = "";
		final Iterator<QuantifiedComponent> iterator = this.materialsWithQuantity.values().iterator();
		while (iterator.hasNext()) {
			final QuantifiedComponent current = iterator.next();
			out = out + current.toString() + "\n";
		}
		return out;

	}

}
