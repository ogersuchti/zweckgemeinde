package model;

import util.Observee;

public abstract class ComponentCommon extends Observee implements Component {

	protected final String name;
	protected final int price;

	protected ComponentCommon(String name, int price) {
		super();
		this.name = name;
		this.price = price;

	}

	public abstract void addPart(ComponentCommon part, int quantity) throws Exception;
	@Override
	public String getName() {
		return this.name;
	}
	@Override
	public boolean equals(Object argument) {
		if (argument instanceof Component) {
			final Component argumentasComponent = (Component) argument;
			return this.name.equals(argumentasComponent.getName());

		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

	@Override
	public boolean contains(Component part) {
		if (this.equals(part))
			return true;
		return this.ContainsDetails(part);

	}

	protected abstract boolean ContainsDetails(Component part);

	@Override
	public String toString() {
		return this.getName();
	}

}
