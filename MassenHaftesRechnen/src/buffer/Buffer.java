package buffer;


import java.util.LinkedList;
import java.util.List;

public class Buffer<T> {
	
	List<T> implementingList;
	
	public Buffer(){
		this.implementingList = new LinkedList<>();
	}

	synchronized public void put(T value) { // mutex.enter()
		this.implementingList.add(value);
		this.notify();
	} // mutex.leave()

	synchronized public T get() {
		while (this.isEmpty())
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		T result = this.implementingList.get(0);
		this.implementingList.remove(0);
		return result;
	}
	private boolean isEmpty(){
		return this.implementingList.isEmpty();
	}
}
