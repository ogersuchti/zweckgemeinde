package buffer;

public interface BufferEntry {
	
	<T> T accept(BufferEntryVisitor<T> visitor);

}
