package buffer;


import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class ConstantBuffer {
	
	List<BigInteger> implementingList;
	
	public ConstantBuffer(){
		this.implementingList = new LinkedList<>();
	}

	synchronized public void put(BigInteger value) {
		this.implementingList.add(value);
		this.notify();
	}

	synchronized public BigInteger get() {
		while (this.isEmpty())
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		BigInteger result = this.implementingList.get(0);
		return result;
	}
	private boolean isEmpty(){
		return this.implementingList.isEmpty();
	}
}
