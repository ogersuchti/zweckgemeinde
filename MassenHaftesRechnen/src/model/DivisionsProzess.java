package model;
import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;

public class DivisionsProzess extends ArithmetischerProzess{
	
	public DivisionsProzess (Buffer<BufferEntry> eingabe1, Buffer<BufferEntry> eingabe2) {
		super(eingabe1, eingabe2);
	}

	@Override
	protected BigInteger combine(BigInteger firstValue, BigInteger secondValue) {
		if (secondValue.equals(BigInteger.ZERO)){
			throw new Error("404: Schatzi... nicht durch Null!");
		}
		return firstValue.divide(secondValue);
		
	}
	

}
