package model;

import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.BufferEntryVisitor;
import buffer.IntegerEntry;
import buffer.StopCommand;

public abstract class ArithmetischerProzess {
	// TODO Vorschl�ge von Schulz pr�fen
	// Error Entry f�r Division durch Null (Kapselt java Exception)
	// Vorschl�ge Operator Hierarchie statt abstrakter Klasse
	// Nur ein Eingabestrom mit BufferEntry z.b als Tupel Triple errpor einzelneer Wert
	// Anonyme Klasse hat keine m�glichkeit f�r javaDoc Kommentare
	// Anonyme Klasse hat keine Konstruktoren --> Keine Variablen k�nnen �bergeben werden

	protected Buffer<BufferEntry> firstArguments;
	protected Buffer<BufferEntry> secondArguments;
	protected Buffer<BufferEntry> results;
	private Thread myThread;
	
	public ArithmetischerProzess(Buffer<BufferEntry> eingabe1, Buffer<BufferEntry> eingabe2) {
		firstArguments = eingabe1;
		secondArguments = eingabe2;
		results = new Buffer<BufferEntry>();
	}

	public void start() {
		myThread = new Thread(new Runnable() {
			
	
			@Override
			public void run() {
				boolean rechnen = true;
				while(rechnen) {
					BufferEntry first = firstArguments.get();
					BufferEntry second = secondArguments.get();
					rechnen = first.accept(new BufferEntryVisitor<Boolean>(){
						
						@Override
						public Boolean handle(StopCommand stopCommand) {
							return false;
						}
						
						@Override
						public Boolean handle(IntegerEntry integerEntry) {
							BigInteger firstValue = integerEntry.getValue();
							return second.accept(new BufferEntryVisitor<Boolean>() {
								
								@Override
								public Boolean handle(StopCommand stopCommand) {
									return false;
								}
								
								@Override
								public Boolean handle(IntegerEntry integerEntry) {
									BigInteger result = ArithmetischerProzess.this.combine(firstValue,integerEntry.getValue());
									ArithmetischerProzess.this.results.put(new IntegerEntry(result));
									return true;
								}
							});
						}
					});
				}
			}
		});
		myThread.start();
	}

	protected  abstract BigInteger combine(BigInteger firstValue, BigInteger value);

	public Buffer<BufferEntry> getAusgabePuffer() {
		return results;
	}
	
	

}
