package model;
import java.math.BigInteger;

import buffer.Buffer;
import buffer.BufferEntry;

public class MultiplikationsProzess extends ArithmetischerProzess{
	
	public MultiplikationsProzess (Buffer<BufferEntry> eingabe1, Buffer<BufferEntry> eingabe2) {
		super(eingabe1, eingabe2);
	}

	@Override
	protected BigInteger combine(BigInteger firstValue, BigInteger secondValue) {
		return firstValue.multiply(secondValue);
	}
	

}
