package test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.IntegerEntry;
import buffer.StopCommand;
import model.AdditionsProzess;
import model.ArithmetischerProzess;
import model.DivisionsProzess;
import model.MultiplikationsProzess;

public class TestProcess {

	private ArithmetischerProzess aProzess1;
	private ArithmetischerProzess aProzess2;

	@Before
	public void setUp() {
		Buffer<BufferEntry> eingabe1 = new Buffer<>();
		Buffer<BufferEntry> eingabe2 = new Buffer<>();
		Buffer<BufferEntry> eingabe3 = new Buffer<>();
		Buffer<BufferEntry> eingabe4 = new Buffer<>();

		eingabe1.put(new IntegerEntry(BigInteger.valueOf(5)));
		eingabe2.put(new IntegerEntry(BigInteger.valueOf(6)));
		eingabe1.put(new IntegerEntry(BigInteger.valueOf(-3)));
		eingabe2.put(new IntegerEntry(BigInteger.valueOf(5)));
		eingabe1.put(new IntegerEntry(BigInteger.valueOf(0)));
		eingabe2.put(new IntegerEntry(BigInteger.valueOf(1)));
		eingabe1.put(new StopCommand());
		eingabe2.put(new StopCommand());

		eingabe3.put(new IntegerEntry(BigInteger.valueOf(5)));
		eingabe4.put(new IntegerEntry(BigInteger.valueOf(6)));
		eingabe3.put(new IntegerEntry(BigInteger.valueOf(-3)));
		eingabe4.put(new IntegerEntry(BigInteger.valueOf(5)));
		eingabe3.put(new IntegerEntry(BigInteger.valueOf(0)));
		eingabe4.put(new IntegerEntry(BigInteger.valueOf(1)));
		eingabe3.put(new StopCommand());
		eingabe4.put(new StopCommand());

		aProzess1 = new AdditionsProzess(eingabe1, eingabe2);
		aProzess2 = new AdditionsProzess(eingabe3, eingabe4);
	}

	@Test
	public void testsimpleAddProzess() {
		aProzess1.start();
		Buffer<BufferEntry> ausgabePuffer1 = aProzess1.getAusgabePuffer();
		assertEquals(new IntegerEntry(BigInteger.valueOf(11)), ausgabePuffer1.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(2)), ausgabePuffer1.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(1)), ausgabePuffer1.get());

		aProzess2.start();
		Buffer<BufferEntry> ausgabePuffer2 = aProzess2.getAusgabePuffer();
		assertEquals(new IntegerEntry(BigInteger.valueOf(11)), ausgabePuffer2.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(2)), ausgabePuffer2.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(1)), ausgabePuffer2.get());
	}

	@Test
	public void testNotSimpleAProzess() {
		ArithmetischerProzess derProzess = new AdditionsProzess(aProzess1.getAusgabePuffer(),
				aProzess2.getAusgabePuffer());
		derProzess.start();
		aProzess1.start();
		aProzess2.start();
		Buffer<BufferEntry> ausgabePuffer = derProzess.getAusgabePuffer();
		assertEquals(new IntegerEntry(BigInteger.valueOf(22)), ausgabePuffer.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(4)), ausgabePuffer.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(2)), ausgabePuffer.get());
	}

	@Test
	public void testNotSimpleMProzess() {
		ArithmetischerProzess derProzess = new MultiplikationsProzess(aProzess1.getAusgabePuffer(),
				aProzess2.getAusgabePuffer());
		derProzess.start();
		aProzess1.start();
		aProzess2.start();
		Buffer<BufferEntry> ausgabePuffer = derProzess.getAusgabePuffer();
		assertEquals(new IntegerEntry(BigInteger.valueOf(121)), ausgabePuffer.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(4)), ausgabePuffer.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(1)), ausgabePuffer.get());
	}

	@Test
	public void testNotSimpleDProzess() {
		ArithmetischerProzess derProzess = new DivisionsProzess(aProzess1.getAusgabePuffer(),
				aProzess2.getAusgabePuffer());
		derProzess.start();
		aProzess1.start();
		aProzess2.start();
		Buffer<BufferEntry> ausgabePuffer = derProzess.getAusgabePuffer();
		assertEquals(new IntegerEntry(BigInteger.valueOf(1)), ausgabePuffer.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(1)), ausgabePuffer.get());
		assertEquals(new IntegerEntry(BigInteger.valueOf(1)), ausgabePuffer.get());
	}
}
