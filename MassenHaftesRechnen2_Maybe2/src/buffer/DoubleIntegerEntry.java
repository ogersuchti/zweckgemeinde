package buffer;


import java.math.BigInteger;

public class DoubleIntegerEntry implements BufferEntry{
	
	private BigInteger first;
	private BigInteger second;
	
	public DoubleIntegerEntry(BigInteger first, BigInteger second) {
		this.first = first;
		this.second = second;
	}
	
	public BigInteger getFirstValue() {
		return this.first;
	}
	
	public BigInteger getSecondValue() {
		return this.second;
	}

	@Override
	public <T> T accept(BufferEntryVisitor<T> visitor) {
		return visitor.handle(this);
	}
	
	@Override
	public boolean equals(Object entry){
		if(entry instanceof DoubleIntegerEntry) {
			DoubleIntegerEntry entryAsIntegerEntry = (DoubleIntegerEntry) entry;
			return entryAsIntegerEntry.getFirstValue().equals(this.getFirstValue()) && entryAsIntegerEntry.getSecondValue().equals(this.getSecondValue());
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("(");
		buffer.append(this.first.toString());
		buffer.append(",");
		buffer.append(this.second.toString());
		buffer.append(")");
		return buffer.toString();
	}

}
