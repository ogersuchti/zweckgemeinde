package buffer;

public class ErrorEntry implements BufferEntry{
	
	private Exception encapsulatedException;

	@Override
	public <T> T accept(BufferEntryVisitor<T> visitor) {
		return visitor.handle(this);
	}

	
	public ErrorEntry() {
	}
	public ErrorEntry(Exception exception) {
		super();
		encapsulatedException = exception;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof ErrorEntry;
	}
}
