package buffer;


import java.math.BigInteger;

public class SingleIntegerEntry implements BufferEntry{
	
	private BigInteger value;
	
	public SingleIntegerEntry(BigInteger value) {
		this.value = value;
	}
	
	public BigInteger getValue() {
		return this.value;
	}

	@Override
	public <T> T accept(BufferEntryVisitor<T> visitor) {
		return visitor.handle(this);
	}
	
	@Override
	public boolean equals(Object entry){
		if(entry instanceof SingleIntegerEntry) {
			SingleIntegerEntry entryAsIntegerEntry = (SingleIntegerEntry) entry;
			return entryAsIntegerEntry.getValue().equals(this.getValue());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.value.toString();
	}

}
