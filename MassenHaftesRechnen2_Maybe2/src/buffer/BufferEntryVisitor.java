package buffer;


public interface BufferEntryVisitor<T> {

	public T handle(SingleIntegerEntry integerEntry);

	public T handle(StopCommand stopCommand);

	public T handle(ErrorEntry errorEntry);

	public T handle(DoubleIntegerEntry doubleIntegerEntry);

}
