package Operation;

import java.math.BigInteger;

import buffer.BufferEntry;

public interface ProcessOperation {
	
	public BufferEntry getValue(BigInteger first, BigInteger second);

}
