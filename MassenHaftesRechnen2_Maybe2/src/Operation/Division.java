package Operation;

import java.math.BigInteger;

import buffer.BufferEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;

public class Division implements ProcessOperation{

	@Override
	public BufferEntry getValue(BigInteger first, BigInteger second) {
		if(second.equals(BigInteger.ZERO)) {
			return new ErrorEntry(new Exception("No division by Zero possible"));
		}
		return new SingleIntegerEntry(first.divide(second));
	}

}
