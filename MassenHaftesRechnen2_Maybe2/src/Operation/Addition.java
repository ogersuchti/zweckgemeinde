package Operation;

import java.math.BigInteger;

import buffer.BufferEntry;
import buffer.SingleIntegerEntry;

public class Addition implements ProcessOperation{

	@Override
	public BufferEntry getValue(BigInteger first, BigInteger second) {
		return new SingleIntegerEntry(first.add(second));
	}
	
}
