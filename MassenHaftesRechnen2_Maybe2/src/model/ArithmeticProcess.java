package model;


import java.math.BigInteger;

import Operation.ProcessOperation;
import buffer.Buffer;
import buffer.BufferEntry;
import buffer.BufferEntryVisitor;
import buffer.DoubleIntegerEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;
import buffer.StopCommand;

public class ArithmeticProcess {

	private Buffer inputBuffer;
	private Buffer outputBuffer;
	private ProcessOperation processOperation;
	private Thread myThread;
	
	
	
	public ArithmeticProcess(Buffer inputBuffer,ProcessOperation operation) {
		super();
		this.inputBuffer = inputBuffer;
		processOperation = operation;
		this.outputBuffer = new Buffer();
	}



	public void start() {
		myThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				boolean rechnen = true;
				while(rechnen) {
					rechnen = ArithmeticProcess.this.inputBuffer.get().accept(new BufferEntryVisitor<Boolean>() {

						@Override
						public Boolean handle(SingleIntegerEntry integerEntry) {
							// Einzeleinträge werden einfach weitergegeben TODO überlegen soll das so
							ArithmeticProcess.this.addToOutput(integerEntry);
							return true;
						}

						@Override
						public Boolean handle(StopCommand stopCommand) {
							return false;
						}

						@Override
						public Boolean handle(ErrorEntry errorEntry) {
							// TODO throw Exception ?
							return false;
						}

						@Override
						public Boolean handle(DoubleIntegerEntry doubleIntegerEntry) {
							BigInteger first = doubleIntegerEntry.getFirstValue();
							BigInteger second = doubleIntegerEntry.getSecondValue();
							BufferEntry entry = ArithmeticProcess.this.processOperation.getValue(first, second);
							ArithmeticProcess.this.addToOutput(entry);
							return true;
						}

					});
				}
				
			}
		});
		myThread.start();
	}

	private void addToOutput(BufferEntry entry) {
		ArithmeticProcess.this.outputBuffer.put(entry);
	}


	public Buffer getOutputBuffer() {
		return this.outputBuffer;
	}
	
}
