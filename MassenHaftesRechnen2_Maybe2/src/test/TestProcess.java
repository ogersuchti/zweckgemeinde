package test;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import Operation.Addition;
import Operation.Division;
import Operation.Multiplikation;
import Operation.Subtraktion;
import buffer.Buffer;
import buffer.DoubleIntegerEntry;
import buffer.ErrorEntry;
import buffer.SingleIntegerEntry;
import buffer.StopCommand;
import model.ArithmeticProcess;

public class TestProcess {
	
	private ArithmeticProcess process;
	private Buffer input;

	@Before
	public void setUp() {
		input = new Buffer();
		input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(33)));
		input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(25)));
		input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(-25)));
		input.put(new StopCommand());
	}
	
	@Test
	public void testAddition() {
		process = new ArithmeticProcess(input, new Addition());
		process.start();
		Buffer output = process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(58)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(50)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(0)), output.get());
		
	}
	
	@Test
	public void testMultiplikation() {
		process = new ArithmeticProcess(input, new Multiplikation());
		process.start();
		Buffer output = process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(825)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(625)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(-625)), output.get());
		
	}
	@Test
	public void testSubtraktion() {
		process = new ArithmeticProcess(input, new Subtraktion());
		process.start();
		Buffer output = process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(-8)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(0)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(50)), output.get());
		
	}
	@Test
	public void testDivision() {
		input = new Buffer();
		input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(33)));
		input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(25)));
		input.put(new DoubleIntegerEntry(BigInteger.valueOf(25), BigInteger.valueOf(-25)));
		input.put(new DoubleIntegerEntry(BigInteger.valueOf(55), BigInteger.ZERO));
		input.put(new StopCommand());
		
		process = new ArithmeticProcess(input, new Division());
		process.start();
		Buffer output = process.getOutputBuffer();
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(0)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(1)), output.get());
		assertEquals(new SingleIntegerEntry(BigInteger.valueOf(-1)), output.get());
		assertEquals(new ErrorEntry(), output.get());
		
	}

}
