package model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class Automat {

	private final State anfangsZustand;
	private final State endZustand;
	private final Collection<State> states;
	private final Collection<Transition> delta;
	
	public static Automat create() {
		return new Automat();
	}
	
	private Automat() {
		this.anfangsZustand = State.create(this);
		this.endZustand = State.create(this);
		this.states = new HashSet<State>();
		this.delta = new HashSet<Transition>();
	}
	
	public boolean recognizes(String input) {
		final HashSet<State> startSet = new HashSet<State>();
		startSet.add(anfangsZustand);
		final Configuration start = Configuration.create(input, this, startSet) ;
		final Configuration finalConfig = start.run();
		return this.containsEndState(finalConfig.getCurrentStates());
	}

	public void addState(State state) {
		this.states.add(state);
	}

	public void addTransition(Transition transition) {
		this.delta.add(transition);
	}

	public HashSet<State> nextStates(char input, State state) {
		final HashSet<State> nextStates = new HashSet<State>();
		final Iterator<Transition> iterator = this.delta.iterator();
		while (iterator.hasNext()) {
			final Transition current = iterator.next();
			if (current.getBefore().equals(state)) {
				if (current.isInput(input)) {
					nextStates.add(current.getAfter());
				}
			}

		}
		return nextStates;
	}

	public boolean containsEndState(Collection<State> states) {
		final Iterator<State> iterator = states.iterator();
		while (iterator.hasNext()) {
			final State current = iterator.next();
			if (current.equals(this.endZustand))
				return true;
		}
		return false;
	}

	public State getEndZustand() {
		return endZustand;
	}

	public State getAnfangsZustand() {
		return anfangsZustand;
	}

	public Object getDelta() {
		return this.delta;
	}

	// boolean recognizes (String input)
	// macht Confioguration mit input String und als currentStates nur den Start
	// State und dann run
	// danach pr�fen ob currentStates einen Endzustand enth�lt. --> NFA

}
