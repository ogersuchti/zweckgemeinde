package model;

public class Transition {
	
	private final char input;
	private final State before;
	private final State after;
	
	public static Transition create(char input, State before, State after) {
		return new Transition(input, before, after);
	}
	
	private Transition(char input, State before, State after) {
		this.input = input;
		this.before = before;
		this.after = after;
	}
	
	public boolean isInput(char input) {
		return this.input == input ? true : false;
	}

	public State getAfter() {
		return this.after;
	}

	public State getBefore() {
		return this.before;
	}
	
	
	// prepare State funktion f�r das :
	// Map von [char --> [State --> State*]] --> Index
	// triple zweistellige Abbildung in Potenzmenge 4 fun ma boys


}
