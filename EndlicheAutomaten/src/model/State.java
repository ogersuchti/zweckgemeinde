package model;

public class State {
	
	private final Automat automat;
	
	public  static State create(Automat automat) {
		return new State(automat);
	}
	
	private State(Automat automat) {
		this.automat = automat;
	}
	
	public void addTransition(char c, State after) {
		this.automat.addTransition(Transition.create(c, this, after));
	}
	
	@Override
	public boolean equals(Object arg) {
		if(arg instanceof State) {
			final State argAsState = (State) arg;
			return this == argAsState;
		}
		return false;
	}

}
