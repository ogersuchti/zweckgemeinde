package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import model.Automat;
import model.Configuration;
import model.State;
import model.Transition;

public class AutomatTest {

	// Automat Nr.1
	public State testState1;
	public State testState2;
	public State Z1;
	public Automat a1;

	public Transition t1;

	// Automat Nr.2

	private Automat a2;
	private State z1;
	private State z2;

	@Before
	public void setUp() throws Exception {
		// Automat Nr.1

		testState1 = State.create(a1);
		testState2 = State.create(a1);
		a1 = Automat.create();
		Z1 = State.create(a1);
		a1.addState(Z1);
		t1 = Transition.create('a', a1.getAnfangsZustand(), Z1);
		a1.addTransition(t1);
		a1.addTransition(Transition.create('b', Z1, Z1));
		a1.addTransition(Transition.create('a', Z1, a1.getEndZustand()));

		// Automat Nr.2

		a2 = Automat.create();
		z1 = State.create(a2);
		z2 = State.create(a2);
		a2.addState(z1);
		a2.addState(z2);
		a2.getAnfangsZustand().addTransition('a', z1);
		z1.addTransition('b', a2.getAnfangsZustand());
		z1.addTransition('b', z1);
		z1.addTransition('a', z2);
		z2.addTransition('b', z1);
		z2.addTransition('c', a2.getEndZustand());
		z2.addTransition('b', z2);
	}

	@Test
	public void testEquals() {
		final State state1 = State.create(a1);
		final State state2 = State.create(a1);
		final State state3 = State.create(a1);

		assertFalse(testState1.equals(testState2));
		assertFalse(state1.equals(testState1));
		assertFalse(state2.equals(testState2));

		assertTrue(testState1.equals(testState1));
		assertTrue(testState2.equals(testState2));
		assertTrue(state1.equals(state1));
		assertTrue(state2.equals(state2));
		assertTrue(state3.equals(state3));
	}

	@Test
	public void testRecognizeAutomat1() {
		assertTrue(a1.recognizes("aa"));
		assertFalse(a1.recognizes("a"));
		assertFalse(a1.recognizes("abbbb"));
		assertFalse(a1.recognizes("ababbb"));
		assertTrue(a1.recognizes("abbbba"));
	}

	@Test
	/**
	 * testet automat aus pdf
	 */
	public void testRecognizeAutomat2() {

		assertTrue(a2.recognizes("aac"));
		assertTrue(a2.recognizes("abaac"));
		assertTrue(a2.recognizes("abbbbbaac"));
		assertTrue(a2.recognizes("aabbbbbac"));
		assertTrue(a2.recognizes("aabbc"));
		assertTrue(a2.recognizes("aabac"));
		assertTrue(a2.recognizes("aabbaac"));
		assertTrue(a2.recognizes("aabbbbbac"));
		assertTrue(a2.recognizes("abbbbbaac"));
		assertFalse(a2.recognizes("abbbbbc"));
		assertFalse(a2.recognizes("baac"));
		assertFalse(a2.recognizes("aacb"));
		assertFalse(a2.recognizes("abbbcc"));
	}

	@Test
	public void testContainsEndState() {
		final HashSet<State> states = new HashSet<State>();
		states.add(a1.getEndZustand());
		states.add(a1.getAnfangsZustand());
		states.add(Z1);
		assertTrue(a1.containsEndState(states));
		states.remove(a1.getEndZustand());
		assertFalse(a1.containsEndState(states));
	}

	@Test
	public void testStep() {
		final Collection<State> currentStates2 = new HashSet<State>();
		currentStates2.add(a1.getAnfangsZustand());
		final Configuration startConfig = Configuration.create("a", a1, currentStates2);
		assertEquals(Configuration.create("", a1), Configuration.create("", a1).step());
		final Configuration nextConfig = startConfig.step();
		final Collection<State> currentStates1 = new HashSet<State>();
		currentStates1.add(Z1);
		assertEquals(Configuration.create("", a1, currentStates1), nextConfig);

	}

	@Test
	public void testNextStates() {
		final HashSet<State> oneEntryA1 = new HashSet<State>();
		final HashSet<State> oneEntryEnd = new HashSet<State>();
		oneEntryA1.add(Z1);
		oneEntryEnd.add(a1.getEndZustand());
		assertEquals(oneEntryA1, a1.nextStates('a', a1.getAnfangsZustand()));
		assertEquals(oneEntryEnd, a1.nextStates('a', Z1));
		assertEquals(oneEntryA1, a1.nextStates('b', Z1));
	}

	@Test
	public void testIsInput() {
		assertTrue(t1.isInput('a'));
	}

}
