package test;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.ErrorEntry;
import buffer.IntegerEntry;
import buffer.StopCommand;
import exceptions.VariableNotAssignedException;
import model.Add;
import model.Constant;
import model.Divide;
import model.Multiply;
import model.Operator;
import model.Subtraction;
import model.Variable;
import toProcess.ProcessExpression;

public class TestForTesting {

	private Operator E0;
	private Operator E1;
	private Operator E2;
	private Operator E3;
	private Operator E4;
	private Variable X;
	private Variable Y;
	private Variable Z;
	private Constant C10;
	private Constant C5;

	@Before
	public void setUp() throws Exception {
		this.X = Variable.createVariable("X");
		this.X.setValue(new BigInteger("30"));
		this.Y = Variable.createVariable("Y");
		this.Y.setValue(new BigInteger("2"));
		this.Z = Variable.createVariable("Z");
		this.Z.setValue(new BigInteger("5"));
		this.C10 = Constant.create(BigInteger.TEN);
		this.C5 = Constant.create(new BigInteger("5"));
		this.E2 = Subtraction.create(this.X, this.Y);
		this.E4 = Divide.create(this.C10, this.C5);
		this.E3 = Multiply.create(this.E4, this.Z);
		this.E1 = Add.create(this.E2, this.E3);
		this.E0 = Multiply.create(this.X, this.E1);
	}



	@Test
	public void multipleOperations() throws VariableNotAssignedException {
		Operator eX = Multiply.create(this.E1, this.E1);
		List<BufferEntry> xyzInput = new LinkedList<>();
		xyzInput.add(new IntegerEntry(3));
		xyzInput.add(new IntegerEntry(2));
		xyzInput.add(new IntegerEntry(1));
		xyzInput.add(new StopCommand());
		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put(this.Z.getName(), xyzInput);
		variableAssignment.put(this.X.getName(), xyzInput);
		variableAssignment.put(this.Y.getName(), xyzInput);
		List<BufferEntry> expectedValues = new LinkedList<>();
		this.testOperation(eX, variableAssignment, expectedValues);
	}

	public void testOperation(Operator op, Map<String, List<BufferEntry>> variableAssignment,
			List<BufferEntry> expectedValues) throws VariableNotAssignedException {
		ProcessExpression opProcess = op.toProcessExpression();
		opProcess.start(variableAssignment);
		Buffer output = opProcess.getOutputBuffer();
		BufferEntry entry = new ErrorEntry();
		while(! entry.equals(new StopCommand())) {
			entry = output.get();
		}
//		System.out.println(opProcess);
//		Iterator<BufferEntry> it = expectedValues.iterator();
//		while (it.hasNext()) {
//			BufferEntry expectedEntry = it.next();
//			assertEquals(expectedEntry, output.get());
//		}

	}

}
