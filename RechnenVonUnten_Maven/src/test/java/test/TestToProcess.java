package test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import buffer.BufferEntry;
import buffer.ErrorEntry;
import buffer.IntegerEntry;
import buffer.StopCommand;
import exceptions.VariableNotAssignedException;
import model.Add;
import model.Constant;
import model.Divide;
import model.Multiply;
import model.Operator;
import model.Subtraction;
import model.Variable;
import toProcess.ProcessExpression;

public class TestToProcess {

	private Operator E0;
	private Operator E1;
	private Operator E2;
	private Operator E3;
	private Operator E4;
	private Variable X;
	private Variable Y;
	private Variable Z;
	private Constant C10;
	private Constant C5;

	@Before
	public void setUp() throws Exception {
		this.X = Variable.createVariable("X");
		this.X.setValue(new BigInteger("30"));
		this.Y = Variable.createVariable("Y");
		this.Y.setValue(new BigInteger("2"));
		this.Z = Variable.createVariable("Z");
		this.Z.setValue(new BigInteger("5"));
		this.C10 = Constant.create(BigInteger.TEN);
		this.C5 = Constant.create(new BigInteger("5"));
		this.E2 = Subtraction.create(this.X, this.Y);
		this.E4 = Divide.create(this.C10, this.C5);
		this.E3 = Multiply.create(this.E4, this.Z);
		this.E1 = Add.create(this.E2, this.E3);
		this.E0 = Multiply.create(this.X, this.E1);
	}

	@Test
	public void testToProcessVariable() throws VariableNotAssignedException {
		List<BufferEntry> input = new LinkedList<>();
		input.add(new IntegerEntry(25));
		input.add(new IntegerEntry(-7));
		input.add(new IntegerEntry(-11));
		input.add(new IntegerEntry(-20));
		input.add(new StopCommand());
		this.testVariable(this.X, input);
		this.testVariable(this.Y, input);
		this.testVariable(this.Z, input);
		List<BufferEntry> input2 = new LinkedList<>();
		input2.add(new IntegerEntry(25));
		input2.add(new IntegerEntry(-20));
		input2.add(new ErrorEntry());
		this.testVariable(this.X, input2);
		this.testVariable(this.Y, input2);
		this.testVariable(this.Z, input2);

	}

	private void testVariable(Variable variable, List<BufferEntry> input) throws VariableNotAssignedException {
		ProcessExpression variableProcess = variable.toProcessExpression();
		Buffer variableOutput = variableProcess.getOutputBuffer();
		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put(variable.getName(), input);
		variableProcess.start(variableAssignment);

		Iterator<BufferEntry> it = input.iterator();
		while (it.hasNext()) {
			BufferEntry current = it.next();
			assertEquals(current, variableOutput.get());
		}
	}

	@Test
	public void testToProcessConstant() throws VariableNotAssignedException {
		this.testConstant(this.C5);
		this.testConstant(this.C10);
	}

	private void testConstant(Constant c) throws VariableNotAssignedException {
		ProcessExpression cProzess = c.toProcessExpression();
		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		cProzess.start(variableAssignment);
		Buffer outPut = cProzess.getOutputBuffer();
		assertEquals(new IntegerEntry(c.getValue()), outPut.get());
		assertEquals(new IntegerEntry(c.getValue()), outPut.get());
		assertEquals(new IntegerEntry(c.getValue()), outPut.get());
		assertEquals(new IntegerEntry(c.getValue()), outPut.get());
		assertEquals(new IntegerEntry(c.getValue()), outPut.get());
		assertEquals(new IntegerEntry(c.getValue()), outPut.get());

	}

	@Test
	public void testToProcessOperation() throws VariableNotAssignedException {
		List<BufferEntry> xyzInput = new LinkedList<>();
		xyzInput.add(new IntegerEntry(25));
		xyzInput.add(new IntegerEntry(15));
		xyzInput.add(new IntegerEntry(-11));
		xyzInput.add(new StopCommand());
		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put(this.X.getName(), xyzInput);
		variableAssignment.put(this.Y.getName(), xyzInput);
		variableAssignment.put(this.Z.getName(), xyzInput);

		List<BufferEntry> expectedValuesE4 = new LinkedList<>();
		// Da ConstantenOperation werden hier viele 2en erwartet
		expectedValuesE4.add(new IntegerEntry(2));
		expectedValuesE4.add(new IntegerEntry(2));
		expectedValuesE4.add(new IntegerEntry(2));
		expectedValuesE4.add(new IntegerEntry(2));
		expectedValuesE4.add(new IntegerEntry(2));
		expectedValuesE4.add(new IntegerEntry(2));
		this.testOperation(this.E4, variableAssignment, expectedValuesE4);

		List<BufferEntry> expectedValuesE3 = new LinkedList<>();
		expectedValuesE3.add(new IntegerEntry(50));
		expectedValuesE3.add(new IntegerEntry(30));
		expectedValuesE3.add(new IntegerEntry(-22));
		expectedValuesE3.add(new StopCommand());
		this.testOperation(this.E3, variableAssignment, expectedValuesE3);

		List<BufferEntry> expectedValuesE2 = new LinkedList<>();
		expectedValuesE2.add(new IntegerEntry(0));
		expectedValuesE2.add(new IntegerEntry(0));
		expectedValuesE2.add(new IntegerEntry(0));
		expectedValuesE2.add(new StopCommand());
		this.testOperation(this.E2, variableAssignment, expectedValuesE2);

		List<BufferEntry> expectedValuesE1 = new LinkedList<>();
		expectedValuesE1.add(new IntegerEntry(50));
		expectedValuesE1.add(new IntegerEntry(30));
		expectedValuesE1.add(new IntegerEntry(-22));
		expectedValuesE1.add(new StopCommand());
		this.testOperation(this.E1, variableAssignment, expectedValuesE1);

		List<BufferEntry> expectedValuesE0 = new LinkedList<>();
		expectedValuesE0.add(new IntegerEntry(50 * 25));
		expectedValuesE0.add(new IntegerEntry(30 * 15));
		expectedValuesE0.add(new IntegerEntry(-22 * -11));
		expectedValuesE0.add(new StopCommand());
		this.testOperation(this.E0, variableAssignment, expectedValuesE0);

	}

	public void testOperation(Operator op, Map<String, List<BufferEntry>> variableAssignment,
			List<BufferEntry> expectedValues) throws VariableNotAssignedException {
		ProcessExpression opProcess = op.toProcessExpression();
		opProcess.start(variableAssignment);
		Buffer output = opProcess.getOutputBuffer();
		Iterator<BufferEntry> it = expectedValues.iterator();
		while (it.hasNext()) {
			BufferEntry expectedEntry = it.next();
			assertEquals(expectedEntry, output.get());
		}

	}

	@Test
	public void testToProcessKomplexWithE3() throws VariableNotAssignedException {
		List<BufferEntry> zBuffer = new LinkedList<>();
		zBuffer.add(new IntegerEntry(25));
		zBuffer.add(new IntegerEntry(10));
		zBuffer.add(new IntegerEntry(-11));
		zBuffer.add(new IntegerEntry(-0));
		zBuffer.add(new StopCommand());

		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put("Z", zBuffer);

		ProcessExpression e3Expression = this.E3.toProcessExpression();
		e3Expression.start(variableAssignment);
		Buffer output = e3Expression.getOutputBuffer();
		assertEquals(new IntegerEntry(50), output.get());
		assertEquals(new IntegerEntry(20), output.get());
		assertEquals(new IntegerEntry(-22), output.get());
		assertEquals(new IntegerEntry(0), output.get());
		assertEquals(new StopCommand(), output.get());
	}

	@Test
	public void testToProcessKomplexWithE1() throws VariableNotAssignedException {
		List<BufferEntry> zBuffer = new LinkedList<>();
		zBuffer.add(new IntegerEntry(25));
		zBuffer.add(new IntegerEntry(10));
		zBuffer.add(new IntegerEntry(-11));
		zBuffer.add(new StopCommand());

		List<BufferEntry> xBuffer = new LinkedList<>();
		xBuffer.add(new IntegerEntry(25));
		xBuffer.add(new IntegerEntry(10));
		xBuffer.add(new IntegerEntry(25));
		xBuffer.add(new StopCommand());

		List<BufferEntry> yBuffer = new LinkedList<>();
		yBuffer.add(new IntegerEntry(15));
		yBuffer.add(new IntegerEntry(10));
		yBuffer.add(new IntegerEntry(100));
		yBuffer.add(new StopCommand());

		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put("Z", zBuffer);
		variableAssignment.put("X", xBuffer);
		variableAssignment.put("Y", yBuffer);

		ProcessExpression e1Expression = this.E1.toProcessExpression();
		e1Expression.start(variableAssignment);
		Buffer output = e1Expression.getOutputBuffer();
		assertEquals(new IntegerEntry(60), output.get());
		assertEquals(new IntegerEntry(20), output.get());
		assertEquals(new IntegerEntry(-97), output.get());
		assertEquals(new StopCommand(), output.get());
	}

	@Test
	public void testToProcessKomplexWithE0() throws VariableNotAssignedException {

		List<BufferEntry> zBuffer = new LinkedList<>();
		zBuffer.add(new IntegerEntry(25));
		zBuffer.add(new IntegerEntry(10));
		zBuffer.add(new IntegerEntry(-11));
		zBuffer.add(new StopCommand());

		List<BufferEntry> xBuffer = new LinkedList<>();
		xBuffer.add(new IntegerEntry(25));
		xBuffer.add(new IntegerEntry(10));
		xBuffer.add(new IntegerEntry(25));
		xBuffer.add(new StopCommand());

		List<BufferEntry> yBuffer = new LinkedList<>();
		yBuffer.add(new IntegerEntry(15));
		yBuffer.add(new IntegerEntry(10));
		yBuffer.add(new IntegerEntry(100));
		yBuffer.add(new StopCommand());

		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put("Z", zBuffer);
		variableAssignment.put("X", xBuffer);
		variableAssignment.put("Y", yBuffer);

		ProcessExpression e0Expression = this.E0.toProcessExpression();
		e0Expression.start(variableAssignment);
		Buffer output = e0Expression.getOutputBuffer();
		assertEquals(new IntegerEntry(1500), output.get());
		assertEquals(new IntegerEntry(200), output.get());
		assertEquals(new IntegerEntry(-2425), output.get());
		assertEquals(new StopCommand(), output.get());
	}

	@Test
	public void testErrorEntryStrict() throws VariableNotAssignedException {

		List<BufferEntry> xyInput = new LinkedList<>();
		xyInput.add(new IntegerEntry(25));
		xyInput.add(new IntegerEntry(15));
		xyInput.add(new IntegerEntry(-11));
		xyInput.add(new StopCommand());
		List<BufferEntry> zInput = new LinkedList<>();
		zInput.add(new ErrorEntry());
		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put(this.X.getName(), xyInput);
		variableAssignment.put(this.Y.getName(), xyInput);
		variableAssignment.put(this.Z.getName(), zInput);

		this.testOperation(this.E0, variableAssignment, zInput);
	}

	@Test(expected = VariableNotAssignedException.class)
	public void testVariableAssignment() throws VariableNotAssignedException {
		ProcessExpression e0Process = this.E0.toProcessExpression();
		e0Process.start(new HashMap<>());
	}

	@Test
	public void multipleOperations() throws VariableNotAssignedException {
		Operator eX = Multiply.create(this.E1, this.E1);
		List<BufferEntry> xyzInput = new LinkedList<>();
		xyzInput.add(new IntegerEntry(3));
		xyzInput.add(new IntegerEntry(2));
		xyzInput.add(new IntegerEntry(1));
		xyzInput.add(new StopCommand());
		Map<String, List<BufferEntry>> variableAssignment = new HashMap<>();
		variableAssignment.put(this.Z.getName(), xyzInput);
		variableAssignment.put(this.X.getName(), xyzInput);
		variableAssignment.put(this.Y.getName(), xyzInput);
		List<BufferEntry> expectedValues = new LinkedList<>();
		expectedValues.add(new IntegerEntry(36));
		expectedValues.add(new IntegerEntry(16));
		expectedValues.add(new IntegerEntry(4));
		this.testOperation(eX, variableAssignment, expectedValues);
	}

}
