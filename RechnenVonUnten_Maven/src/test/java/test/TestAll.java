package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestToProcess.class})
/**
 * Ich f�hre alle oben genannten Testklassen aus.
 * @author tim
 *
 */
public class TestAll {
	// the class remains empty,
	// used only as a holder for the above annotations
}