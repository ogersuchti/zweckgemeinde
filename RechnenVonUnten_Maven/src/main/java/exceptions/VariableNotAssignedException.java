package exceptions;

@SuppressWarnings("serial")
public class VariableNotAssignedException extends Exception {

	public VariableNotAssignedException(String errorMessage) {
		super(errorMessage);
	}
	
}
