package toProcess;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import buffer.Buffer;
import buffer.BufferEntry;
import exceptions.VariableNotAssignedException;
import model.Expression;
import model.Process;
import model.Variable;

public class ProcessExpression {

	private Buffer output;
	private Map<String, Vervielfacher> variableVervielfacher;
	private Map<String, Vervielfacher> expressionVervielfacher;
	private Collection<Process> processes;

	public ProcessExpression(Collection<Process> alleProzesse) {
		super();
		this.output = new Buffer();
		this.variableVervielfacher = new HashMap<>();
		this.expressionVervielfacher = new HashMap<>();
		this.processes = alleProzesse;
	}

	public void start(Map<String, List<BufferEntry>> variableAssignment) throws VariableNotAssignedException {
		Iterator<String> keys = this.variableVervielfacher.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			List<BufferEntry> entry = variableAssignment.get(key);
			if (entry == null) {
				throw new VariableNotAssignedException("Variable " + key + " not assigned");
			}
			this.variableVervielfacher.get(key).addToBuffer(entry);

		}
		this.startProcesses();
	}

	private void startProcesses() {
		Iterator<Process> it1 = this.processes.iterator();
		while (it1.hasNext()) {
			Process current = it1.next();
			current.start();
		}
		Iterator<Vervielfacher> it2 = this.getVervielfacher().iterator();
		while (it2.hasNext()) {
			Vervielfacher current = it2.next();
			current.start();
		}

	}

	private Collection<Vervielfacher> getVervielfacher() {
		Collection<Vervielfacher> variableVervielfacher = this.variableVervielfacher.values();
		Collection<Vervielfacher> expressionVervielfacher = this.expressionVervielfacher.values();
		// Dunno why but we need a fresh Collection here.
		Collection<Vervielfacher> result = new LinkedList<>();
		result.addAll(variableVervielfacher);
		result.addAll(expressionVervielfacher);
		return result;
	}

	public Buffer getOutputBuffer() {
		return this.output;
	}

	/**
	 * F�gt Variable hinzu und gibt den entsprechenden Ausgabepuffer zur Variablen zur�ck.
	 * Falls diese Variable mehrfach vorkommt erh�lt jede Variable ihren eigenen Ausgabepuffer.
	 * Alle Ausgabepuffer einer Variablen werden aus einem Eingabepuffer gespeist.
	 */
	public Buffer addVariable(Variable variable) {
		String variableName = variable.getName();
		Vervielfacher entry = this.variableVervielfacher.get(variableName);
		if (entry == null) {
			Vervielfacher vervielfacher = new Vervielfacher(new Buffer());
			Buffer newOutPutBuffer = vervielfacher.newOutPutBuffer();
			this.variableVervielfacher.put(variableName, vervielfacher);
			return newOutPutBuffer;
		}
		Buffer newOutPutBuffer = entry.newOutPutBuffer();
		return newOutPutBuffer;

	}
	
	public InputOutput addExpression(Expression expression) {
		Vervielfacher entry = this.expressionVervielfacher.get(expression.toString());
		if (entry == null) {
			Vervielfacher vervielfacher = new Vervielfacher(new Buffer());
			Buffer newOutPutBuffer = vervielfacher.newOutPutBuffer();
			this.expressionVervielfacher.put(expression.toString(), vervielfacher);
			return new InputOutput(vervielfacher.getInputBuffer(), newOutPutBuffer);
		}
		Buffer newOutPutBuffer = entry.newOutPutBuffer();
		return new InputOutput(entry.getInputBuffer(), newOutPutBuffer);

	}


	public void addProcess(Process process) {
		this.processes.add(process);
	}


	public void setOutputBuffer(Buffer outputBuffer) {
		this.output = outputBuffer;
	}


	
	
}
