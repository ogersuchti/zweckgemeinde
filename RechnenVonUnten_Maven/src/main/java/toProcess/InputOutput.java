package toProcess;

import buffer.Buffer;

public class InputOutput {
	
	private final Buffer input;
	private final Buffer output;
	
	
	public Buffer getInput() {
		return this.input;
	}


	public Buffer getOutput() {
		return this.output;
	}


	public InputOutput(Buffer input, Buffer output) {
		super();
		this.input = input;
		this.output = output;
	}
	
	

}
