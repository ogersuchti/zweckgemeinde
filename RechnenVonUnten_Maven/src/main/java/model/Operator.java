package model;


import buffer.Buffer;
import toProcess.ProcessExpression;

public abstract class Operator extends ExpressionCommon{

	protected final Expression first;
	protected final Expression second;
	
	/**
	 * Constructs the wanted Operator by using fields.
	 * State is initialized by NoValue.
	 * first and second register the receiver to their Collection of currentObservers. 
	 */
	protected Operator(Expression first, Expression second) {
		super();
		this.first = first;
		this.second = second;
	}
	
	@Override
	public boolean contains(Expression expression) {
		return this.equals(expression) || this.first.contains(expression) || this.second.contains(expression);
	}
	
	@Override
	public Buffer toProcess(ProcessExpression result) {
//		InputOutput inputOutput = result.addExpression(this);
		Buffer input1 = this.first.toProcess(result);
		Buffer input2 = this.second.toProcess(result);
//		OperationProcess process = this.reallyToProcess(input1,input2,inputOutput.getInput());
		OperationProcess process = this.reallyToProcess(input1,input2);
		result.addProcess(process);
		return process.getOutputBuffer();
	}

	protected abstract OperationProcess reallyToProcess(Buffer input1, Buffer input2);

	protected abstract OperationProcess reallyToProcess(Buffer input1, Buffer input2, Buffer outPutBuffer);

}
