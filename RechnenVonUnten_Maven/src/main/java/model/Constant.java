package model;

import java.math.BigInteger;

import buffer.Buffer;
import toProcess.ProcessExpression;

public class Constant extends ExpressionCommon {

	private final BigInteger value;

	public static Constant create(BigInteger value) {
		return new Constant(value);
	}

	private Constant(BigInteger value) {
		this.value = value;
	}


	@Override
	public boolean contains(Expression expression) {
		return this.equals(expression);
	}

	@Override
	public Buffer toProcess(ProcessExpression result) {
		ConstantProcess process = new ConstantProcess(this.value);
		return process.getOutputBuffer();
	}

	public BigInteger getValue() {
		return this.value;
	}




}
