package model;

import buffer.Buffer;

public class Divide extends Operator {


	public static Divide create(Expression first, Expression second) {
		return new Divide(first, second);
	}

	private Divide(Expression first, Expression second) {
		super(first, second);
	}


	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2, Buffer output) {
		return DivisionProcess.create(input1, input2, output);
	}

	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2) {
		return DivisionProcess.create(input1, input2);
	}
}
