package model;

import buffer.Buffer;

public class Add extends Operator {


	/**
	 * @param first
	 *            expression
	 * @param second
	 *            expression
	 * @return a new Object representing the Expression second added to first.
	 */
	public static Add create(Expression first, Expression second) {
		return new Add(first, second);

	}

	private Add(Expression first, Expression second) {
		super(first, second);
	}

	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2, Buffer output) {
		return AdditionProcess.create(input1, input2, output);
	}

	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2) {
		return AdditionProcess.create(input1, input2);
	}

}
