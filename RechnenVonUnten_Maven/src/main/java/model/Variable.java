package model;

import java.math.BigInteger;

import buffer.Buffer;
import toProcess.ProcessExpression;

public class Variable extends ExpressionCommon {

	private static final BigInteger InitialVariableValue = BigInteger.ZERO;

	public static Variable createVariable(final String name) {
		return new Variable(name, InitialVariableValue);
	}

	private final String name;
	private BigInteger value;

	public BigInteger getValue() {
		return this.value;
	}

	private Variable(final String name, final BigInteger initialValue) {
		this.name = name;
		this.value = initialValue;
		;
	}

	@Override
	public boolean equals(Object argument) {
		return super.equals(argument);
	}

	@Override
	public boolean contains(Expression expression) {
		return this.equals(expression);
	}

	@Override
	public Buffer toProcess(ProcessExpression result) {
		Buffer meinOutput = result.addVariable(this);
		return meinOutput;
	}

	public String getName() {
		return this.name;
	}

	public void setValue(BigInteger newValue) {
		this.value = newValue;
	}

}
