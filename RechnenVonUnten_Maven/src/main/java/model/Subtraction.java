package model;

import buffer.Buffer;

public class Subtraction extends Operator {


	public static Subtraction create(Expression first, Expression second) {
		return new Subtraction(first, second);

	}

	private Subtraction(Expression first, Expression second) {
		super(first, second);
	}

	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2, Buffer output) {
		return SubtractionProcess.create(input1, input2, output);
	}

	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2) {
		return SubtractionProcess.create(input1, input2);
	}
}
