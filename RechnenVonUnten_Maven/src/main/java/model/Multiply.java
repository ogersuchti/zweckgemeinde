package model;

import buffer.Buffer;

public class Multiply extends Operator {
	
	

	public static Multiply create(Expression first, Expression second) {
		return new Multiply(first, second);

	}

	private Multiply(Expression first, Expression second) {
		super(first, second);
	}
	

	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2,Buffer output) {
		return MultiplikationProcess.create(input1, input2,output);
	}

	@Override
	protected OperationProcess reallyToProcess(Buffer input1, Buffer input2) {
		return MultiplikationProcess.create(input1, input2);
	}

}
