package model;

import java.util.Collection;
import java.util.LinkedList;

import buffer.Buffer;
import toProcess.ProcessExpression;

public abstract class ExpressionCommon implements Expression{
	
	@Override
	public ProcessExpression toProcessExpression() {
		Collection<Process> alleProzesse = new LinkedList<>();
		ProcessExpression result = new ProcessExpression(alleProzesse);
		Buffer outputBuffer = this.toProcess(result);
		result.setOutputBuffer(outputBuffer);
		return result;
	}

	@Override
	public abstract Buffer toProcess(ProcessExpression result);

}
