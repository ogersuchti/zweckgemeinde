package model;

import buffer.Buffer;
import toProcess.ProcessExpression;

public interface Expression {

	public abstract boolean contains(Expression expression);

	public abstract ProcessExpression toProcessExpression();

	public abstract Buffer toProcess(ProcessExpression result);

}
