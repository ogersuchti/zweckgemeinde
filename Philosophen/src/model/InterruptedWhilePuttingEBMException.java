package model;

public class InterruptedWhilePuttingEBMException extends Exception {

	public InterruptedWhilePuttingEBMException(String string) {
		super(string);
	}

}
