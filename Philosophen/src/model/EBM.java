package model;

public class EBM {

	boolean available;

	public EBM() {
		this.available = true;
	}

	synchronized public EBM get() throws InterruptedWhileGettingEBMException {
		while (!this.available) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				throw new InterruptedWhileGettingEBMException("Interrupted while waiting to get EBM: " + this);
			}
		}
		this.available = false;
		this.notify();
		return this;
	}

	synchronized public void put() throws InterruptedWhilePuttingEBMException {
		while (this.available) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				throw new InterruptedWhilePuttingEBMException("Interrupted while waiting to put EBM: " + this);
			}
		}
		this.available = true;
		this.notify();

	}

}
