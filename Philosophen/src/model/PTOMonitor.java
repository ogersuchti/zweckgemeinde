package model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class PTOMonitor {

	private int maxDenkzeit;
	private int maxEssenszeit;
	private int anzahlPhilosophen;
	private Collection<Philosopher> meinePhilosophen;
	private Collection<Thread> meinePhilosophenThreads;

	public PTOMonitor(int denkzeit, int essenszeit, int anzahlPhilosophen) {
		super();
		this.maxDenkzeit = denkzeit;
		this.maxEssenszeit = essenszeit;
		this.anzahlPhilosophen = anzahlPhilosophen;
		this.meinePhilosophen = new LinkedList<Philosopher>();
		this.meinePhilosophenThreads = new LinkedList<>();
	}

	public void losGehts() {
		this.verteileEBMsUndErstellePhilosophen();
		this.startePhilosophen();
	}

	private void startePhilosophen() {
		for (Philosopher currentP : this.meinePhilosophen) {
			Thread thread = new Thread(currentP,currentP.toString());
			this.meinePhilosophenThreads.add(thread);
			thread.start();
		}
	}

	public void stoppePhilosophen() {
		for (Philosopher currentP : this.meinePhilosophen) {
			currentP.stopp();
		}
		for(Thread philosopher : this.meinePhilosophenThreads) {
			philosopher.interrupt();
		}
	}

	private void verteileEBMsUndErstellePhilosophen() {
		for (int i = 0; i < this.anzahlPhilosophen; i++) {
			this.meinePhilosophen.add(new Philosopher(this, "Philosoph " + (i + 1)));
		}
		Iterator<Philosopher> it = this.meinePhilosophen.iterator();
		EBM links = new EBM();
		EBM erste = links;
		while (it.hasNext()) {
			Philosopher philosopher = it.next();
			philosopher.addLeftEBM(links);
			if (it.hasNext()) {
				EBM rechts = new EBM();
				philosopher.addRightEBM(rechts);
				links = rechts;

			} else {
				philosopher.addRightEBM(erste);
			}
		}
		System.out.println(this.meinePhilosophen);
	}

	public int getMaxDenkzeit() {
		return this.maxDenkzeit;
	}

	public int getMaxEssenszeit() {
		return this.maxEssenszeit;
	}

	public void erDenkt(Philosopher philosopher) {
		System.out.println("Er denkt jetzt: " + philosopher);
	}

	public void erIsst(Philosopher philosopher) {
		System.out.println("Er isst  jetzt: " + philosopher);
	}

	public int getDenkzeit() {
		Random randomGen = new Random();
		int echteDenkzeit = randomGen.nextInt(this.maxDenkzeit) + 1; // Weil
																		// nicht
																		// Null
																		// + 1
		return echteDenkzeit;
	}

	public int getEssenszeit() {
		Random randomGen = new Random();
		int echteEssenszeit = randomGen.nextInt(this.maxEssenszeit) + 1; // Weil
																			// nicht
																			// Null
																			// +
																			// 1
		return echteEssenszeit;
	}
}
