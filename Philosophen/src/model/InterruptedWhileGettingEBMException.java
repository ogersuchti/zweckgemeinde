package model;

public class InterruptedWhileGettingEBMException extends Exception {

	public InterruptedWhileGettingEBMException(String string) {
		super(string);
	}

}
