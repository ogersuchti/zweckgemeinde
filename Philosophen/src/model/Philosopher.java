package model;

public class Philosopher implements Runnable{
	
	
	private EBM left;
	private EBM right;
	private boolean thinking;
	private PTOMonitor god;
	private boolean erregt;
	private String name;
	
	public Philosopher(PTOMonitor god, String name){
		this.name = name;
		this.thinking = true;
		this.god = god;
		this.erregt = true;
	}
	
	public void addLeftEBM(EBM left){
		this.left = left;
	}
	public void addRightEBM(EBM right){
		this.right = right;
	}


	@Override
	public void run() {
		this.letsRumble();
	}

	private void letsRumble() {
		while(this.erregt) {
			try {
				this.essenWollen();
				try {
					this.denkenWollen();
				} catch (InterruptedWhilePuttingEBMException e) {
					System.out.println(e.getMessage());
				}
			} catch (InterruptedWhileGettingEBMException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private void denkenWollen() throws InterruptedWhilePuttingEBMException {
			this.left.put();
			this.right.put();
			this.denken();
	}

	private synchronized void denken() {
		this.thinking = true;
		this.god.erDenkt(this);
		try {
			this.wait(this.god.getDenkzeit());
		} catch (InterruptedException e) {
			System.out.println("Interupted while thinking");
		}
	}

	private void essenWollen() throws InterruptedWhileGettingEBMException {
			this.left.get();
			this.right.get();
			this.essen();
	}

	private synchronized void essen() {
		this.thinking = false;
		this.god.erIsst(this);
		try {
			this.wait(this.god.getEssenszeit());
		} catch (InterruptedException e) {
			System.out.println("Interupted while eating");
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.name);
		return builder.toString();
	}

	public void stopp() {
		this.erregt = false;
	}
	
	

}
