package model;

public class DerNichtAusreichendeTest {
	
	public static void main(String[] args) {
		PTOMonitor god = new PTOMonitor(1, 1, 4);
		god.losGehts();
		synchronized (god) {
			
			try {
				god.wait(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		god.stoppePhilosophen();
	}

}
